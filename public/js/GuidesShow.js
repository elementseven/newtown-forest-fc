(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["GuidesShow"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/guides/GuidesShow.vue?vue&type=script&lang=js&":



function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsGuidesGuidesShowVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
//
//
//
//
//
//
//
//
//
var GuideCard=function GuideCard(){
return __webpack_require__.e(/*! import() | GuideCard */"GuideCard").then(__webpack_require__.bind(__webpack_require__,/*! ./GuideCard.vue */"./resources/js/components/guides/GuideCard.vue"));
};

/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
props:{
guides:Array},

components:{
GuideCard:GuideCard},

watch:{
guides:function guides(val){
console.log('changed');
}},

methods:{}};


/***/},

/***/"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/guides/GuidesShow.vue?vue&type=style&index=0&lang=scss&":



function node_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsGuidesGuidesShowVueVueTypeStyleIndex0LangScss(module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */"./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___=_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1];});
// Module
___CSS_LOADER_EXPORT___.push([module.id,".guide-card {\n  position: relative;\n  width: 260px;\n  height: 372px;\n  background-repeat: no-repeat;\n  background-position: center center;\n  background-size: auto 110%;\n  border-radius: 20px;\n  text-align: left;\n  overflow: hidden;\n  cursor: pointer;\n  padding-top: 2rem;\n  padding-left: 2rem;\n  box-shadow: 0rem 0rem 1rem rgba(0, 0, 0, 0.3) !important;\n  transition: all 300ms ease;\n}\n.guide-card a {\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    display: block;\n}\n.guide-card .guide-info {\n    position: absolute;\n    bottom: 0;\n    left: 0;\n    width: 100%;\n    padding: 7rem 1rem 1rem 1rem;\n    background: #111213;\n    background: linear-gradient(180deg, rgba(17, 18, 19, 0) 0%, #111213 100%);\n}\n.guide-card .guide-info p {\n      color: #fff;\n}\n.guide-card .guide-info p.guide-card-top {\n        font-family: ff-zwo-corr-web-pro, sans-serif;\n        color: #fff;\n        font-size: 1rem;\n        line-height: 1.3;\n        margin-bottom: 0.3rem;\n        letter-spacing: 0.2rem;\n}\n.guide-card .guide-info p.guide-card-qualifications {\n        font-family: ff-zwo-corr-web-pro, sans-serif;\n        color: #fff;\n        font-size: 0.8rem;\n        line-height: 1.2;\n        margin-bottom: 0.3rem;\n        letter-spacing: 0.2rem;\n}\n.guide-card .guide-info p.guide-card-experiences {\n        font-family: ff-zwo-corr-web-pro, sans-serif;\n        color: #fff;\n        font-size: 0.7rem;\n        line-height: 1.4;\n        margin-bottom: 0.3rem;\n        font-weight: 100;\n        letter-spacing: 2px;\n        text-transform: uppercase;\n}\n.guide-card .guide-info p.guide-card-title {\n        font-family: ff-zwo-corr-web-pro, sans-serif;\n        color: #fff;\n        font-size: 1.2rem;\n        line-height: 1.4;\n        margin-bottom: 0;\n}\n.guide-card .guide-info p.guide-card-languages {\n        font-family: ff-zwo-corr-web-pro, sans-serif;\n        color: #fff;\n        font-size: 0.6rem;\n        line-height: 1.2;\n        margin-bottom: 0.3rem;\n        letter-spacing: 1px;\n        text-transform: uppercase;\n}\n.guide-card:hover {\n    background-size: auto 120%;\n}\n@media only screen and (max-width: 767px) {\n.guide-card {\n    width: 100%;\n    background-size: cover !important;\n}\n.guide-card:hover {\n      background-size: cover !important;\n}\n}\n",""]);
// Exports
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=___CSS_LOADER_EXPORT___;


/***/},

/***/"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/guides/GuidesShow.vue?vue&type=style&index=0&lang=scss&":



function node_modulesLaravelMixNode_modulesStyleLoaderDistCjsJsNode_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsGuidesGuidesShowVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! !../../../../node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */"./node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_GuidesShow_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./GuidesShow.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/guides/GuidesShow.vue?vue&type=style&index=0&lang=scss&");



var options={};

options.insert="head";
options.singleton=false;

var update=_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_GuidesShow_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"],options);



/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_GuidesShow_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"].locals||{};

/***/},

/***/"./resources/js/components/guides/GuidesShow.vue":



function resourcesJsComponentsGuidesGuidesShowVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _GuidesShow_vue_vue_type_template_id_12cef011___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./GuidesShow.vue?vue&type=template&id=12cef011& */"./resources/js/components/guides/GuidesShow.vue?vue&type=template&id=12cef011&");
/* harmony import */var _GuidesShow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./GuidesShow.vue?vue&type=script&lang=js& */"./resources/js/components/guides/GuidesShow.vue?vue&type=script&lang=js&");
/* harmony import */var _GuidesShow_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./GuidesShow.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/guides/GuidesShow.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
_GuidesShow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
_GuidesShow_vue_vue_type_template_id_12cef011___WEBPACK_IMPORTED_MODULE_0__.render,
_GuidesShow_vue_vue_type_template_id_12cef011___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/guides/GuidesShow.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/guides/GuidesShow.vue?vue&type=script&lang=js&":



function resourcesJsComponentsGuidesGuidesShowVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_GuidesShow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./GuidesShow.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/guides/GuidesShow.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_GuidesShow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"];

/***/},

/***/"./resources/js/components/guides/GuidesShow.vue?vue&type=style&index=0&lang=scss&":



function resourcesJsComponentsGuidesGuidesShowVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_GuidesShow_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./GuidesShow.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/guides/GuidesShow.vue?vue&type=style&index=0&lang=scss&");


/***/},

/***/"./resources/js/components/guides/GuidesShow.vue?vue&type=template&id=12cef011&":



function resourcesJsComponentsGuidesGuidesShowVueVueTypeTemplateId12cef011(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_GuidesShow_vue_vue_type_template_id_12cef011___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_GuidesShow_vue_vue_type_template_id_12cef011___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_GuidesShow_vue_vue_type_template_id_12cef011___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./GuidesShow.vue?vue&type=template&id=12cef011& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/guides/GuidesShow.vue?vue&type=template&id=12cef011&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/guides/GuidesShow.vue?vue&type=template&id=12cef011&":



function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsGuidesGuidesShowVueVueTypeTemplateId12cef011(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"container"},[
_c(
"div",
{staticClass:"row"},
_vm._l(_vm.guides,function(guide){
return _c(
"div",
{staticClass:"col-lg-3"},
[_c("guide-card",{attrs:{guide:guide}})],
1);

}),
0)]);


};
var _staticRenderFns=[];
_render._withStripped=true;



/***/}}]);

}());
