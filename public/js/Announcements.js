(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["Announcements"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/AnnouncementSlide.vue?vue&type=script&lang=js&":



function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeAnnouncementSlideVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
props:{
announcement:Object},

data:function data(){
return {
errors:{},
success:false,
loaded:true,
bgImgDouble:this.announcement["double"],
bgImgDoubleWebp:this.announcement.doublewebp,
bgImgNormal:this.announcement.normal,
bgImgNormalWebp:this.announcement.normalwebp,
bgImgFeatured:this.announcement.featured,
bgImgFeaturedWebp:this.announcement.featuredwebp};

},
computed:{
bgstyles:function bgstyles(){
return {
'--bg-img-double':'url('+this.announcement["double"]+')',
'--bg-img-double-webp':'url('+this.announcement.doublewebp+')',
'--bg-img-normal':'url('+this.announcement.normal+')',
'--bg-img-normal-webp':'url('+this.announcement.normalwebp+')',
'--bg-img-featured':'url('+this.announcement.featured+')',
'--bg-img-featured-webp':'url('+this.announcement.featuredwebp+')'};

}}};



/***/},

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Announcements.vue?vue&type=script&lang=js&":



function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeAnnouncementsVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! vue-owl-carousel */"./node_modules/vue-owl-carousel/dist/vue-owl-carousel.js");
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */var _AnnouncementSlide__WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./AnnouncementSlide */"./resources/js/components/Home/AnnouncementSlide.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
components:{
carousel:vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default(),
AnnouncementSlide:_AnnouncementSlide__WEBPACK_IMPORTED_MODULE_1__["default"]},

watch:{
category:function category(val){
this.changeCategory(val);
}},

data:function data(){
return {
category:'*',
errors:{},
success:false,
loaded:false,
announcements:[],
limit:3,
page:1};

},
mounted:function mounted(){
this.getPosts();
},
methods:{
changeCategory:function changeCategory(val){
this.category=val;
this.getPosts();
},
getPosts:function getPosts(){
var _this=this;

var page=arguments.length>0&&arguments[0]!==undefined?arguments[0]:1;
this.loaded=false;
this.success=false;
this.errors={};
this.offset=0;
this.page=1;
axios.get('/news/get?limit='+this.limit+'&page='+page+'&category=1').then(function(response){
// Empty fields and enable success message
_this.announcements=response.data.data;
_this.loaded=true;
_this.success=true;
})["catch"](function(error){
// Error handling
_this.loaded=true;
console.log(error.response);
});
}}};



/***/},

/***/"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/AnnouncementSlide.vue?vue&type=style&index=0&id=149efe85&lang=scss&scoped=true&":



function node_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeAnnouncementSlideVueVueTypeStyleIndex0Id149efe85LangScssScopedTrue(module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */"./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___=_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1];});
// Module
___CSS_LOADER_EXPORT___.push([module.id,".no-webp .announcement[data-v-149efe85] {\n  background-image: var(--bg-img-double);\n}\n.webp .announcement[data-v-149efe85] {\n  background-image: var(--bg-img-double-webp);\n}\n.announcement[data-v-149efe85] {\n  -webkit-backface-visibility: hidden !important;\n  -webkit-transform: translateZ(0) scale(1, 1) !important;\n}\n.announcement .announcements-content[data-v-149efe85] {\n    max-width: 650px;\n    display: block;\n    margin: 0 auto 60px auto;\n}\n@media only screen and (max-width: 992px) {\n.no-webp .announcement[data-v-149efe85] {\n    background-image: var(--bg-img-featured);\n}\n.webp .announcement[data-v-149efe85] {\n    background-image: var(--bg-img-featured-webp);\n}\n}\n",""]);
// Exports
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=___CSS_LOADER_EXPORT___;


/***/},

/***/"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Announcements.vue?vue&type=style&index=0&lang=scss&":



function node_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeAnnouncementsVueVueTypeStyleIndex0LangScss(module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */"./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___=_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1];});
// Module
___CSS_LOADER_EXPORT___.push([module.id,".bg-hex {\n  background-color: #1C2D4B;\n  background-image: url(\"/img/graphics/hex.svg\");\n}\n#home-announcements {\n  width: calc(100% + 15px);\n  margin-left: -15px;\n  margin-right: -15px;\n}\n#home-announcements .title-topper {\n    background-color: #4D71A5;\n    padding: 0 1rem;\n    color: #fff;\n    width: auto;\n    border-radius: 10px;\n}\n#home-announcements .owl-item {\n    -webkit-backface-visibility: hidden !important;\n    -webkit-transform: translateZ(0) scale(1, 1) !important;\n}\n#home-announcements .owl-dots {\n    position: absolute;\n    width: 650px;\n    margin: auto;\n    left: calc(50% - 330px);\n    bottom: 1rem;\n    text-align: left;\n}\n#home-announcements .owl-dots .owl-dot span {\n      width: 20px;\n      height: 4px;\n      background: #fff;\n      border-radius: 0;\n}\n#home-announcements .owl-dots .owl-dot.active span {\n      background: #FBDC64;\n}\n",""]);
// Exports
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=___CSS_LOADER_EXPORT___;


/***/},

/***/"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/AnnouncementSlide.vue?vue&type=style&index=0&id=149efe85&lang=scss&scoped=true&":



function node_modulesLaravelMixNode_modulesStyleLoaderDistCjsJsNode_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeAnnouncementSlideVueVueTypeStyleIndex0Id149efe85LangScssScopedTrue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! !../../../../node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */"./node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AnnouncementSlide_vue_vue_type_style_index_0_id_149efe85_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AnnouncementSlide.vue?vue&type=style&index=0&id=149efe85&lang=scss&scoped=true& */"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/AnnouncementSlide.vue?vue&type=style&index=0&id=149efe85&lang=scss&scoped=true&");



var options={};

options.insert="head";
options.singleton=false;

var update=_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AnnouncementSlide_vue_vue_type_style_index_0_id_149efe85_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_1__["default"],options);



/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AnnouncementSlide_vue_vue_type_style_index_0_id_149efe85_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_1__["default"].locals||{};

/***/},

/***/"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Announcements.vue?vue&type=style&index=0&lang=scss&":



function node_modulesLaravelMixNode_modulesStyleLoaderDistCjsJsNode_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeAnnouncementsVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! !../../../../node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */"./node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Announcements_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Announcements.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Announcements.vue?vue&type=style&index=0&lang=scss&");



var options={};

options.insert="head";
options.singleton=false;

var update=_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Announcements_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"],options);



/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Announcements_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"].locals||{};

/***/},

/***/"./resources/js/components/Home/AnnouncementSlide.vue":



function resourcesJsComponentsHomeAnnouncementSlideVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _AnnouncementSlide_vue_vue_type_template_id_149efe85_scoped_true___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./AnnouncementSlide.vue?vue&type=template&id=149efe85&scoped=true& */"./resources/js/components/Home/AnnouncementSlide.vue?vue&type=template&id=149efe85&scoped=true&");
/* harmony import */var _AnnouncementSlide_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./AnnouncementSlide.vue?vue&type=script&lang=js& */"./resources/js/components/Home/AnnouncementSlide.vue?vue&type=script&lang=js&");
/* harmony import */var _AnnouncementSlide_vue_vue_type_style_index_0_id_149efe85_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./AnnouncementSlide.vue?vue&type=style&index=0&id=149efe85&lang=scss&scoped=true& */"./resources/js/components/Home/AnnouncementSlide.vue?vue&type=style&index=0&id=149efe85&lang=scss&scoped=true&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
_AnnouncementSlide_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
_AnnouncementSlide_vue_vue_type_template_id_149efe85_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
_AnnouncementSlide_vue_vue_type_template_id_149efe85_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
"149efe85",
null);
component.options.__file="resources/js/components/Home/AnnouncementSlide.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Home/Announcements.vue":



function resourcesJsComponentsHomeAnnouncementsVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _Announcements_vue_vue_type_template_id_63e43e67___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./Announcements.vue?vue&type=template&id=63e43e67& */"./resources/js/components/Home/Announcements.vue?vue&type=template&id=63e43e67&");
/* harmony import */var _Announcements_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./Announcements.vue?vue&type=script&lang=js& */"./resources/js/components/Home/Announcements.vue?vue&type=script&lang=js&");
/* harmony import */var _Announcements_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./Announcements.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/Home/Announcements.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
_Announcements_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
_Announcements_vue_vue_type_template_id_63e43e67___WEBPACK_IMPORTED_MODULE_0__.render,
_Announcements_vue_vue_type_template_id_63e43e67___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/Home/Announcements.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Home/AnnouncementSlide.vue?vue&type=script&lang=js&":



function resourcesJsComponentsHomeAnnouncementSlideVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AnnouncementSlide_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AnnouncementSlide.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/AnnouncementSlide.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AnnouncementSlide_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"];

/***/},

/***/"./resources/js/components/Home/Announcements.vue?vue&type=script&lang=js&":



function resourcesJsComponentsHomeAnnouncementsVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Announcements_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Announcements.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Announcements.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Announcements_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"];

/***/},

/***/"./resources/js/components/Home/AnnouncementSlide.vue?vue&type=style&index=0&id=149efe85&lang=scss&scoped=true&":



function resourcesJsComponentsHomeAnnouncementSlideVueVueTypeStyleIndex0Id149efe85LangScssScopedTrue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AnnouncementSlide_vue_vue_type_style_index_0_id_149efe85_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AnnouncementSlide.vue?vue&type=style&index=0&id=149efe85&lang=scss&scoped=true& */"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/AnnouncementSlide.vue?vue&type=style&index=0&id=149efe85&lang=scss&scoped=true&");


/***/},

/***/"./resources/js/components/Home/Announcements.vue?vue&type=style&index=0&lang=scss&":



function resourcesJsComponentsHomeAnnouncementsVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Announcements_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Announcements.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Announcements.vue?vue&type=style&index=0&lang=scss&");


/***/},

/***/"./resources/js/components/Home/AnnouncementSlide.vue?vue&type=template&id=149efe85&scoped=true&":



function resourcesJsComponentsHomeAnnouncementSlideVueVueTypeTemplateId149efe85ScopedTrue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AnnouncementSlide_vue_vue_type_template_id_149efe85_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AnnouncementSlide_vue_vue_type_template_id_149efe85_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AnnouncementSlide_vue_vue_type_template_id_149efe85_scoped_true___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AnnouncementSlide.vue?vue&type=template&id=149efe85&scoped=true& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/AnnouncementSlide.vue?vue&type=template&id=149efe85&scoped=true&");


/***/},

/***/"./resources/js/components/Home/Announcements.vue?vue&type=template&id=63e43e67&":



function resourcesJsComponentsHomeAnnouncementsVueVueTypeTemplateId63e43e67(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Announcements_vue_vue_type_template_id_63e43e67___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Announcements_vue_vue_type_template_id_63e43e67___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Announcements_vue_vue_type_template_id_63e43e67___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Announcements.vue?vue&type=template&id=63e43e67& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Announcements.vue?vue&type=template&id=63e43e67&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/AnnouncementSlide.vue?vue&type=template&id=149efe85&scoped=true&":



function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeAnnouncementSlideVueVueTypeTemplateId149efe85ScopedTrue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"div",
{
staticClass:"announcement position-relative full-height bg z-3 mob-px-0",
style:_vm.bgstyles},

[
_c("div",{staticClass:"trans"}),
_vm._v(" "),
_c(
"div",
{staticClass:"d-table full-height w-100 position-relative z-2"},
[
_c(
"div",
{staticClass:"d-table-cell align-bottom w-100 h-100 pb-5"},
[
_c("div",{staticClass:"announcements-content pr-5"},[
_c("p",{staticClass:"mimic-h2 text-white"},[
_vm._v(_vm._s(_vm.announcement.title))]),

_vm._v(" "),
_c(
"a",
{
staticClass:"text-white",
attrs:{
href:
"/news/"+
_vm.announcement.id+
"/"+
_vm.announcement.slug}},


[
_c(
"button",
{
staticClass:"btn btn-primary",
attrs:{type:"button"}},

[_vm._v("Read more")])])])])])]);










};
var _staticRenderFns=[];
_render._withStripped=true;



/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Announcements.vue?vue&type=template&id=63e43e67&":



function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeAnnouncementsVueVueTypeTemplateId63e43e67(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render2);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns2);}
/* harmony export */});
var _render2=function _render2(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"row"},[
_c(
"div",
{staticClass:"col-lg-8 pr-0"},
[
_vm.loaded==true?
_c(
"carousel",
{
attrs:{
id:"home-announcements",
items:1,
margin:0,
center:false,
loop:false,
nav:false,
dots:true,
autoplayTimeout:7000,
autoplay:true,
animateOut:"fadeOut",
mouseDrag:false}},


_vm._l(_vm.announcements,function(announcement,key){
return _vm.loaded==true?
_c("announcement-slide",{
key:key,
attrs:{announcement:announcement}}):

_vm._e();
}),
1):

_vm._e()],

1),

_vm._v(" "),
_vm._m(0)]);

};
var _staticRenderFns2=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"col-lg-4 pl-0 bg bg-hex "},[
_c("div",{staticClass:"d-table w-100 h-100 text-white"},[
_c(
"div",
{staticClass:"d-table-cell align-middle w-100 text-center pt-5"},
[
_c("div",{staticClass:"pb-5 mb-5"},[
_c(
"p",
{
staticClass:"text-uppercase text-primary letter-spacing mb-0"},

[_vm._v("14.00PM - 18TH SEP 2021")]),

_vm._v(" "),
_c(
"p",
{
staticClass:
"text-small text-uppercase text-white letter-spacing"},

[_vm._v("WESTWINDS, NEWTOWNARDS")])]),


_vm._v(" "),
_c("div",{staticClass:"scores container mb-5 pb-5"},[
_c("div",{staticClass:"row"},[
_c("div",{staticClass:"score col-5"},[
_c("p",{staticClass:"score-home mimic-h1 mb-1"},[
_vm._v("1")]),

_vm._v(" "),
_c("p",{staticClass:"team-name mb-0"},[
_vm._v("Comber Star II")])]),


_vm._v(" "),
_c("div",{staticClass:"score-middle col-2 text-center"},[
_c(
"p",
{
staticClass:
"text-primary text-uppercase letter-spacing mb-0 pt-4"},

[_c("b",[_vm._v("FT")])])]),


_vm._v(" "),
_c("div",{staticClass:"score col-5"},[
_c("p",{staticClass:"score-home mimic-h1 mb-1"},[
_vm._v("4")]),

_vm._v(" "),
_c("p",{staticClass:"team-name mb-0"},[
_vm._v("Newtown Forest II")])])]),



_vm._v(" "),
_c("div",{staticClass:"row"},[
_c("div",{staticClass:"col-5"},[
_c("p",{staticClass:"goalscorers text-smallest"})]),

_vm._v(" "),
_c("div",{staticClass:"col-5 offset-2"},[
_c("p",{staticClass:"goalscorers text-smallest"},[
_vm._v("Phil Tanser (30, 80)"),
_c("br"),
_vm._v("Matthew McCarrison (20)"),
_c("br"),
_vm._v("Darren O'Hara (87)")])])])]),




_vm._v(" "),
_c("div",{staticClass:"links"},[
_c(
"p",
{
staticClass:
"text-smallest text-uppercase text-primary letter-spacing mb-0"},

[_vm._v("Second Team")]),

_vm._v(" "),
_c(
"p",
{
staticClass:
"text-smallest text-uppercase text-white letter-spacing"},

[_vm._v("DOWN AREA WINTER FOOTBALL LEAGUE")]),

_vm._v(" "),
_c("button",{staticClass:"btn btn-primary"},[
_vm._v("Read Match Report")])])])])]);






}];

_render2._withStripped=true;



/***/}}]);

}());
