(function () {
'use strict';

var fails = function (exec) {
  try {
    return !!exec();
  } catch (error) {
    return true;
  }
};

// Detect IE8's incomplete defineProperty implementation
var descriptors = !fails(function () {
  // eslint-disable-next-line es/no-object-defineproperty -- required for testing
  return Object.defineProperty({}, 1, { get: function () { return 7; } })[1] != 7;
});

var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var check = function (it) {
  return it && it.Math == Math && it;
};

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global_1 =
  // eslint-disable-next-line es/no-global-this -- safe
  check(typeof globalThis == 'object' && globalThis) ||
  check(typeof window == 'object' && window) ||
  // eslint-disable-next-line no-restricted-globals -- safe
  check(typeof self == 'object' && self) ||
  check(typeof commonjsGlobal == 'object' && commonjsGlobal) ||
  // eslint-disable-next-line no-new-func -- fallback
  (function () { return this; })() || Function('return this')();

var isObject = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};

var document$1 = global_1.document;
// typeof document.createElement is 'object' in old IE
var EXISTS = isObject(document$1) && isObject(document$1.createElement);

var documentCreateElement = function (it) {
  return EXISTS ? document$1.createElement(it) : {};
};

// Thank's IE8 for his funny defineProperty
var ie8DomDefine = !descriptors && !fails(function () {
  // eslint-disable-next-line es/no-object-defineproperty -- requied for testing
  return Object.defineProperty(documentCreateElement('div'), 'a', {
    get: function () { return 7; }
  }).a != 7;
});

var anObject = function (it) {
  if (!isObject(it)) {
    throw TypeError(String(it) + ' is not an object');
  } return it;
};

var aFunction = function (variable) {
  return typeof variable == 'function' ? variable : undefined;
};

var getBuiltIn = function (namespace, method) {
  return arguments.length < 2 ? aFunction(global_1[namespace]) : global_1[namespace] && global_1[namespace][method];
};

var engineUserAgent = getBuiltIn('navigator', 'userAgent') || '';

var process = global_1.process;
var Deno = global_1.Deno;
var versions = process && process.versions || Deno && Deno.version;
var v8 = versions && versions.v8;
var match, version;

if (v8) {
  match = v8.split('.');
  version = match[0] < 4 ? 1 : match[0] + match[1];
} else if (engineUserAgent) {
  match = engineUserAgent.match(/Edge\/(\d+)/);
  if (!match || match[1] >= 74) {
    match = engineUserAgent.match(/Chrome\/(\d+)/);
    if (match) version = match[1];
  }
}

var engineV8Version = version && +version;

/* eslint-disable es/no-symbol -- required for testing */



// eslint-disable-next-line es/no-object-getownpropertysymbols -- required for testing
var nativeSymbol = !!Object.getOwnPropertySymbols && !fails(function () {
  var symbol = Symbol();
  // Chrome 38 Symbol has incorrect toString conversion
  // `get-own-property-symbols` polyfill symbols converted to object are not Symbol instances
  return !String(symbol) || !(Object(symbol) instanceof Symbol) ||
    // Chrome 38-40 symbols are not inherited from DOM collections prototypes to instances
    !Symbol.sham && engineV8Version && engineV8Version < 41;
});

/* eslint-disable es/no-symbol -- required for testing */


var useSymbolAsUid = nativeSymbol
  && !Symbol.sham
  && typeof Symbol.iterator == 'symbol';

var isSymbol = useSymbolAsUid ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  var $Symbol = getBuiltIn('Symbol');
  return typeof $Symbol == 'function' && Object(it) instanceof $Symbol;
};

// `OrdinaryToPrimitive` abstract operation
// https://tc39.es/ecma262/#sec-ordinarytoprimitive
var ordinaryToPrimitive = function (input, pref) {
  var fn, val;
  if (pref === 'string' && typeof (fn = input.toString) == 'function' && !isObject(val = fn.call(input))) return val;
  if (typeof (fn = input.valueOf) == 'function' && !isObject(val = fn.call(input))) return val;
  if (pref !== 'string' && typeof (fn = input.toString) == 'function' && !isObject(val = fn.call(input))) return val;
  throw TypeError("Can't convert object to primitive value");
};

var setGlobal = function (key, value) {
  try {
    // eslint-disable-next-line es/no-object-defineproperty -- safe
    Object.defineProperty(global_1, key, { value: value, configurable: true, writable: true });
  } catch (error) {
    global_1[key] = value;
  } return value;
};

var SHARED = '__core-js_shared__';
var store = global_1[SHARED] || setGlobal(SHARED, {});

var sharedStore = store;

var shared = createCommonjsModule(function (module) {
(module.exports = function (key, value) {
  return sharedStore[key] || (sharedStore[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: '3.17.3',
  mode:  'global',
  copyright: '© 2021 Denis Pushkarev (zloirock.ru)'
});
});

// `RequireObjectCoercible` abstract operation
// https://tc39.es/ecma262/#sec-requireobjectcoercible
var requireObjectCoercible = function (it) {
  if (it == undefined) throw TypeError("Can't call method on " + it);
  return it;
};

// `ToObject` abstract operation
// https://tc39.es/ecma262/#sec-toobject
var toObject = function (argument) {
  return Object(requireObjectCoercible(argument));
};

var hasOwnProperty = {}.hasOwnProperty;

var has = Object.hasOwn || function hasOwn(it, key) {
  return hasOwnProperty.call(toObject(it), key);
};

var id = 0;
var postfix = Math.random();

var uid = function (key) {
  return 'Symbol(' + String(key === undefined ? '' : key) + ')_' + (++id + postfix).toString(36);
};

var WellKnownSymbolsStore = shared('wks');
var Symbol$1 = global_1.Symbol;
var createWellKnownSymbol = useSymbolAsUid ? Symbol$1 : Symbol$1 && Symbol$1.withoutSetter || uid;

var wellKnownSymbol = function (name) {
  if (!has(WellKnownSymbolsStore, name) || !(nativeSymbol || typeof WellKnownSymbolsStore[name] == 'string')) {
    if (nativeSymbol && has(Symbol$1, name)) {
      WellKnownSymbolsStore[name] = Symbol$1[name];
    } else {
      WellKnownSymbolsStore[name] = createWellKnownSymbol('Symbol.' + name);
    }
  } return WellKnownSymbolsStore[name];
};

var TO_PRIMITIVE = wellKnownSymbol('toPrimitive');

// `ToPrimitive` abstract operation
// https://tc39.es/ecma262/#sec-toprimitive
var toPrimitive = function (input, pref) {
  if (!isObject(input) || isSymbol(input)) return input;
  var exoticToPrim = input[TO_PRIMITIVE];
  var result;
  if (exoticToPrim !== undefined) {
    if (pref === undefined) pref = 'default';
    result = exoticToPrim.call(input, pref);
    if (!isObject(result) || isSymbol(result)) return result;
    throw TypeError("Can't convert object to primitive value");
  }
  if (pref === undefined) pref = 'number';
  return ordinaryToPrimitive(input, pref);
};

// `ToPropertyKey` abstract operation
// https://tc39.es/ecma262/#sec-topropertykey
var toPropertyKey = function (argument) {
  var key = toPrimitive(argument, 'string');
  return isSymbol(key) ? key : String(key);
};

// eslint-disable-next-line es/no-object-defineproperty -- safe
var $defineProperty = Object.defineProperty;

// `Object.defineProperty` method
// https://tc39.es/ecma262/#sec-object.defineproperty
var f = descriptors ? $defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPropertyKey(P);
  anObject(Attributes);
  if (ie8DomDefine) try {
    return $defineProperty(O, P, Attributes);
  } catch (error) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};

var objectDefineProperty = {
	f: f
};

var defineProperty = objectDefineProperty.f;

var FunctionPrototype = Function.prototype;
var FunctionPrototypeToString = FunctionPrototype.toString;
var nameRE = /^\s*function ([^ (]*)/;
var NAME = 'name';

// Function instances `.name` property
// https://tc39.es/ecma262/#sec-function-instances-name
if (descriptors && !(NAME in FunctionPrototype)) {
  defineProperty(FunctionPrototype, NAME, {
    configurable: true,
    get: function () {
      try {
        return FunctionPrototypeToString.call(this).match(nameRE)[1];
      } catch (error) {
        return '';
      }
    }
  });
}

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["ContactGuide"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Contact/ContactGuide.vue?vue&type=script&lang=js&":



function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsContactContactGuideVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
props:{
recaptcha:String,
page:String,
about:String},

data:function data(){
return {
fields:{
country:""},

errors:{},
success:false,
loaded:true};

},
mounted:function mounted(){
$.getScript("https://www.google.com/recaptcha/api.js");
},
methods:{
messageSent:function messageSent(){
this.fields={};
},
changeCountry:function changeCountry(value){
this.fields.country=value;
},
submit:function submit(){
var _this=this;

if(this.loaded&&grecaptcha.getResponse().length>0){
$('#recaptcha-error').html('');
this.loaded=false;
this.success=false;
this.errors={};
document.getElementById("loader").classList.add("on");
document.getElementById("loader-message").innerHTML="Sending Enquiry";
axios.post('/send-message',this.fields).then(function(response){
// Empty fields and enable success message
grecaptcha.reset();
_this.loaded=true;
_this.success=true;

_this.messageSent();

document.getElementById("loader-message").innerHTML="Enquiry sent successfully";
$('#close-loader-btn').removeClass('d-none').addClass('d-block');
$('#loader-roller').removeClass('d-block').addClass('d-none');
$('#loader-success').removeClass('d-none').addClass('d-block');
$('#enquire_modal').modal('hide');
})["catch"](function(error){
// Error handling
console.log(error.response);
_this.loaded=true;

if(error.response.status===422){
_this.errors=error.response.data.errors||{};
document.getElementById("loader-message").innerHTML="Check form for errors";
document.getElementById("loader").classList.remove("on");
}
});
}else {
$('#recaptcha-error').html('Please confirm you are not a robot.');
}
}}};



/***/},

/***/"./resources/js/components/Contact/ContactGuide.vue":



function resourcesJsComponentsContactContactGuideVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _ContactGuide_vue_vue_type_template_id_15be13d6___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./ContactGuide.vue?vue&type=template&id=15be13d6& */"./resources/js/components/Contact/ContactGuide.vue?vue&type=template&id=15be13d6&");
/* harmony import */var _ContactGuide_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./ContactGuide.vue?vue&type=script&lang=js& */"./resources/js/components/Contact/ContactGuide.vue?vue&type=script&lang=js&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
_ContactGuide_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
_ContactGuide_vue_vue_type_template_id_15be13d6___WEBPACK_IMPORTED_MODULE_0__.render,
_ContactGuide_vue_vue_type_template_id_15be13d6___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/Contact/ContactGuide.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Contact/ContactGuide.vue?vue&type=script&lang=js&":



function resourcesJsComponentsContactContactGuideVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactGuide_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ContactGuide.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Contact/ContactGuide.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactGuide_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"];

/***/},

/***/"./resources/js/components/Contact/ContactGuide.vue?vue&type=template&id=15be13d6&":



function resourcesJsComponentsContactContactGuideVueVueTypeTemplateId15be13d6(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactGuide_vue_vue_type_template_id_15be13d6___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactGuide_vue_vue_type_template_id_15be13d6___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactGuide_vue_vue_type_template_id_15be13d6___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ContactGuide.vue?vue&type=template&id=15be13d6& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Contact/ContactGuide.vue?vue&type=template&id=15be13d6&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Contact/ContactGuide.vue?vue&type=template&id=15be13d6&":



function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsContactContactGuideVueVueTypeTemplateId15be13d6(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"form",
{
staticClass:"row half_row",
attrs:{id:"contact-page-form"},
on:{
submit:function submit($event){
$event.preventDefault();
return _vm.submit.apply(null,arguments);
}}},


[
_c("div",{staticClass:"form-group col-md-6 half_col"},[
_c("input",{
directives:[
{
name:"model",
rawName:"v-model",
value:_vm.fields.name,
expression:"fields.name"}],


staticClass:"form-control",
attrs:{
id:"name",
type:"text",
name:"name",
placeholder:"Name*"},

domProps:{value:_vm.fields.name},
on:{
input:function input($event){
if($event.target.composing){
return;
}
_vm.$set(_vm.fields,"name",$event.target.value);
}}}),


_vm._v(" "),
_vm.errors&&_vm.errors.name?
_c("div",{staticClass:"text-primary"},[
_vm._v(_vm._s(_vm.errors.name[0]))]):

_vm._e()]),

_vm._v(" "),
_c("div",{staticClass:"form-group col-md-6 half_col"},[
_c("input",{
directives:[
{
name:"model",
rawName:"v-model",
value:_vm.fields.phone,
expression:"fields.phone"}],


staticClass:"form-control",
attrs:{
id:"phone",
type:"text",
name:"phone",
placeholder:"Phone"},

domProps:{value:_vm.fields.phone},
on:{
input:function input($event){
if($event.target.composing){
return;
}
_vm.$set(_vm.fields,"phone",$event.target.value);
}}}),


_vm._v(" "),
_vm.errors&&_vm.errors.phone?
_c("div",{staticClass:"text-primary"},[
_vm._v(_vm._s(_vm.errors.phone[0]))]):

_vm._e()]),

_vm._v(" "),
_c("div",{staticClass:"form-group col-md-6 half_col"},[
_c("input",{
directives:[
{
name:"model",
rawName:"v-model",
value:_vm.fields.email,
expression:"fields.email"}],


staticClass:"form-control",
attrs:{
id:"email",
type:"email",
name:"email",
placeholder:"Email Address*"},

domProps:{value:_vm.fields.email},
on:{
input:function input($event){
if($event.target.composing){
return;
}
_vm.$set(_vm.fields,"email",$event.target.value);
}}}),


_vm._v(" "),
_vm.errors&&_vm.errors.email?
_c("div",{staticClass:"text-primary"},[
_vm._v(_vm._s(_vm.errors.email[0]))]):

_vm._e()]),

_vm._v(" "),
_c("div",{staticClass:"form-group col-md-6 half_col"},[
_c("input",{
directives:[
{
name:"model",
rawName:"v-model",
value:_vm.fields.date,
expression:"fields.date"}],


staticClass:"form-control",
attrs:{
id:"date",
type:"date",
name:"date",
placeholder:"Select Date*"},

domProps:{value:_vm.fields.date},
on:{
input:function input($event){
if($event.target.composing){
return;
}
_vm.$set(_vm.fields,"date",$event.target.value);
}}}),


_vm._v(" "),
_vm.errors&&_vm.errors.date?
_c("div",{staticClass:"text-primary"},[
_vm._v(_vm._s(_vm.errors.date[0]))]):

_vm._e()]),

_vm._v(" "),
_c("div",{staticClass:"form-group col-12 half_col"},[
_c("textarea",{
directives:[
{
name:"model",
rawName:"v-model",
value:_vm.fields.message,
expression:"fields.message"}],


staticClass:"form-control",
attrs:{
id:"message",
type:"text",
name:"message",
placeholder:
"Please give a description of the type of tour you wish to go on and any further information required for the tour. e.g language type, date flexibility, wheelchair access and preferred times…",
rows:"4"},

domProps:{value:_vm.fields.message},
on:{
input:function input($event){
if($event.target.composing){
return;
}
_vm.$set(_vm.fields,"message",$event.target.value);
}}}),


_vm._v(" "),
_vm.errors&&_vm.errors.message?
_c("div",{staticClass:"text-primary"},[
_vm._v(_vm._s(_vm.errors.message[0]))]):

_vm._e()]),

_vm._v(" "),
_vm._m(0),
_vm._v(" "),
_c("div",{staticClass:"col-md-6 half_col"},[
_c("div",{
staticClass:"text-primary text-left",
attrs:{id:"recaptcha-error"}}),

_vm._v(" "),
_c("div",{
staticClass:"g-recaptcha mb-3",
staticStyle:{
transform:"scale(0.77)",
"-webkit-transform":"scale(0.77)",
"transform-origin":"0 0",
"-webkit-transform-origin":"0 0"},

attrs:{
"data-sitekey":this.$props.recaptcha,
"data-theme":"light"}})]),



_vm._v(" "),
_vm._m(1)]);


};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"col-12 half_col mb-4"},[
_c("div",{staticClass:"form-check"},[
_c("input",{
staticClass:"form-check-input",
attrs:{type:"checkbox",value:"",id:"consent"}}),

_vm._v(" "),
_c(
"label",
{staticClass:"form-check-label",attrs:{for:"consent"}},
[
_vm._v(
"\n\t\t    Please tick the box to consent to Tour Guides NI storing and using the data collected in this form for reasons detailed in the Terms & Conditions. I have read and agree to the Terms & Conditions and Privacy Policy.\n\n\t\t  ")])])]);





},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"col-md-6 half_col"},[
_c("div",{staticClass:"row"},[
_c("div",{staticClass:"col-12 text-center text-lg-right"},[
_c(
"button",
{
staticClass:"btn btn-primary btn-icon",
attrs:{id:"submit",type:"submit"}},

[
_vm._v("Send message "),
_c("i",{staticClass:"fa fa-envelope"})])])])]);





}];

_render._withStripped=true;



/***/}}]);

}());
