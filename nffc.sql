-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Sep 24, 2021 at 06:23 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nffc`
--

-- --------------------------------------------------------

--
-- Table structure for table `action_events`
--

CREATE TABLE `action_events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actionable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actionable_id` bigint(20) UNSIGNED NOT NULL,
  `target_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED DEFAULT NULL,
  `fields` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'running',
  `exception` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `original` mediumtext COLLATE utf8mb4_unicode_ci,
  `changes` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `action_events`
--

INSERT INTO `action_events` (`id`, `batch_id`, `user_id`, `name`, `actionable_type`, `actionable_id`, `target_type`, `target_id`, `model_type`, `model_id`, `fields`, `status`, `exception`, `created_at`, `updated_at`, `original`, `changes`) VALUES
(3, '9477bd3c-cb9f-482f-8708-e8af54923bd0', 1, 'Update', 'App\\Models\\Profile', 330, 'App\\Models\\Profile', 330, 'App\\Models\\Profile', 330, '', 'finished', '', '2021-09-23 16:59:28', '2021-09-23 16:59:28', '[]', '[]'),
(4, '9477bd85-b869-40b2-97c0-35e1dddb3cb5', 1, 'Update', 'App\\Models\\Profile', 299, 'App\\Models\\Profile', 299, 'App\\Models\\Profile', 299, '', 'finished', '', '2021-09-23 17:00:16', '2021-09-23 17:00:16', '[]', '[]'),
(5, '9477bdac-bcf8-419d-b45a-da97521ec9f6', 1, 'Update', 'App\\Models\\Profile', 234, 'App\\Models\\Profile', 234, 'App\\Models\\Profile', 234, '', 'finished', '', '2021-09-23 17:00:42', '2021-09-23 17:00:42', '[]', '[]'),
(6, '9477bdc8-ab60-4fa4-9b81-605c933da824', 1, 'Update', 'App\\Models\\Profile', 232, 'App\\Models\\Profile', 232, 'App\\Models\\Profile', 232, '', 'finished', '', '2021-09-23 17:01:00', '2021-09-23 17:01:00', '[]', '[]'),
(7, '9477bde4-b5a8-4b87-8b64-943c422cc513', 1, 'Update', 'App\\Models\\Profile', 296, 'App\\Models\\Profile', 296, 'App\\Models\\Profile', 296, '', 'finished', '', '2021-09-23 17:01:19', '2021-09-23 17:01:19', '[]', '[]'),
(8, '9477be01-5288-4590-aa33-8e5cca43e844', 1, 'Update', 'App\\Models\\Profile', 179, 'App\\Models\\Profile', 179, 'App\\Models\\Profile', 179, '', 'finished', '', '2021-09-23 17:01:37', '2021-09-23 17:01:37', '[]', '[]'),
(9, '9477be50-a4a7-4190-80ab-57292fbc63ae', 1, 'Update', 'App\\Models\\Profile', 117, 'App\\Models\\Profile', 117, 'App\\Models\\Profile', 117, '', 'finished', '', '2021-09-23 17:02:29', '2021-09-23 17:02:29', '{\"position\":null}', '{\"position\":\"Midfielder\"}'),
(10, '9477be6d-2198-4647-b543-24b120249e25', 1, 'Update', 'App\\Models\\Profile', 154, 'App\\Models\\Profile', 154, 'App\\Models\\Profile', 154, '', 'finished', '', '2021-09-23 17:02:48', '2021-09-23 17:02:48', '[]', '[]'),
(11, '9477beab-ef23-44e6-bc62-688d8dddf86d', 1, 'Update', 'App\\Models\\Profile', 109, 'App\\Models\\Profile', 109, 'App\\Models\\Profile', 109, '', 'finished', '', '2021-09-23 17:03:29', '2021-09-23 17:03:29', '{\"position\":null}', '{\"position\":\"Midfielder\"}'),
(12, '9477bed5-ea47-407a-aa0f-9c7547338cd2', 1, 'Update', 'App\\Models\\Profile', 261, 'App\\Models\\Profile', 261, 'App\\Models\\Profile', 261, '', 'finished', '', '2021-09-23 17:03:57', '2021-09-23 17:03:57', '[]', '[]'),
(13, '9477befa-508f-46dd-9edf-5bca42c1f3da', 1, 'Update', 'App\\Models\\Profile', 17, 'App\\Models\\Profile', 17, 'App\\Models\\Profile', 17, '', 'finished', '', '2021-09-23 17:04:20', '2021-09-23 17:04:20', '[]', '[]'),
(14, '9477bf12-2512-4ada-bd4d-f4a944fecb93', 1, 'Update', 'App\\Models\\Profile', 189, 'App\\Models\\Profile', 189, 'App\\Models\\Profile', 189, '', 'finished', '', '2021-09-23 17:04:36', '2021-09-23 17:04:36', '[]', '[]'),
(15, '9477bf3d-0577-4da9-a6f1-b62732d50e4f', 1, 'Update', 'App\\Models\\Profile', 246, 'App\\Models\\Profile', 246, 'App\\Models\\Profile', 246, '', 'finished', '', '2021-09-23 17:05:04', '2021-09-23 17:05:04', '{\"position\":null}', '{\"position\":\"Midfielder\"}'),
(16, '9477bf82-0fd9-44b2-ac61-b3d1d8b37286', 1, 'Update', 'App\\Models\\Profile', 272, 'App\\Models\\Profile', 272, 'App\\Models\\Profile', 272, '', 'finished', '', '2021-09-23 17:05:49', '2021-09-23 17:05:49', '[]', '[]'),
(17, '9479addf-3a79-404d-af3b-98fb728c3fc8', 1, 'Create', 'App\\Models\\Team', 1, 'App\\Models\\Team', 1, 'App\\Models\\Team', 1, '', 'finished', '', '2021-09-24 16:08:10', '2021-09-24 16:08:10', NULL, '{\"name\":\"Newtown Forest\",\"nickname\":\"First Team\",\"slug\":\"newtown-forest\",\"updated_at\":\"2021-09-24T17:08:10.000000Z\",\"created_at\":\"2021-09-24T17:08:10.000000Z\",\"id\":1}'),
(18, '9479adf6-a515-4139-9be6-2ab6f9f137e9', 1, 'Create', 'App\\Models\\Team', 2, 'App\\Models\\Team', 2, 'App\\Models\\Team', 2, '', 'finished', '', '2021-09-24 16:08:25', '2021-09-24 16:08:25', NULL, '{\"name\":\"Newtown Forest II\",\"nickname\":\"Second Team\",\"slug\":\"newtown-forest-ii\",\"updated_at\":\"2021-09-24T17:08:25.000000Z\",\"created_at\":\"2021-09-24T17:08:25.000000Z\",\"id\":2}'),
(19, '9479ae07-d9cb-438b-a131-1410b26ee8a2', 1, 'Create', 'App\\Models\\Team', 3, 'App\\Models\\Team', 3, 'App\\Models\\Team', 3, '', 'finished', '', '2021-09-24 16:08:37', '2021-09-24 16:08:37', NULL, '{\"name\":\"Newtown Forest III\",\"nickname\":\"Third Team\",\"slug\":\"newtown-forest-iii\",\"updated_at\":\"2021-09-24T17:08:37.000000Z\",\"created_at\":\"2021-09-24T17:08:37.000000Z\",\"id\":3}'),
(20, '9479ae82-4e2d-4521-affc-100a5a9342a0', 1, 'Create', 'App\\Models\\Competition', 1, 'App\\Models\\Competition', 1, 'App\\Models\\Competition', 1, '', 'finished', '', '2021-09-24 16:09:57', '2021-09-24 16:09:57', NULL, '{\"name\":\"Down Area Winter Football League\",\"slug\":\"down-area-winter-football-league\",\"updated_at\":\"2021-09-24T17:09:57.000000Z\",\"created_at\":\"2021-09-24T17:09:57.000000Z\",\"id\":1}'),
(21, '9479aebb-608c-4d05-827c-070b7f18c278', 1, 'Create', 'App\\Models\\Competition', 2, 'App\\Models\\Competition', 2, 'App\\Models\\Competition', 2, '', 'finished', '', '2021-09-24 16:10:34', '2021-09-24 16:10:34', NULL, '{\"name\":\"Down Area Winter Football League - Reserve Division 1\",\"slug\":\"down-area-winter-football-league-reserve-division-1\",\"updated_at\":\"2021-09-24T17:10:34.000000Z\",\"created_at\":\"2021-09-24T17:10:34.000000Z\",\"id\":2}'),
(22, '9479aee5-3836-4a02-b5b1-fd26efbd6817', 1, 'Update', 'App\\Models\\Competition', 2, 'App\\Models\\Competition', 2, 'App\\Models\\Competition', 2, '', 'finished', '', '2021-09-24 16:11:02', '2021-09-24 16:11:02', '{\"name\":\"Down Area Winter Football League - Reserve Division 1\"}', '{\"name\":\"DAWFL - Reserve Division 1\"}'),
(23, '9479aefd-e9b7-439c-9f93-f7614bbee5f9', 1, 'Update', 'App\\Models\\Competition', 1, 'App\\Models\\Competition', 1, 'App\\Models\\Competition', 1, '', 'finished', '', '2021-09-24 16:11:18', '2021-09-24 16:11:18', '{\"name\":\"Down Area Winter Football League\"}', '{\"name\":\"DAWFL - Premier Division\"}'),
(24, '9479af59-e251-4bec-80b9-279d154dab52', 1, 'Create', 'App\\Models\\Game', 1, 'App\\Models\\Game', 1, 'App\\Models\\Game', 1, '', 'finished', '', '2021-09-24 16:12:18', '2021-09-24 16:12:18', NULL, '{\"opposition\":\"Lisburn Rovers\",\"location\":\"Wallace Park\",\"city\":\"Lisburn\",\"home_away\":\"Away\",\"time\":\"14:00\",\"date\":\"2021-09-25\",\"competition_id\":1,\"team_id\":3,\"updated_at\":\"2021-09-24T17:12:18.000000Z\",\"created_at\":\"2021-09-24T17:12:18.000000Z\",\"id\":1}'),
(25, '9479afd2-542a-49b8-907f-16e47e5c5c28', 1, 'Create', 'App\\Models\\Game', 2, 'App\\Models\\Game', 2, 'App\\Models\\Game', 2, '', 'finished', '', '2021-09-24 16:13:37', '2021-09-24 16:13:37', NULL, '{\"opposition\":\"Vision Athletic\",\"location\":\"Londonderry Park\",\"city\":\"Newtownards\",\"home_away\":\"Away\",\"time\":\"14:00\",\"date\":\"2021-09-25T00:00:00.000000Z\",\"competition_id\":1,\"team_id\":1,\"updated_at\":\"2021-09-24T17:13:37.000000Z\",\"created_at\":\"2021-09-24T17:13:37.000000Z\",\"id\":2}'),
(26, '9479b05f-854d-48b5-b4ea-3f293b37903a', 1, 'Create', 'App\\Models\\Competition', 3, 'App\\Models\\Competition', 3, 'App\\Models\\Competition', 3, '', 'finished', '', '2021-09-24 16:15:10', '2021-09-24 16:15:10', NULL, '{\"name\":\"Belfast & District Football League\",\"slug\":\"belfast-district-football-league\",\"updated_at\":\"2021-09-24T17:15:10.000000Z\",\"created_at\":\"2021-09-24T17:15:10.000000Z\",\"id\":3}'),
(27, '9479b0ed-386a-4935-b609-13263a8aab23', 1, 'Update', 'App\\Models\\Game', 1, 'App\\Models\\Game', 1, 'App\\Models\\Game', 1, '', 'finished', '', '2021-09-24 16:16:42', '2021-09-24 16:16:42', '{\"competition_id\":1}', '{\"competition_id\":3}');

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `colour` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `player_id` bigint(20) UNSIGNED DEFAULT NULL,
  `game_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category_post`
--

CREATE TABLE `category_post` (
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `competitions`
--

CREATE TABLE `competitions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `competitions`
--

INSERT INTO `competitions` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'DAWFL - Premier Division', 'dawfl-premier-division', '2021-09-24 16:09:57', '2021-09-24 16:11:18'),
(2, 'DAWFL - Reserve Division 1', 'dawfl-reserve-division-1', '2021-09-24 16:10:34', '2021-09-24 16:11:02'),
(3, 'Belfast & District Football League', 'belfast-district-football-league', '2021-09-24 16:15:10', '2021-09-24 16:15:10');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE `games` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `opposition` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_away` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `competition_id` bigint(20) UNSIGNED NOT NULL,
  `team_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`id`, `opposition`, `location`, `city`, `home_away`, `date`, `time`, `competition_id`, `team_id`, `created_at`, `updated_at`) VALUES
(1, 'Lisburn Rovers', 'Wallace Park', 'Lisburn', 'Away', '2021-09-25', '14:00', 3, 3, '2021-09-24 16:12:18', '2021-09-24 16:16:42'),
(2, 'Vision Athletic', 'Londonderry Park', 'Newtownards', 'Away', '2021-09-25', '14:00', 1, 1, '2021-09-24 16:13:37', '2021-09-24 16:13:37');

-- --------------------------------------------------------

--
-- Table structure for table `goals`
--

CREATE TABLE `goals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scorer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `assisted_id` bigint(20) UNSIGNED DEFAULT NULL,
  `game_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint(20) UNSIGNED NOT NULL,
  `manipulations` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `generated_conversions` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsive_images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `uuid`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `conversions_disk`, `size`, `manipulations`, `custom_properties`, `generated_conversions`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(3, 'App\\Models\\Profile', 330, 'd0b687a8-558f-4b76-9d05-171be0c3071a', 'profiles', 'luke', 'luke.jpg', 'image/jpeg', 'public', 'public', 223687, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 1, '2021-09-23 16:59:27', '2021-09-23 16:59:28'),
(4, 'App\\Models\\Profile', 299, '005b0691-6ef3-49e9-b821-7fc39639eb9d', 'profiles', '_20210918_180114', '_20210918_180114.JPG', 'image/jpeg', 'public', 'public', 230348, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 2, '2021-09-23 17:00:15', '2021-09-23 17:00:16'),
(5, 'App\\Models\\Profile', 234, 'd6f3278b-d739-493d-a34a-c37788b66cae', 'profiles', '_20210918_180032', '_20210918_180032.JPG', 'image/jpeg', 'public', 'public', 229176, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 3, '2021-09-23 17:00:40', '2021-09-23 17:00:42'),
(6, 'App\\Models\\Profile', 232, '4626ab4b-9f86-4e0e-aa13-535a2a08f371', 'profiles', '_20210918_180023', '_20210918_180023.JPG', 'image/jpeg', 'public', 'public', 203718, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 4, '2021-09-23 17:00:59', '2021-09-23 17:01:00'),
(7, 'App\\Models\\Profile', 296, '1361e6cd-9e29-4a67-9649-67a62eab9be8', 'profiles', '_20210918_180005', '_20210918_180005.JPG', 'image/jpeg', 'public', 'public', 254142, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 5, '2021-09-23 17:01:17', '2021-09-23 17:01:19'),
(8, 'App\\Models\\Profile', 179, 'acd85bea-151d-4060-84ff-6af92e20a02c', 'profiles', '_20210918_175954', '_20210918_175954.JPG', 'image/jpeg', 'public', 'public', 245115, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 6, '2021-09-23 17:01:36', '2021-09-23 17:01:37'),
(9, 'App\\Models\\Profile', 117, '972d597c-2b3c-492d-b4c5-fff46ea94519', 'profiles', '_20210918_175944', '_20210918_175944.JPG', 'image/jpeg', 'public', 'public', 225969, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 7, '2021-09-23 17:02:28', '2021-09-23 17:02:29'),
(10, 'App\\Models\\Profile', 154, '43341a1d-134f-4ec9-9954-8977f983cfd7', 'profiles', '_20210918_175931', '_20210918_175931.JPG', 'image/jpeg', 'public', 'public', 236344, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 8, '2021-09-23 17:02:46', '2021-09-23 17:02:48'),
(11, 'App\\Models\\Profile', 109, '017dadbf-aeeb-41a3-9715-9e9b65e19b2a', 'profiles', '_20210911_175024.JPG', '_20210911_175024.JPG.jpg', 'image/jpeg', 'public', 'public', 428323, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 9, '2021-09-23 17:03:27', '2021-09-23 17:03:29'),
(12, 'App\\Models\\Profile', 261, '42ac6a0d-f7b1-49c8-80bf-950c951ec3c5', 'profiles', '_20210911_175041.JPG', '_20210911_175041.JPG.jpg', 'image/jpeg', 'public', 'public', 398368, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 10, '2021-09-23 17:03:55', '2021-09-23 17:03:57'),
(13, 'App\\Models\\Profile', 17, '235e0898-0c84-4d29-b1b9-53bbcdbe5b05', 'profiles', '_20210911_175051.JPG', '_20210911_175051.JPG.jpg', 'image/jpeg', 'public', 'public', 247405, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 11, '2021-09-23 17:04:19', '2021-09-23 17:04:20'),
(14, 'App\\Models\\Profile', 189, '7f7cdff9-9a9f-4f03-ac40-de2faaec37aa', 'profiles', '_20210911_175110.JPG', '_20210911_175110.JPG.jpg', 'image/jpeg', 'public', 'public', 380858, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 12, '2021-09-23 17:04:34', '2021-09-23 17:04:36'),
(15, 'App\\Models\\Profile', 246, 'f493ad9a-7c7d-41ba-8099-9c8c04a2fcc7', 'profiles', '_20210911_175130.JPG', '_20210911_175130.JPG.jpg', 'image/jpeg', 'public', 'public', 449176, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 13, '2021-09-23 17:05:02', '2021-09-23 17:05:04'),
(16, 'App\\Models\\Profile', 272, '5f090ec4-94fa-4fbd-b680-b79d0ba8c3c7', 'profiles', '_20210911_175226.JPG', '_20210911_175226.JPG.jpg', 'image/jpeg', 'public', 'public', 469244, '[]', '[]', '{\"normal\":true,\"normal-webp\":true,\"thumbnail\":true,\"mob\":true,\"mob-webp\":true}', '[]', 14, '2021-09-23 17:05:47', '2021-09-23 17:05:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_08_13_125502_create_roles_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2018_01_01_000000_create_action_events_table', 1),
(5, '2019_05_10_000000_add_fields_to_action_events_table', 1),
(6, '2019_08_19_000000_create_failed_jobs_table', 1),
(7, '2021_08_13_125545_nova_pending_trix_attachments', 1),
(8, '2021_08_13_125621_create_media_table', 1),
(9, '2021_08_13_125649_create_categories_table', 1),
(26, '2021_08_13_125656_create_posts_table', 2),
(27, '2021_08_13_125726_create_category_post_pivot', 2),
(28, '2021_08_13_125746_create_profiles_table', 2),
(37, '2021_07_20_110931_create_froala_attachment_tables', 3),
(38, '2021_08_13_130000_create_teams_table', 3),
(39, '2021_08_13_130002_create_competitions_table', 3),
(40, '2021_08_13_130130_create_games_table', 3),
(41, '2021_08_13_130612_create_reports_table', 3),
(42, '2021_08_13_131042_create_goals_table', 3),
(43, '2021_08_13_131050_create_cards_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `nova_froala_attachments`
--

CREATE TABLE `nova_froala_attachments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attachable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachable_id` bigint(20) UNSIGNED NOT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nova_pending_froala_attachments`
--

CREATE TABLE `nova_pending_froala_attachments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `draft_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nova_pending_trix_attachments`
--

CREATE TABLE `nova_pending_trix_attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `draft_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nova_trix_attachments`
--

CREATE TABLE `nova_trix_attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `attachable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachable_id` int(10) UNSIGNED NOT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foot` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `joined` datetime DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `full_name`, `first_name`, `last_name`, `position`, `status`, `height`, `foot`, `dob`, `joined`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Andrew Rendall', 'Andrew', 'Rendall', NULL, 1, NULL, NULL, '2001-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(2, 'William Gillan', 'William', 'Gillan', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(3, 'Craig Murray', 'Craig', 'Murray', NULL, 1, NULL, NULL, '1980-03-08', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(4, 'Davy Galloway', 'Davy', 'Galloway', NULL, 1, NULL, NULL, '1970-12-28', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(5, 'Johnny Marley', 'Johnny', 'Marley', NULL, 1, NULL, NULL, '1971-04-27', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(6, 'Johnny Taggart', 'Johnny', 'Taggart', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(7, 'John Toal', 'John', 'Toal', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(8, 'Keith Hunter', 'Keith', 'Hunter', NULL, 1, NULL, NULL, '1979-06-19', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(9, 'Karl Irvine', 'Karl', 'Irvine', NULL, 1, NULL, NULL, '1978-01-17', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(10, 'Kyle Rendall', 'Kyle', 'Rendall', 'Midfielder', 1, NULL, NULL, '1979-04-03', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(11, 'Marc Butterwood', 'Marc', 'Butterwood', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(12, 'Phil Kernaghan', 'Phil', 'Kernaghan', NULL, 1, NULL, NULL, '1980-04-11', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(13, 'Ross McCaughrain', 'Ross', 'McCaughrain', NULL, 1, NULL, NULL, '1980-02-20', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(14, 'Stevie Armstrong', 'Stevie', 'Armstrong', NULL, 1, NULL, NULL, '1976-05-06', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(15, 'Stevie Campbell', 'Stevie', 'Campbell', NULL, 1, NULL, NULL, '2001-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(16, 'Stevie Robinson', 'Stevie', 'Robinson', NULL, 0, NULL, NULL, '1977-11-04', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(17, 'Tommy Moreland', 'Tommy', 'Moreland', 'Midfielder', 1, NULL, NULL, '1977-05-10', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(18, 'Jason Marley', 'Jason', 'Marley', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(19, 'Philip Donnan', 'Philip', 'Donnan', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(20, 'Kris Byers', 'Kris', 'Byers', NULL, 1, NULL, NULL, '1979-07-20', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(21, 'Mark Carlisle', 'Mark', 'Carlisle', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(22, 'Ryan Carr', 'Ryan', 'Carr', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(23, 'C. McGurk', 'C.', 'McGurk', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(25, 'Aaron Callaghan', 'Aaron', 'Callaghan', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(26, 'Michael Griffiths', 'Michael', 'Griffiths', NULL, 1, NULL, NULL, '1989-08-18', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(27, 'Michael Lavery', 'Michael', 'Lavery', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(28, 'Brian Hannon', 'Brian', 'Hannon', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(29, 'Barry Oliver', 'Barry', 'Oliver', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(30, 'Justin Hill', 'Justin', 'Hill', NULL, 1, NULL, NULL, '2001-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(32, 'James Dowson', 'James', 'Dowson', NULL, 1, NULL, NULL, '1988-05-12', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(33, 'David McCallan', 'David', 'McCallan', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(34, 'Scott Lupton', 'Scott', 'Lupton', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(35, 'Michael Wylie', 'Michael', 'Wylie', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(36, 'Mark Forsythe', 'Mark', 'Forsythe', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(37, 'Davey Shaw', 'Davey', 'Shaw', NULL, 1, NULL, NULL, '1976-05-24', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(38, 'Davy Neilson', 'Davy', 'Neilson', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(39, 'Michael Armstrong', 'Michael', 'Armstrong', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(40, 'Steffan Greer', 'Steffan', 'Greer', NULL, 1, NULL, NULL, '1978-12-16', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(41, 'Keith Fleming', 'Keith', 'Fleming', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(42, 'Chris Johnston', 'Chris', 'Johnston', NULL, 1, NULL, NULL, '1980-05-19', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(43, 'Matthew Matthew', 'Matthew', 'Matthew', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(44, 'David Hiroz', 'David', 'Hiroz', NULL, 1, NULL, NULL, '1977-07-18', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(46, 'Paul Robinson', 'Paul', 'Robinson', NULL, 1, NULL, NULL, '1980-11-28', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(47, 'Mark McDowell', 'Mark', 'McDowell', NULL, 1, NULL, NULL, '1989-02-24', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(48, 'Ivan Thompson', 'Ivan', 'Thompson', NULL, 1, NULL, NULL, '1979-07-10', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(49, 'Gregory Szum', 'Gregory', 'Szum', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(50, 'Paul Fleming', 'Paul', 'Fleming', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(51, 'Graeme Bailie', 'Graeme', 'Bailie', NULL, 1, NULL, NULL, '1981-08-16', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(52, 'James Davies', 'James', 'Davies', NULL, 1, NULL, NULL, '1985-01-15', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(53, 'Bryan Dallas', 'Bryan', 'Dallas', NULL, 1, NULL, NULL, '1986-01-27', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(54, 'Darren Floyd', 'Darren', 'Floyd', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(55, 'Joesph Hoff', 'Joesph', 'Hoff', NULL, 1, NULL, NULL, '1978-03-29', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(56, 'Luc Rock', 'Luc', 'Rock', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(57, 'Chris Galloway', 'Chris', 'Galloway', NULL, 1, NULL, NULL, '1989-08-15', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(58, 'Mark Trimmings', 'Mark', 'Trimmings', NULL, 1, NULL, NULL, '1981-11-17', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(59, 'Barry Miller', 'Barry', 'Miller', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(60, 'Brian Brian', 'Brian', 'Brian', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(61, 'Chris Chris', 'Chris', 'Chris', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(62, 'Hugh Hand', 'Hugh', 'Hand', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(63, 'Mick Foster', 'Mick', 'Foster', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(64, 'William McAllister', 'William', 'McAllister', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(65, 'Stephen Taylor', 'Stephen', 'Taylor', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(66, 'Michael Kitchen', 'Michael', 'Kitchen', NULL, 1, NULL, NULL, '1980-01-14', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(67, 'Justin Justin', 'Justin', 'Justin', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(68, 'Connor Phillips', 'Connor', 'Phillips', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(69, 'Steven Allen', 'Steven', 'Allen', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(70, 'Gary Allen', 'Gary', 'Allen', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(71, 'Andy Grey', 'Andy', 'Grey', 'Midfielder', 1, NULL, NULL, '2001-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(72, 'Jonathan Gillender', 'Jonathan', 'Gillender', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(73, 'Paul Paul', 'Paul', 'Paul', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(74, 'Laurence Rankin', 'Laurence', 'Rankin', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(75, 'Michael Young', 'Michael', 'Young', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(76, 'John Doran', 'John', 'Doran', NULL, 1, NULL, NULL, '1986-08-24', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(78, 'Adrian Addrian', 'Adrian', 'Addrian', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(79, 'Neil Gillender', 'Neil', 'Gillender', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(80, 'Samuel Samuel', 'Samuel', 'Samuel', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(81, 'Gavin McKibbin', 'Gavin', 'McKibbin', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(82, 'Ciaran McCartney', 'Ciaran', 'McCartney', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(83, 'Glenn McKibbin', 'Glenn', 'McKibbin', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(84, 'Brendan Starkey', 'Brendan', 'Starkey', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(85, 'Gordon Gordon', 'Gordon', 'Gordon', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(86, 'Richie Richie', 'Richie', 'Richie', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(87, 'Padraig Toner', 'Padraig', 'Toner', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(88, 'Ryan Kane', 'Ryan', 'Kane', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(89, 'Richard O\'Toole', 'Richard', 'O\'Toole', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(90, 'Bryan Mills', 'Bryan', 'Mills', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(91, 'Thomas McNulty', 'Thomas', 'McNulty', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(92, 'Andrew McAllister', 'Andrew', 'McAllister', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(95, 'Mathew Quinn', 'Mathew', 'Quinn', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(96, 'Brian Wells', 'Brian', 'Wells', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(97, 'Patrick Walls', 'Patrick', 'Walls', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(98, 'Ross Andrews', 'Ross', 'Andrews', NULL, 1, NULL, NULL, '1979-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(99, 'Eoin Morgan', 'Eoin', 'Morgan', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(100, 'Michael Morrison', 'Michael', 'Morrison', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(103, 'Dennis Cameron', 'Dennis', 'Cameron', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(104, 'Desmond Beattie', 'Desmond', 'Beattie', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(105, 'Patrick Gruener', 'Patrick', 'Gruener', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(106, 'Richard McFerran', 'Richard', 'McFerran', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(107, 'Roman Polinek', 'Roman', 'Polinek', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(109, 'James Canning', 'James', 'Canning', 'Midfielder', 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 17:03:29'),
(110, 'Craig McCullough', 'Craig', 'McCullough', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(111, 'Jonny Pankhurst', 'Jonny', 'Pankhurst', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(112, 'Joe Lemon', 'Joe', 'Lemon', NULL, 1, NULL, NULL, '1983-09-08', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(113, 'Liam Parker', 'Liam', 'Parker', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(114, 'Bill Armstrong', 'Bill', 'Armstrong', NULL, 1, NULL, NULL, '2001-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(116, 'John Mills', 'John', 'Mills', NULL, 1, NULL, NULL, '1971-03-24', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(117, 'James Patterson', 'James', 'Patterson', 'Midfielder', 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 17:02:29'),
(118, 'David Love', 'David', 'Love', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(119, 'James Brampston', 'James', 'Brampston', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(120, 'Chris Duffy', 'Chris', 'Duffy', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(121, 'Richard Gould', 'Richard', 'Gould', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(122, 'Andrew Young', 'Andrew', 'Young', NULL, 1, NULL, NULL, '1974-11-19', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(123, 'Emmett McKeown', 'Emmett', 'McKeown', NULL, 1, NULL, NULL, '1984-09-16', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(124, 'Matthew Gamble', 'Matthew', 'Gamble', NULL, 1, NULL, NULL, '1985-12-18', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(125, 'Phil Williams', 'Phil', 'Williams', NULL, 1, NULL, NULL, '1988-06-13', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(126, 'Raphael Emanuel', 'Raphael', 'Emanuel', NULL, 1, NULL, NULL, '1988-03-17', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(127, 'Thomas Baylis', 'Thomas', 'Baylis', NULL, 1, NULL, NULL, '1989-06-12', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(128, 'Mark Hanvey', 'Mark', 'Hanvey', NULL, 1, NULL, NULL, '1986-08-18', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(129, 'Joe  Cullen', 'Joe ', 'Cullen', NULL, 1, NULL, NULL, '1991-03-03', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(130, 'Scott Anderson', 'Scott', 'Anderson', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(131, 'Michael Galbraith', 'Michael', 'Galbraith', NULL, 1, NULL, NULL, '1990-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(132, 'Ricky Millar', 'Ricky', 'Millar', NULL, 1, NULL, NULL, '2000-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(133, 'David Agnew', 'David', 'Agnew', 'Midfielder', 1, NULL, NULL, '2000-01-01', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(134, 'Stevie McGinnity', 'Stevie', 'McGinnity', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2010-08-30 13:58:20', '2021-09-23 15:55:34'),
(138, 'Phil Hill', 'Phil', 'Hill', NULL, 1, NULL, NULL, '1991-09-06', NULL, NULL, NULL, '2010-09-07 00:16:29', '2021-09-23 15:55:34'),
(140, 'Jonny Smith', 'Jonny', 'Smith', NULL, 1, NULL, NULL, '1984-10-10', NULL, NULL, NULL, '2010-09-07 00:21:46', '2021-09-23 15:55:34'),
(143, 'Stephen Wells', 'Stephen', 'Wells', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2010-10-25 16:43:30', '2021-09-23 15:55:34'),
(145, 'Marty Cullen', 'Marty', 'Cullen', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2011-01-18 17:20:04', '2021-09-23 15:55:34'),
(146, 'Art McKibben', 'Art', 'McKibben', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2011-03-26 22:23:05', '2021-09-23 15:55:34'),
(152, 'Jimmy Devlin', 'Jimmy', 'Devlin', NULL, 1, NULL, NULL, '1980-05-07', NULL, NULL, NULL, '2011-07-29 19:20:13', '2021-09-23 15:55:34'),
(153, 'Jason McCord', 'Jason', 'McCord', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-07-29 19:20:55', '2021-09-23 15:55:34'),
(154, 'James McKeown', 'James', 'McKeown', 'Defender', 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-07-29 19:21:40', '2021-09-23 15:55:34'),
(155, 'Jonny McCutcheon', 'Jonny', 'McCutcheon', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-07-29 19:22:13', '2021-09-23 15:55:34'),
(156, 'Colm Best', 'Colm', 'Best', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-07-30 21:07:03', '2021-09-23 15:55:34'),
(157, 'James Whitten', 'James', 'Whitten', NULL, 1, NULL, NULL, '1990-05-07', NULL, NULL, NULL, '2011-08-05 01:26:08', '2021-09-23 15:55:34'),
(158, 'Fra McCrory', 'Fra', 'McCrory', NULL, 0, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-08-08 06:13:05', '2021-09-23 15:55:34'),
(159, 'Chris Noble', 'Chris', 'Noble', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-09-03 21:27:02', '2021-09-23 15:55:34'),
(160, 'Colum McGrath', 'Colum', 'McGrath', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-09-04 18:47:20', '2021-09-23 15:55:34'),
(161, 'Sinclair Dowey', 'Sinclair', 'Dowey', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-09-04 18:47:42', '2021-09-23 15:55:34'),
(162, 'Chris Redden', 'Chris', 'Redden', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-09-04 18:48:17', '2021-09-23 15:55:34'),
(163, 'Darren Brown', 'Darren', 'Brown', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-10-08 22:42:25', '2021-09-23 15:55:34'),
(164, 'Paddy McShane', 'Paddy', 'McShane', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-10-16 04:06:57', '2021-09-23 15:55:34'),
(165, 'Fergal Campbell', 'Fergal', 'Campbell', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-10-16 04:07:25', '2021-09-23 15:55:34'),
(166, 'Rene Fusco', 'Rene', 'Fusco', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-10-24 05:05:44', '2021-09-23 15:55:34'),
(169, 'Peter Lavery', 'Peter', 'Lavery', NULL, 1, NULL, NULL, '2002-02-02', NULL, NULL, NULL, '2011-11-13 00:16:12', '2021-09-23 15:55:34'),
(170, 'Dee McAdams', 'Dee', 'McAdams', NULL, 1, NULL, NULL, '1994-07-01', NULL, NULL, NULL, '2012-07-28 17:06:57', '2021-09-23 15:55:34'),
(171, 'Darren Smith', 'Darren', 'Smith', NULL, 1, NULL, NULL, '1994-07-01', NULL, NULL, NULL, '2012-07-28 17:07:19', '2021-09-23 15:55:34'),
(172, 'Damian Fay', 'Damian', 'Fay', NULL, 1, NULL, NULL, '2002-08-01', NULL, NULL, NULL, '2012-08-04 15:22:10', '2021-09-23 15:55:34'),
(173, 'Richard Simpson', 'Richard', 'Simpson', NULL, 1, NULL, NULL, '2002-08-01', NULL, NULL, NULL, '2012-08-18 15:06:00', '2021-09-23 15:55:34'),
(174, 'Callum McNab', 'Callum', 'McNab', 'Defender', 1, NULL, NULL, '2002-08-01', NULL, NULL, NULL, '2012-08-18 15:07:44', '2021-09-23 15:55:34'),
(175, 'John Quinn', 'John', 'Quinn', NULL, 1, NULL, NULL, '1980-09-01', NULL, NULL, NULL, '2012-09-29 22:59:51', '2021-09-23 15:55:34'),
(176, 'Johnny Haggarty', 'Johnny', 'Haggarty', NULL, 1, NULL, NULL, '1984-01-01', NULL, NULL, NULL, '2012-11-17 16:49:25', '2021-09-23 15:55:34'),
(177, 'Paddy Forte', 'Paddy', 'Forte', NULL, 1, NULL, NULL, '2002-12-01', NULL, NULL, NULL, '2012-12-01 17:31:05', '2021-09-23 15:55:34'),
(178, 'Alan Baker', 'Alan', 'Baker', NULL, 1, NULL, NULL, '1996-07-05', NULL, NULL, NULL, '2012-12-10 16:44:42', '2021-09-23 15:55:34'),
(179, 'Niall Kennedy', 'Niall', 'Kennedy', 'Midfielder', 1, NULL, NULL, '1990-02-01', NULL, NULL, NULL, '2013-02-03 14:05:16', '2021-09-23 15:55:34'),
(180, 'Álvaro Santisteban', 'Álvaro', 'Santisteban', NULL, 1, NULL, NULL, '1987-03-23', NULL, NULL, NULL, '2013-03-16 22:25:59', '2021-09-23 15:55:34'),
(181, 'Alan TBC', 'Alan', 'TBC', NULL, 1, NULL, NULL, '2003-04-07', NULL, NULL, NULL, '2013-04-07 15:08:38', '2021-09-23 15:55:34'),
(182, 'Paul McIver', 'Paul', 'McIver', NULL, 1, NULL, NULL, '2003-04-07', NULL, NULL, NULL, '2013-04-07 15:09:20', '2021-09-23 15:55:34'),
(183, 'Norman Boyd', 'Norman', 'Boyd', NULL, 1, NULL, NULL, '2003-04-07', NULL, NULL, NULL, '2013-04-07 15:10:09', '2021-09-23 15:55:34'),
(184, 'Ryan Skinner', 'Ryan', 'Skinner', NULL, 1, NULL, NULL, '2003-07-25', NULL, NULL, NULL, '2013-07-25 21:46:40', '2021-09-23 15:55:34'),
(185, 'Jim McConnellogue', 'Jim', 'McConnellogue', 'Midfielder', 1, NULL, NULL, '2003-07-18', NULL, NULL, NULL, '2013-07-25 21:51:57', '2021-09-23 15:55:34'),
(186, 'Marc Butler', 'Marc', 'Butler', NULL, 1, NULL, NULL, '2003-08-01', NULL, NULL, NULL, '2013-08-01 21:57:02', '2021-09-23 15:55:34'),
(187, 'Jonny Forrest', 'Jonny', 'Forrest', NULL, 1, NULL, NULL, '2003-08-01', NULL, NULL, NULL, '2013-08-01 21:57:55', '2021-09-23 15:55:34'),
(188, 'Bryan Beacom', 'Bryan', 'Beacom', NULL, 1, NULL, NULL, '2003-08-01', NULL, NULL, NULL, '2013-08-01 21:58:19', '2021-09-23 15:55:34'),
(189, 'Gary Ashwood', 'Gary', 'Ashwood', 'Goalkeeper', 1, NULL, NULL, '2003-08-01', NULL, NULL, NULL, '2013-08-01 21:58:44', '2021-09-23 15:55:34'),
(190, 'Paul McMeekin', 'Paul', 'McMeekin', NULL, 1, NULL, NULL, '2003-08-01', NULL, NULL, NULL, '2013-08-01 21:59:29', '2021-09-23 15:55:34'),
(191, 'Chris Thompson', 'Chris', 'Thompson', NULL, 1, NULL, NULL, '2003-08-01', NULL, NULL, NULL, '2013-08-01 22:08:48', '2021-09-23 15:55:34'),
(192, 'Luke Casey', 'Luke', 'Casey', NULL, 1, NULL, NULL, '1990-01-01', NULL, NULL, NULL, '2013-08-03 15:30:25', '2021-09-23 15:55:34'),
(193, 'Richard Reid', 'Richard', 'Reid', NULL, 1, NULL, NULL, '1990-01-01', NULL, NULL, NULL, '2013-08-03 15:31:17', '2021-09-23 15:55:34'),
(194, 'Niall Lawlor', 'Niall', 'Lawlor', NULL, 1, NULL, NULL, '1990-01-01', NULL, NULL, NULL, '2013-08-03 15:32:56', '2021-09-23 15:55:34'),
(195, 'Phil Mannagh', 'Phil', 'Mannagh', NULL, 1, NULL, NULL, '1984-08-24', NULL, NULL, NULL, '2013-08-24 16:43:58', '2021-09-23 15:55:34'),
(196, 'Stephen Ennis', 'Stephen', 'Ennis', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:07:47', '2021-09-23 15:55:34'),
(197, 'Darren Heasly', 'Darren', 'Heasly', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:11:52', '2021-09-23 15:55:34'),
(198, 'Dean McDowell', 'Dean', 'McDowell', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:12:14', '2021-09-23 15:55:34'),
(199, 'Martin Jackson', 'Martin', 'Jackson', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:12:36', '2021-09-23 15:55:34'),
(200, 'Henry Smith', 'Henry', 'Smith', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:13:33', '2021-09-23 15:55:34'),
(201, 'John Smith', 'John', 'Smith', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:13:58', '2021-09-23 15:55:34'),
(202, 'Ben Rowley', 'Ben', 'Rowley', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:21:26', '2021-09-23 15:55:34'),
(203, 'Jamie Morrison', 'Jamie', 'Morrison', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:21:44', '2021-09-23 15:55:34'),
(204, 'John Adams', 'John', 'Adams', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:22:01', '2021-09-23 15:55:34'),
(205, 'Dylan McComb', 'Dylan', 'McComb', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:23:18', '2021-09-23 15:55:34'),
(206, 'Chris McClaughlin', 'Chris', 'McClaughlin', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:23:50', '2021-09-23 15:55:34'),
(207, 'Jonny Connor', 'Jonny', 'Connor', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:24:10', '2021-09-23 15:55:34'),
(208, 'Taylor Reid', 'Taylor', 'Reid', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:24:35', '2021-09-23 15:55:34'),
(209, 'Kyle Jordan', 'Kyle', 'Jordan', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:25:51', '2021-09-23 15:55:34'),
(210, 'Niall Ferguson', 'Niall', 'Ferguson', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:26:11', '2021-09-23 15:55:34'),
(211, 'Jack Lamb', 'Jack', 'Lamb', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:26:30', '2021-09-23 15:55:34'),
(212, 'Kris Burns', 'Kris', 'Burns', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:26:52', '2021-09-23 15:55:34'),
(213, 'Lewis La Roux', 'Lewis', 'La Roux', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:27:17', '2021-09-23 15:55:34'),
(214, 'Curt Hughes', 'Curt', 'Hughes', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:27:37', '2021-09-23 15:55:34'),
(215, 'Michael Jess', 'Michael', 'Jess', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:28:01', '2021-09-23 15:55:34'),
(216, 'Jonny Macintosh', 'Jonny', 'Macintosh', NULL, 1, NULL, NULL, '2003-09-07', NULL, NULL, NULL, '2013-09-07 16:28:26', '2021-09-23 15:55:34'),
(217, 'Michael Timoney', 'Michael', 'Timoney', NULL, 1, NULL, NULL, '1990-01-01', NULL, NULL, NULL, '2013-09-07 16:52:52', '2021-09-23 15:55:34'),
(218, 'James Lyttle', 'James', 'Lyttle', NULL, 1, NULL, NULL, '1990-01-01', NULL, NULL, NULL, '2013-09-07 16:53:32', '2021-09-23 15:55:34'),
(219, 'Ciaran McGreevy', 'Ciaran', 'McGreevy', NULL, 1, NULL, NULL, '1990-01-01', NULL, NULL, NULL, '2013-09-07 16:54:13', '2021-09-23 15:55:34'),
(220, 'Alan Alexander', 'Alan', 'Alexander', NULL, 1, NULL, NULL, '1990-01-01', NULL, NULL, NULL, '2013-09-07 16:55:19', '2021-09-23 15:55:34'),
(221, 'Duncan Campbell', 'Duncan', 'Campbell', NULL, 1, NULL, NULL, '2003-09-01', NULL, NULL, NULL, '2013-09-15 19:45:53', '2021-09-23 15:55:34'),
(222, 'Matty Patterson', 'Matty', 'Patterson', NULL, 1, NULL, NULL, '2003-09-03', NULL, NULL, NULL, '2013-09-15 19:56:46', '2021-09-23 15:55:34'),
(223, 'Chris Rentowl', 'Chris', 'Rentowl', NULL, 1, NULL, NULL, '2003-09-03', NULL, NULL, NULL, '2013-09-15 19:57:05', '2021-09-23 15:55:34'),
(224, 'David Fee', 'David', 'Fee', 'Forward', 1, NULL, NULL, '2003-09-03', NULL, NULL, NULL, '2013-09-15 19:59:21', '2021-09-23 15:55:34'),
(225, 'Conor Patton', 'Conor', 'Patton', NULL, 1, NULL, NULL, '1990-01-01', NULL, NULL, NULL, '2013-09-21 15:30:25', '2021-09-23 15:55:34'),
(226, 'James Connery', 'James', 'Connery', NULL, 1, NULL, NULL, '2003-09-28', NULL, NULL, NULL, '2013-09-28 16:10:21', '2021-09-23 15:55:34'),
(227, 'Mark Devine', 'Mark', 'Devine', NULL, 1, NULL, NULL, '1990-01-01', NULL, NULL, NULL, '2013-10-05 22:31:35', '2021-09-23 15:55:34'),
(228, 'Sam Crawford', 'Sam', 'Crawford', NULL, 1, NULL, NULL, '2003-10-13', NULL, NULL, NULL, '2013-10-13 08:28:43', '2021-09-23 15:55:34'),
(229, 'Mark Currie', 'Mark', 'Currie', NULL, 1, NULL, NULL, '2003-10-13', NULL, NULL, NULL, '2013-10-13 08:29:07', '2021-09-23 15:55:34'),
(230, 'Michael Stuart', 'Michael', 'Stuart', NULL, 1, NULL, NULL, '2003-10-02', NULL, NULL, NULL, '2013-10-30 17:32:43', '2021-09-23 15:55:34'),
(231, 'Joe Gervin', 'Joe', 'Gervin', NULL, 1, NULL, NULL, '2003-10-02', NULL, NULL, NULL, '2013-10-30 17:33:09', '2021-09-23 15:55:34'),
(232, 'Phil McLaughlin', 'Phil', 'McLaughlin', NULL, 1, NULL, NULL, '2003-10-03', NULL, NULL, NULL, '2013-10-30 17:33:32', '2021-09-23 15:55:34'),
(233, 'Jonny Smith2', 'Jonny', 'Smith2', NULL, 1, NULL, NULL, '2003-10-03', NULL, NULL, NULL, '2013-10-30 17:36:14', '2021-09-23 15:55:34'),
(234, 'Mark McLaughlin', 'Mark', 'McLaughlin', 'Goalkeeper', 1, NULL, NULL, '2003-10-02', NULL, NULL, NULL, '2013-10-30 17:37:58', '2021-09-23 15:55:34'),
(235, 'Ross McCommish', 'Ross', 'McCommish', NULL, 1, NULL, NULL, '2003-11-12', NULL, NULL, NULL, '2013-11-12 22:57:26', '2021-09-23 15:55:34'),
(236, 'Conor Power', 'Conor', 'Power', NULL, 1, NULL, NULL, '2003-11-12', NULL, NULL, NULL, '2013-11-12 22:57:52', '2021-09-23 15:55:34'),
(237, 'Barry Green', 'Barry', 'Green', NULL, 1, NULL, NULL, '1990-12-01', NULL, NULL, NULL, '2013-12-28 22:21:39', '2021-09-23 15:55:34'),
(238, 'Paul Devlin', 'Paul', 'Devlin', NULL, 1, NULL, NULL, '2004-03-03', NULL, NULL, NULL, '2014-03-23 02:17:49', '2021-09-23 15:55:34'),
(239, 'Eoin Neeson', 'Eoin', 'Neeson', NULL, 1, NULL, NULL, '2004-03-03', NULL, NULL, NULL, '2014-03-23 02:19:03', '2021-09-23 15:55:34'),
(240, 'Gerard Conlon', 'Gerard', 'Conlon', NULL, 1, NULL, NULL, '2004-03-03', NULL, NULL, NULL, '2014-03-23 02:19:31', '2021-09-23 15:55:34'),
(241, 'Chris Anderson', 'Chris', 'Anderson', NULL, 1, NULL, NULL, '1985-12-15', NULL, NULL, NULL, '2014-08-17 13:01:56', '2021-09-23 15:55:34'),
(242, 'Brendan McCrisken', 'Brendan', 'McCrisken', 'Forward', 1, NULL, NULL, '1992-03-07', NULL, NULL, NULL, '2014-08-17 13:07:02', '2021-09-23 15:55:34'),
(243, 'Michael Magowan', 'Michael', 'Magowan', NULL, 1, NULL, NULL, '2004-08-10', NULL, NULL, NULL, '2014-08-17 13:08:08', '2021-09-23 15:55:34'),
(244, 'Matt Gault', 'Matt', 'Gault', NULL, 1, NULL, NULL, '1992-09-01', NULL, NULL, NULL, '2014-09-07 17:19:15', '2021-09-23 15:55:34'),
(245, 'Brendan Holbeach', 'Brendan', 'Holbeach', NULL, 1, NULL, NULL, '1992-09-01', NULL, NULL, NULL, '2014-09-07 17:20:09', '2021-09-23 15:55:34'),
(246, 'Eamonn McNamee', 'Eamonn', 'McNamee', 'Midfielder', 1, NULL, NULL, '1982-10-20', NULL, NULL, NULL, '2014-09-07 17:21:23', '2021-09-23 17:05:04'),
(247, 'Chris Hillen', 'Chris', 'Hillen', NULL, 1, NULL, NULL, '1992-09-03', NULL, NULL, NULL, '2014-09-07 17:49:58', '2021-09-23 15:55:34'),
(248, 'Michael Fay', 'Michael', 'Fay', NULL, 1, NULL, NULL, '2004-09-01', NULL, NULL, NULL, '2014-09-07 18:05:30', '2021-09-23 15:55:34'),
(249, 'Harry Bingham', 'Harry', 'Bingham', NULL, 1, NULL, NULL, '2004-09-01', NULL, NULL, NULL, '2014-09-07 18:05:54', '2021-09-23 15:55:34'),
(250, 'Adam Young', 'Adam', 'Young', NULL, 1, NULL, NULL, '2004-09-09', NULL, NULL, NULL, '2014-09-23 06:58:32', '2021-09-23 15:55:34'),
(251, 'Dean Cromie', 'Dean', 'Cromie', NULL, 1, NULL, NULL, '2004-09-09', NULL, NULL, NULL, '2014-09-23 06:59:21', '2021-09-23 15:55:34'),
(252, 'Gerald Elliott', 'Gerald', 'Elliott', NULL, 1, NULL, NULL, '2004-09-09', NULL, NULL, NULL, '2014-09-23 07:01:17', '2021-09-23 15:55:34'),
(253, 'Mickey McCullagh', 'Mickey', 'McCullagh', NULL, 1, NULL, NULL, '2004-09-09', NULL, NULL, NULL, '2014-09-23 07:02:10', '2021-09-23 15:55:34'),
(254, 'James Campbell', 'James', 'Campbell', NULL, 1, NULL, NULL, '2004-09-09', NULL, NULL, NULL, '2014-09-23 07:02:47', '2021-09-23 15:55:34'),
(255, 'Ray McPhillips', 'Ray', 'McPhillips', NULL, 1, NULL, NULL, '2004-09-09', NULL, NULL, NULL, '2014-09-23 07:04:22', '2021-09-23 15:55:34'),
(256, 'David O\'Reilly', 'David', 'O\'Reilly', NULL, 1, NULL, NULL, '2004-09-09', NULL, NULL, NULL, '2014-09-23 07:06:13', '2021-09-23 15:55:34'),
(257, 'Niall Mahon', 'Niall', 'Mahon', 'Midfielder', 1, NULL, NULL, '2004-10-11', NULL, NULL, NULL, '2014-10-13 08:27:28', '2021-09-23 15:55:34'),
(258, 'Mark McCambridge', 'Mark', 'McCambridge', 'Midfielder', 1, NULL, NULL, '2004-11-03', NULL, NULL, NULL, '2014-11-10 12:55:01', '2021-09-23 15:55:34'),
(259, 'Gary Hassan', 'Gary', 'Hassan', 'Forward', 1, NULL, NULL, '2004-11-10', NULL, NULL, NULL, '2014-11-17 16:46:14', '2021-09-23 15:55:34'),
(260, 'Stuart  Armstrong', 'Stuart ', 'Armstrong', NULL, 1, NULL, NULL, '2004-11-10', NULL, NULL, NULL, '2014-11-17 16:46:47', '2021-09-23 15:55:34'),
(261, 'Matt Bell', 'Matt', 'Bell', 'Midfielder', 1, NULL, NULL, '2004-11-03', NULL, NULL, NULL, '2014-11-17 16:47:08', '2021-09-23 15:55:34'),
(262, 'Fergal Lindsay', 'Fergal', 'Lindsay', NULL, 1, NULL, NULL, '2004-11-17', NULL, NULL, NULL, '2014-11-17 16:47:34', '2021-09-23 15:55:34'),
(263, 'Ciaran McIllhatton', 'Ciaran', 'McIllhatton', NULL, 1, NULL, NULL, '2004-11-05', NULL, NULL, NULL, '2014-11-17 16:47:59', '2021-09-23 15:55:34'),
(264, 'Darren McCullagh', 'Darren', 'McCullagh', NULL, 1, NULL, NULL, '2004-11-03', NULL, NULL, NULL, '2014-11-25 15:12:47', '2021-09-23 15:55:34'),
(265, 'David Matthews', 'David', 'Matthews', NULL, 1, NULL, NULL, '2005-02-01', NULL, NULL, NULL, '2015-02-11 09:48:35', '2021-09-23 15:55:34'),
(266, 'Pablo Cordoba Huertos', 'Pablo', 'Cordoba Huertos', NULL, 1, NULL, NULL, '2005-02-05', NULL, NULL, NULL, '2015-02-11 09:49:12', '2021-09-23 15:55:34'),
(267, 'Joss Martin', 'Joss', 'Martin', 'Forward', 1, NULL, NULL, '2005-05-04', NULL, NULL, NULL, '2015-05-04 22:49:31', '2021-09-23 15:55:34'),
(268, 'Matthew Ferguson', 'Matthew', 'Ferguson', NULL, 1, NULL, NULL, '1994-07-07', NULL, NULL, NULL, '2015-08-22 16:52:45', '2021-09-23 15:55:34'),
(269, 'Dominic Emanuel', 'Dominic', 'Emanuel', NULL, 1, NULL, NULL, '2005-09-16', NULL, NULL, NULL, '2015-09-16 11:30:22', '2021-09-23 15:55:34'),
(270, 'Marty Lish', 'Marty', 'Lish', 'Forward', 1, NULL, NULL, '2005-09-16', NULL, NULL, NULL, '2015-09-16 11:31:48', '2021-09-23 15:55:34'),
(271, 'Andrew Hayes', 'Andrew', 'Hayes', NULL, 1, NULL, NULL, '2005-09-16', NULL, NULL, NULL, '2015-09-16 11:35:25', '2021-09-23 15:55:34'),
(272, 'Tristan Crowe', 'Tristan', 'Crowe', 'Forward', 1, NULL, NULL, '2005-09-16', NULL, NULL, NULL, '2015-09-16 11:38:05', '2021-09-23 15:55:34'),
(273, 'Matthew Gourley', 'Matthew', 'Gourley', NULL, 1, NULL, NULL, '1993-10-25', NULL, NULL, NULL, '2015-09-26 16:50:42', '2021-09-23 15:55:34'),
(274, 'Jordan Spring', 'Jordan', 'Spring', NULL, 1, NULL, NULL, '1993-12-15', NULL, NULL, NULL, '2015-10-04 09:28:00', '2021-09-23 15:55:34'),
(275, 'Barry Lynam', 'Barry', 'Lynam', 'Defender', 1, NULL, NULL, '2005-10-02', NULL, NULL, NULL, '2015-10-04 21:32:39', '2021-09-23 15:55:34'),
(276, 'Gary Boyd', 'Gary', 'Boyd', NULL, 1, NULL, NULL, '2005-10-25', NULL, NULL, NULL, '2015-10-25 12:59:43', '2021-09-23 15:55:34'),
(277, 'Wayne Morrison', 'Wayne', 'Morrison', NULL, 1, NULL, NULL, '2005-10-25', NULL, NULL, NULL, '2015-10-25 13:00:31', '2021-09-23 15:55:34'),
(278, 'Jimmy McDonnell', 'Jimmy', 'McDonnell', NULL, 1, NULL, NULL, '2005-10-25', NULL, NULL, NULL, '2015-10-25 13:00:52', '2021-09-23 15:55:34'),
(279, 'Martyn McDonnell', 'Martyn', 'McDonnell', 'Forward', 1, NULL, NULL, '2005-11-02', NULL, NULL, NULL, '2015-11-02 10:38:13', '2021-09-23 15:55:34'),
(280, 'Dylan Frizelle', 'Dylan', 'Frizelle', NULL, 1, NULL, NULL, '2005-11-02', NULL, NULL, NULL, '2015-11-02 10:38:35', '2021-09-23 15:55:34'),
(281, 'Rhys Crawford', 'Rhys', 'Crawford', NULL, 1, NULL, NULL, '2005-11-02', NULL, NULL, NULL, '2015-11-02 10:38:53', '2021-09-23 15:55:34'),
(282, 'David MacCarthy', 'David', 'MacCarthy', NULL, 1, NULL, NULL, '2005-12-01', NULL, NULL, NULL, '2015-12-20 17:15:34', '2021-09-23 15:55:34'),
(283, 'Charlie Maginness', 'Charlie', 'Maginness', NULL, 1, NULL, NULL, '2005-12-26', NULL, NULL, NULL, '2015-12-29 16:35:59', '2021-09-23 15:55:34'),
(284, 'David  Bell', 'David ', 'Bell', NULL, 1, NULL, NULL, '2006-01-17', NULL, NULL, NULL, '2016-01-17 19:46:44', '2021-09-23 15:55:34'),
(285, 'Daniel Abizanda', 'Daniel', 'Abizanda', NULL, 1, NULL, NULL, '1990-07-10', NULL, NULL, NULL, '2016-04-08 10:27:17', '2021-09-23 15:55:34'),
(286, 'Simon Galloway', 'Simon', 'Galloway', NULL, 1, NULL, NULL, '2006-09-04', NULL, NULL, NULL, '2016-09-04 11:15:47', '2021-09-23 15:55:34'),
(287, 'Mark O\'Reilly', 'Mark', 'O\'Reilly', NULL, 1, NULL, NULL, '1990-03-30', NULL, NULL, NULL, '2016-09-04 11:16:04', '2021-09-23 15:55:34'),
(288, 'Nathan O\'Donnell', 'Nathan', 'O\'Donnell', 'Midfielder', 1, NULL, NULL, '2006-09-03', NULL, NULL, NULL, '2016-09-04 11:16:25', '2021-09-23 15:55:34'),
(289, 'Dominic Tolan', 'Dominic', 'Tolan', NULL, 1, NULL, NULL, '2006-09-04', NULL, NULL, NULL, '2016-09-04 11:16:40', '2021-09-23 15:55:34'),
(290, 'Chris Moffet', 'Chris', 'Moffet', NULL, 1, NULL, NULL, '2006-09-04', NULL, NULL, NULL, '2016-09-04 11:17:02', '2021-09-23 15:55:34'),
(291, 'Gareth Watton', 'Gareth', 'Watton', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2016-09-07 17:32:51', '2021-09-23 15:55:34'),
(292, 'Keelan Walsh', 'Keelan', 'Walsh', 'Forward', 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2016-09-07 17:33:29', '2021-09-23 15:55:34'),
(293, 'Jonny Birch', 'Jonny', 'Birch', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2016-09-07 18:23:03', '2021-09-23 15:55:34'),
(294, 'Callum Coulter', 'Callum', 'Coulter', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2016-09-07 18:37:25', '2021-09-23 15:55:34'),
(295, 'Aaron Adams', 'Aaron', 'Adams', NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2016-09-08 21:00:36', '2021-09-23 15:55:34'),
(296, 'Conor \'Rusty\' O\'Neill', 'Conor \'Rusty\'', 'O\'Neill', 'Defender', 1, NULL, NULL, '1970-01-01', NULL, NULL, NULL, '2016-09-08 21:01:09', '2021-09-23 15:55:34'),
(297, 'Ciaran  Murtagh', 'Ciaran', ' Murtagh', NULL, 1, NULL, NULL, '2006-09-11', NULL, NULL, NULL, '2016-09-11 01:39:21', '2021-09-23 15:55:34'),
(298, 'Michael  Steele', 'Michael ', 'Steele', NULL, 1, NULL, NULL, '2006-09-07', NULL, NULL, NULL, '2016-09-17 20:29:20', '2021-09-23 15:55:34'),
(299, 'Brendon Miskelly', 'Brendon', 'Miskelly', 'Midfielder', 1, NULL, NULL, '2006-09-11', NULL, NULL, NULL, '2016-09-17 20:29:41', '2021-09-23 15:55:34'),
(300, 'Stephen  Chapman', 'Stephen ', 'Chapman', 'Goalkeeper', 1, NULL, NULL, '2006-10-01', NULL, NULL, NULL, '2016-10-01 19:55:14', '2021-09-23 15:55:34'),
(301, 'Peter Ferguson', 'Peter', 'Ferguson', NULL, 1, NULL, NULL, '2006-10-08', NULL, NULL, NULL, '2016-10-08 21:37:08', '2021-09-23 15:55:34'),
(302, 'Michael Long', 'Michael', 'Long', 'Defender', 1, NULL, NULL, '1990-10-17', NULL, NULL, NULL, '2016-10-23 16:35:30', '2021-09-23 15:55:34'),
(303, 'Thomas McCloskey', 'Thomas', 'McCloskey', NULL, 1, NULL, NULL, '1994-04-24', NULL, NULL, NULL, '2016-10-24 14:39:36', '2021-09-23 15:55:34'),
(304, 'James McGivern', 'James', 'McGivern', NULL, 1, NULL, NULL, '2006-10-13', NULL, NULL, NULL, '2016-10-30 18:20:42', '2021-09-23 15:55:34'),
(305, 'Damien Edgar', 'Damien', 'Edgar', 'Forward', 1, NULL, NULL, '2006-11-01', NULL, NULL, NULL, '2016-11-05 21:24:56', '2021-09-23 15:55:34'),
(306, 'Brian  Conway', 'Brian ', 'Conway', NULL, 1, NULL, NULL, '2006-11-14', NULL, NULL, NULL, '2016-11-14 17:29:13', '2021-09-23 15:55:34'),
(307, 'Brian Boru', 'Brian', 'Boru', NULL, 1, NULL, NULL, '2006-11-16', NULL, NULL, NULL, '2016-11-20 17:25:17', '2021-09-23 15:55:34'),
(308, 'Brent Esler', 'Brent', 'Esler', NULL, 1, NULL, NULL, '2006-12-04', NULL, NULL, NULL, '2016-12-04 17:56:53', '2021-09-23 15:55:34'),
(309, 'David Mackle', 'David', 'Mackle', NULL, 1, NULL, NULL, '2006-12-03', NULL, NULL, NULL, '2016-12-04 18:02:54', '2021-09-23 15:55:34'),
(310, 'Glen  Matthews', 'Glen ', 'Matthews', NULL, 1, NULL, NULL, '2007-01-21', NULL, NULL, NULL, '2017-01-21 21:55:47', '2021-09-23 15:55:34'),
(311, 'Kyle Harrison', 'Kyle', 'Harrison', 'Midfielder', 1, NULL, NULL, '2007-01-11', NULL, NULL, NULL, '2017-01-29 17:53:16', '2021-09-23 15:55:34'),
(312, 'Scott  Byrne', 'Scott ', 'Byrne', NULL, 1, NULL, NULL, '2007-08-16', NULL, NULL, NULL, '2017-08-17 13:10:10', '2021-09-23 15:55:34'),
(313, 'Pearce McConkey', 'Pearce', 'McConkey', 'Forward', 1, NULL, NULL, '2007-08-01', NULL, NULL, NULL, '2017-08-17 13:10:41', '2021-09-23 15:55:34'),
(314, 'Stuart Rafferty', 'Stuart', 'Rafferty', NULL, 1, NULL, NULL, '1990-09-02', NULL, NULL, NULL, '2017-09-04 18:24:35', '2021-09-23 15:55:34'),
(315, 'Scott Crawford', 'Scott', 'Crawford', NULL, 1, NULL, NULL, '1990-09-02', NULL, NULL, NULL, '2017-09-04 18:26:33', '2021-09-23 15:55:34'),
(316, 'Chris O\'Connor', 'Chris', 'O\'Connor', NULL, 1, NULL, NULL, '1990-09-02', NULL, NULL, NULL, '2017-09-04 18:27:07', '2021-09-23 15:55:34'),
(318, 'Matt Campbell', 'Matt', 'Campbell', NULL, 1, NULL, NULL, '1995-09-13', NULL, NULL, NULL, '2017-09-13 12:48:32', '2021-09-23 15:55:34'),
(319, 'Bob Robertson', 'Bob', 'Robertson', NULL, 1, NULL, NULL, '1987-09-01', NULL, NULL, NULL, '2017-09-15 17:58:00', '2021-09-23 15:55:34'),
(320, 'John McManus', 'John', 'McManus', 'Forward', 1, NULL, NULL, '1989-09-01', NULL, NULL, NULL, '2017-09-15 17:58:38', '2021-09-23 15:55:34'),
(321, 'Emad Hamid', 'Emad', 'Hamid', NULL, 1, NULL, NULL, '1988-09-01', NULL, NULL, NULL, '2017-09-15 18:01:32', '2021-09-23 15:55:34'),
(322, 'Patrick Bingham', 'Patrick', 'Bingham', NULL, 1, NULL, NULL, '2007-09-12', NULL, NULL, NULL, '2017-09-16 18:56:23', '2021-09-23 15:55:34'),
(323, 'Michael  Mullan', 'Michael ', 'Mullan', NULL, 1, NULL, NULL, '2007-09-12', NULL, NULL, NULL, '2017-09-16 18:56:47', '2021-09-23 15:55:34'),
(324, 'Cillian McGinn', 'Cillian', 'McGinn', NULL, 1, NULL, NULL, '2007-09-12', NULL, NULL, NULL, '2017-09-16 18:57:20', '2021-09-23 15:55:34'),
(325, 'Sean McDonald', 'Sean', 'McDonald', NULL, 1, NULL, NULL, '2007-10-02', NULL, NULL, NULL, '2017-10-08 15:43:09', '2021-09-23 15:55:34'),
(326, 'Gary Kane', 'Gary', 'Kane', NULL, 1, NULL, NULL, '1999-11-24', NULL, NULL, NULL, '2017-11-06 22:17:48', '2021-09-23 15:55:34'),
(327, 'Alvin Duke', 'Alvin', 'Duke', NULL, 1, NULL, NULL, '2008-01-02', NULL, NULL, NULL, '2018-01-05 00:55:59', '2021-09-23 15:55:34'),
(328, 'Gordon Cheung', 'Gordon', 'Cheung', NULL, 1, NULL, NULL, '2008-04-03', NULL, NULL, NULL, '2018-04-11 20:32:34', '2021-09-23 15:55:34'),
(329, 'Darren  Hand', 'Darren ', 'Hand', 'Goalkeeper', 1, NULL, NULL, '2008-04-01', NULL, NULL, NULL, '2018-04-12 14:51:59', '2021-09-23 15:55:34'),
(330, 'Luke McKelvey', 'Luke', 'McKelvey', 'Forward', 1, '183cm', 'Right', '1990-05-30', '2019-09-01 00:00:00', NULL, NULL, '2021-09-23 16:56:57', '2021-09-23 16:56:57');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `home_team_goals` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `away_team_goals` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `game_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', '2021-09-03 15:30:11', '2021-09-03 15:30:11'),
(2, 'Manager', 'manager', '2021-09-03 15:30:11', '2021-09-03 15:30:11');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `slug`, `nickname`, `created_at`, `updated_at`) VALUES
(1, 'Newtown Forest', 'newtown-forest', 'First Team', '2021-09-24 16:08:10', '2021-09-24 16:08:10'),
(2, 'Newtown Forest II', 'newtown-forest-ii', 'Second Team', '2021-09-24 16:08:25', '2021-09-24 16:08:25'),
(3, 'Newtown Forest III', 'newtown-forest-iii', 'Third Team', '2021-09-24 16:08:37', '2021-09-24 16:08:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `first_name`, `last_name`, `email`, `email_verified_at`, `phone`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Element Seven', 'Element', 'Seven', 'hello@elementseven.co', '2021-09-03 15:30:11', NULL, '$2y$10$E0gJQTWZEH/mbIe9yYTMCe7sBIVEAz3Zuq4/5cErzOqsdQG2WpxD2', 1, NULL, '2021-09-03 15:30:11', '2021-09-03 15:30:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action_events`
--
ALTER TABLE `action_events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_events_actionable_type_actionable_id_index` (`actionable_type`,`actionable_id`),
  ADD KEY `action_events_batch_id_model_type_model_id_index` (`batch_id`,`model_type`,`model_id`),
  ADD KEY `action_events_user_id_index` (`user_id`);

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cards_player_id_index` (`player_id`),
  ADD KEY `cards_game_id_index` (`game_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_post`
--
ALTER TABLE `category_post`
  ADD PRIMARY KEY (`category_id`,`post_id`),
  ADD KEY `category_post_category_id_index` (`category_id`),
  ADD KEY `category_post_post_id_index` (`post_id`);

--
-- Indexes for table `competitions`
--
ALTER TABLE `competitions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`),
  ADD KEY `games_competition_id_index` (`competition_id`),
  ADD KEY `games_team_id_index` (`team_id`);

--
-- Indexes for table `goals`
--
ALTER TABLE `goals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `goals_scorer_id_index` (`scorer_id`),
  ADD KEY `goals_assisted_id_index` (`assisted_id`),
  ADD KEY `goals_game_id_index` (`game_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `media_uuid_unique` (`uuid`),
  ADD KEY `media_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nova_froala_attachments`
--
ALTER TABLE `nova_froala_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nova_froala_attachments_attachable_type_attachable_id_index` (`attachable_type`,`attachable_id`),
  ADD KEY `nova_froala_attachments_url_index` (`url`);

--
-- Indexes for table `nova_pending_froala_attachments`
--
ALTER TABLE `nova_pending_froala_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nova_pending_froala_attachments_draft_id_index` (`draft_id`);

--
-- Indexes for table `nova_pending_trix_attachments`
--
ALTER TABLE `nova_pending_trix_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nova_pending_trix_attachments_draft_id_index` (`draft_id`);

--
-- Indexes for table `nova_trix_attachments`
--
ALTER TABLE `nova_trix_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nova_trix_attachments_attachable_type_attachable_id_index` (`attachable_type`,`attachable_id`),
  ADD KEY `nova_trix_attachments_url_index` (`url`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_game_id_index` (`game_id`),
  ADD KEY `reports_user_id_index` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action_events`
--
ALTER TABLE `action_events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `competitions`
--
ALTER TABLE `competitions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `games`
--
ALTER TABLE `games`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `goals`
--
ALTER TABLE `goals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `nova_froala_attachments`
--
ALTER TABLE `nova_froala_attachments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nova_pending_froala_attachments`
--
ALTER TABLE `nova_pending_froala_attachments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nova_pending_trix_attachments`
--
ALTER TABLE `nova_pending_trix_attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nova_trix_attachments`
--
ALTER TABLE `nova_trix_attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=331;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cards`
--
ALTER TABLE `cards`
  ADD CONSTRAINT `cards_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cards_player_id_foreign` FOREIGN KEY (`player_id`) REFERENCES `profiles` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `category_post`
--
ALTER TABLE `category_post`
  ADD CONSTRAINT `category_post_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `games`
--
ALTER TABLE `games`
  ADD CONSTRAINT `games_competition_id_foreign` FOREIGN KEY (`competition_id`) REFERENCES `competitions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `games_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `goals`
--
ALTER TABLE `goals`
  ADD CONSTRAINT `goals_assisted_id_foreign` FOREIGN KEY (`assisted_id`) REFERENCES `profiles` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `goals_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `goals_scorer_id_foreign` FOREIGN KEY (`scorer_id`) REFERENCES `profiles` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
