<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Froala\NovaFroalaField\Froala;
use Laravel\Nova\Http\Requests\NovaRequest;

class Profile extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Profile::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'full_name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'full_name','phone'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {

        $positions = array('Forward'=>'Forward','Midfielder'=>'Midfielder','Defender'=>'Defender','Goalkeeper'=>'Goalkeeper');
        $statuses = array( 1 =>'Send Availability Text', 0 =>"Don't Send Availability Text");
        $actives = array( 1 =>'Active', 0 =>"Not Active");
        return [
            Image::make('Image')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('image')->toMediaCollection('profiles');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('profiles', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('profiles', 'thumbnail');
            })->deletable(false),
            Text::make('First Name', 'first_name')->sortable()->rules('required', 'max:255'),
            Text::make('Last Name', 'last_name')->sortable()->rules('required', 'max:255'),
            Select::make('Position')->options($positions)->sortable()->hideFromIndex(),
            Select::make('Status')->options($statuses)->sortable()->help(
                'Should the player recieve availability texts (needs phone number)'
            ),
            Select::make('Active')->options($actives)->sortable()->help(
                'Is the player currently active & playing for the club?'
            ),
            Text::make('Height')->rules('max:255')->hideFromIndex(),
            Text::make('Foot')->rules('max:255')->hideFromIndex(),
            Date::make('Date of Birth', 'dob')->hideFromIndex(),
            Date::make('Joined', 'joined')->hideFromIndex(),
            Text::make('Phone')->help(
                'Must start with +44'
            ),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
