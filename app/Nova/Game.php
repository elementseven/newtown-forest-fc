<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Http\Requests\NovaRequest;
use Carbon\Carbon;

class Game extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Game::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public function title()
    {
        return Carbon::parse($this->date)->format('d/m/Y') . ' ' . $this->team->name . ' vs ' . $this->opposition->name;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $home_away = array('Home'=>'Home','Away'=>'Away');
        $times = array(
            '09:00'=>'09:00',
            '09:15'=>'09:15',
            '09:30'=>'09:30',
            '09:45'=>'09:45',
            '10:00'=>'10:00',
            '10:15'=>'10:15',
            '10:30'=>'10:30',
            '10:45'=>'10:45',
            '11:00'=>'11:00',
            '11:15'=>'11:15',
            '11:30'=>'11:30',
            '11:45'=>'11:45',
            '12:00'=>'12:00',
            '12:15'=>'12:15',
            '12:30'=>'12:30',
            '12:45'=>'12:45',
            '13:00'=>'13:00',
            '13:15'=>'13:15',
            '13:30'=>'13:30',
            '13:45'=>'13:45',
            '14:00'=>'14:00',
            '14:15'=>'14:15',
            '14:30'=>'14:30',
            '14:45'=>'14:45',
            '15:00'=>'15:00',
            '15:15'=>'15:15',
            '15:30'=>'15:30',
            '15:45'=>'15:45',
            '16:00'=>'16:00',
            '16:15'=>'16:15',
            '16:30'=>'16:30',
            '16:45'=>'16:45',
            '17:00'=>'17:00',
            '17:15'=>'17:15',
            '17:30'=>'17:30',
            '17:45'=>'17:45',
            '18:00'=>'18:00',
            '18:15'=>'18:15',
            '18:30'=>'18:30',
            '18:45'=>'18:45',
            '19:00'=>'19:00',
            '19:15'=>'19:15',
            '19:30'=>'19:30',
            '19:45'=>'19:45',
            '20:00'=>'20:00',
            '20:15'=>'20:15',
            '20:30'=>'20:30',
            '20:45'=>'20:45',
            '21:00'=>'21:00'
        );
        return [
            BelongsTo::make('Competition')->onlyOnForms(),
            BelongsTo::make('Team'),
            BelongsTo::make('Opposition','opposition')->sortable(),
            Select::make('Home or Away', 'home_away')->options($home_away)->onlyOnForms(),
            Text::make('Location')->sortable(),
            Text::make('City')->onlyOnForms(),
            Date::make('Date')->sortable(),
            Select::make('Time')->options($times)->sortable(),
            Text::make('Video Url','video_url')->nullable()->onlyOnForms(),
        ];
    }


    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
