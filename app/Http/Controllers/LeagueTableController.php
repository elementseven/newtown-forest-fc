<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LeagueTable;

class LeagueTableController extends Controller
{
    public function firstTeam(){
        $table = LeagueTable::where('team_id', '1')->first();
        return $table->league_table;
    }

    public function secondTeam(){
        $table = LeagueTable::where('team_id', '2')->first();
        return $table->league_table;
    }
}
