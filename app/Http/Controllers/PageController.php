<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Post;
use Carbon\Carbon;

class PageController extends Controller
{

    public function testScorers(){
        $startDate = Carbon::parse('2023-09-01');

        /**
         * Calculate top scorers accross all teams
         *
         * @return int
         */

        $profiles = Profile::whereHas('goals', function($q) use($startDate){
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where('competition_id', '!=', 11);
            });
        })->with(['goals' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where('competition_id', '!=', 11);
            });
        }])
        ->withCount(['squads' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11]]);
            });
        }])->get();

        foreach($profiles as $row){
            $row->thegoals = count($row->goals);
            $row->thesquads = count($row->squads);
        }
        $profiles = $profiles->sortBy('thesquads')->sortBy('thegoals', SORT_REGULAR, true);

        $result = new \DOMDocument();
        $result->formatOutput = true;
        $holder = $result->appendChild($result->createElement("div"));
        $holder->setAttribute('class', 'table-responsive');
        $table = $holder->appendChild($result->createElement("table"));
        $table->setAttribute('class', 'table table-striped');
        $thead = $table->appendChild($result->createElement("thead"));
        $tbody = $table->appendChild($result->createElement("tbody"));
        $newRow = $thead->appendChild($result->createElement("tr"));
        $newRow->appendChild($result->createElement("th", "Player"));
        $newRow->appendChild($result->createElement("th", "Goals Scored"));
        $newRow->appendChild($result->createElement("th", "Games"));
       
        foreach($profiles as $row)
        {
            $newRow = $tbody->appendChild($result->createElement("tr"));
            $newRow->appendChild($result->createElement("td", $row->full_name));
            $newRow->appendChild($result->createElement("td", count($row->goals)));
            $newRow->appendChild($result->createElement("td", $row->squads_count));
        }

        $topscorers = $result->saveXML($result->documentElement);
        return $topscorers;
    }
    public function home(){
        $featured = Post::where('status','Featured')->orderBy('created_at')->first();
        return view('welcome')->with(['featured' => $featured]);
    }

    public function fix(){
        $profiles = Profile::get();
        foreach($profiles as $p){
            $p->image = null;
            $p->full_name = $p->first_name . ' ' . $p->last_name;
            $p->position = null;
            $p->foot = null;
            $p->height = null;
            $p->save();
        }
        return 'done';
    }

    public function about(){
        return view('about');
    }
    public function pay(){
        return view('pay');
    }
    public function donate(){
        return view('donate');
    }
    public function mugs(){
        return view('mugs');
    }
    public function tandcs(){
        return view('tandcs');
    }
    public function privacyPolicy(){
        return view('privacy-policy');
    }
    public function contact(){
        return view('contact');
    }
    public function join(){
        return view('join');
    }
    public function firstTeam(){
        return view('teams.first-team');
    }
    public function secondTeam(){
        return view('teams.second-team');
    }
    public function thirdTeam(){
        return view('teams.third-team');
    }
    public function stats(){
        return view('stats');
    }
    public function summerLeague(){
        return view('summerLeague');
    }

}
