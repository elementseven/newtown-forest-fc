<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Squad;
use App\Models\Team;
use App\Models\Game;
use App\Models\Competition;
use Carbon\Carbon;

class GameController extends Controller
{

    public function index(){
        $competitions = Competition::whereHas('games')->get();
        $teams = Team::get();
        return view('games.index')->with(['competitions' => $competitions, 'teams' => $teams]);
    }

    // Return json news posts
    public function get(Request $request){

        $endofweek = Carbon::now()->endOfWeek();

        if($request->input('date') != ""){
            $date = Carbon::parse($request->input('date'));
        }else{
            $date = '*';
        }
        $competition = $request->input('competition');
        $team = $request->input('team');

        $games = Game::orderBy('date','desc')
        ->whereDate('date', '<=', $endofweek)
        ->whereHas('competition', function($q) use($competition){
            if($competition != '*'){
              $q->where('id', $competition);
            }
        })
        ->whereHas('team', function($q) use($team){
            if($team != '*'){
              $q->where('id', $team);
            }
        })
        ->when($date != "*", function($q) use ($date) {
             $q->whereDate('date', $date);
        })
        ->with('team', 'opposition', 'competition','report')
        ->paginate($request->input('limit'));

        foreach($games as $game){
            $game->opposition->normal = $game->opposition->getFirstMediaUrl('oppositions', 'normal');
            $game->opposition->webp = $game->opposition->getFirstMediaUrl('oppositions', 'normal-webp');
            $game->opposition->mimetype = $game->opposition->getFirstMedia('oppositions')->mime_type;
        }

        return $games;
    }

    // Return json news posts
    public function upcomingFixtures(Request $request){
        
        $team = $request->input('team');
        $endofweek = Carbon::now()->endOfWeek();
        $games = Game::orderBy('date','desc')
        ->whereDate('date', '<=', $endofweek)
        ->whereHas('team', function($q) use($team){
            if($team != '*'){
              $q->where('id', $team);
            }
        })
        ->with('team', 'opposition', 'competition','report')
        ->paginate($request->input('limit'));
        
        foreach($games as $game){
            $game->opposition->normal = $game->opposition->getFirstMediaUrl('oppositions', 'normal');
            $game->opposition->webp = $game->opposition->getFirstMediaUrl('oppositions', 'normal-webp');
            $game->opposition->mimetype = $game->opposition->getFirstMedia('oppositions')->mime_type;
        }

        return $games;
    }

    // Return games player has played in
    public function profileGames(Request $request, Profile $profile){
        
        $squads = Squad::whereHas('profiles', function($q) use($profile){
            $q->where('id', $profile->id);
        })
        ->orderBy('created_at', 'desc')
        ->paginate(3);

        $games = array();

        foreach($squads as $s){
            if(Carbon::parse($s->game->date) >= Carbon::parse('2022-09-01T00:00:00.000000Z')){
                $game = $s->game;
                $game->team;
                $game->competition;
                $game->report;
                $game->opposition->normal = $game->opposition->getFirstMediaUrl('oppositions', 'normal');
                $game->opposition->webp = $game->opposition->getFirstMediaUrl('oppositions', 'normal-webp');
                $game->opposition->mimetype = $game->opposition->getFirstMedia('oppositions')->mime_type;
                array_push($games, $game);
            }
            
        }
        
        return json_encode($games);
    }

    public function show(Game $game){
        return view('games.show')->with(['game' => $game]);
    }
}
