<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

use Newsletter;
use Mail;
use Auth;


class SendMail extends Controller
{
    public function enquiry(Request $request){

        // Validate the form data
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required|string',
        ]); 

        $subject = "General Enquiry";
        $name = $request->input('name');
        $email = $request->input('email');
        $content = $request->input('message');
        $phone = null;
        if($request->input('phone') != ""){
            $phone = $request->input('phone');
        }
        Mail::send('emails.enquiry',[
            'name' => $name,
            'subject' => $subject,
            'email' => $email,
            'phone' => $phone,
            'content' => $content
        ], function ($message) use ($subject, $email, $name, $content, $phone){
            $message->from('donotreply@newtownforest.com', 'Newtown Forest FC');
            $message->subject($subject);
            $message->replyTo($email);
            $message->to('club@newtownforest.com');
            //$message->to('luke@elementseven.co');
        });
        return 'success';
    }

    public function sendJoinForm(Request $request){

        // Validate the form data
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required|string',
        ]); 

        $subject = "New Player Enquiry";
        $name = $request->input('name');
        $email = $request->input('email');
        $content = $request->input('message');
        $phone = null;
        if($request->input('phone') != ""){
            $phone = $request->input('phone');
        }
        $position = null;
        if($request->input('position') != ""){
            $position = $request->input('position');
        }
        $dob = null;
        if($request->input('dob') != ""){
            $dob = Carbon::parse($request->input('dob'))->format('d/m/Y');
        }
        Mail::send('emails.join',[
            'name' => $name,
            'subject' => $subject,
            'email' => $email,
            'phone' => $phone,
            'position' => $position,
            'dob' => $dob,
            'content' => $content
        ], function ($message) use ($subject, $email, $name, $content, $phone, $position, $dob){
            $message->from('donotreply@newtownforest.com', 'Newtown Forest FC');
            $message->subject($subject);
            $message->replyTo($email);
            $message->to('club@newtownforest.com');
            //$message->to('luke@elementseven.co');
        });
        return 'success';
    }

}