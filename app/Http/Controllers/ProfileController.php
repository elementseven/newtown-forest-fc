<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Statistic;
use App\Models\Profile;
use Carbon\Carbon;

class ProfileController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profiles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request){

        $search = $request->input('search');

        $profiles = Profile::when($search != "", function($q) use ($search) {
            $q->where('full_name','LIKE', '%'.$search.'%');
        })
        ->where('active', 1)
        ->orderBy('full_name','asc')
        ->paginate($request->input('limit'),['id','full_name', 'slug']);

        foreach($profiles as $p){
            if($p->hasMedia('profiles')){
                $p->normal = $p->getFirstMediaUrl('profiles', 'normal');
                $p->normalwebp = $p->getFirstMediaUrl('profiles', 'normal-webp');
                $p->mimetype = $p->getFirstMedia('profiles')->mime_type;
            }
        }

        return $profiles;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile, $slug)
    {
        if($profile->hasMedia('profiles')){
            $profile->normal = $profile->getFirstMediaUrl('profiles', 'normal');
            $profile->normalwebp = $profile->getFirstMediaUrl('profiles', 'normal-webp');
            $profile->mimetype = $profile->getFirstMedia('profiles')->mime_type;
        }
        return view('profiles.show')->with(['profile' => $profile]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addSlug()
    {
        $profiles = Profile::get();
        foreach($profiles as $p){
            $p->slug = Str::slug($p->full_name, "-");
            $p->save();
        }
        return "done";
    }

    /**
     * Get the top scorers
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getTopScorers(Request $request)
    {
        $topscorers = Statistic::where([['title', "Top Scorers"],['season', $request->input('season')]])->first();
        return $topscorers;
    }

    /**
     * Get the top scorers
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getTopScorersFirsts(Request $request)
    {
        $topscorers = Statistic::where([['title', "Top Scorers - Firsts"],['season', $request->input('season')]])->first();
        return $topscorers;
    }

    /**
     * Get the top scorers
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getTopScorersSeconds(Request $request)
    {
        $topscorers = Statistic::where([['title', "Top Scorers - Seconds"],['season', $request->input('season')]])->first();
        return $topscorers;
    }

    /**
     * Get the top scorers
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getTopScorersThirds(Request $request)
    {
        $topscorers = Statistic::where([['title', "Top Scorers - Thirds"],['season', $request->input('season')]])->first();
        return $topscorers;
    }

    /**
     * Get appearances
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAppearances(Request $request)
    {
        $appearances = Statistic::where([['title', "Appearances"],['season', $request->input('season')]])->first();
        return $appearances;
    }

    /**
     * Get assists
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAssists(Request $request)
    {
        $assists = Statistic::where([['title', "Top Assists"],['season', $request->input('season')]])->first();
        return $assists;
    }

    /**
     * Get motm performances
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getMotms(Request $request)
    {
        $assists = Statistic::where([['title', "MOTM Performances"],['season', $request->input('season')]])->first();
        return $assists;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
