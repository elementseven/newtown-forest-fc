<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;

    protected $fillable = [
        'home_team_goals', 'away_team_goals', 'description',  'game_id', 'user_id'
    ];

    public function game(){
        return $this->belongsTo('App\Models\Game');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}
