<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Competition extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($competition) {
            $competition->slug = Str::slug($competition->name, "-");
        });
    }

    public function games(){
        return $this->hasMany('App\Models\Game');
    }

}
