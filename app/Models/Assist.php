<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assist extends Model
{
    use HasFactory;

    protected $fillable = [
        'goal_id', 'profile_id'
    ];

    public function goal(){
        return $this->belongsTo('App\Models\Goal');
    }

    public function profile(){
        return $this->belongsTo('App\Models\Profile');
    }

}
