<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

    protected $fillable = [
        'colour', 'profile_id', 'game_id'
    ];

    public function profile(){
        return $this->belongsTo('App\Models\Profile');
    }

    public function game(){
        return $this->belongsTo('App\Models\Game');
    }

}
