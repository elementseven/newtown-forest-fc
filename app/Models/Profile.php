<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use App\Http\Traits\Hashidable;
use Carbon\Carbon;

class Profile extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use Hashidable;

    protected $fillable = [
        'full_name', 'first_name', 'last_name', 'slug', 'position', 'status', 'active', 'height', 'foot', 'dob', 'joined', 'description', 'image', 'phone'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($profile) {
            $profile->full_name = $profile->first_name . ' ' . $profile->last_name;
            $profile->slug = Str::slug($profile->full_name, "-");
        });
    
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'dob' => 'datetime',
        'joined' => 'datetime',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(600);
        $this->addMediaConversion('normal-webp')->width(600)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->width(200)->crop('crop-center', 200, 200);
        $this->addMediaConversion('mob')->keepOriginalImageFormat()->width(300);
        $this->addMediaConversion('mob-webp')->width(300)->format('webp');
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('profiles')->singleFile();
    }

    public function goals(){
        return $this->hasMany('App\Models\Goal')->whereHas('game', function($q){
            $q->whereDate('date', '>=', '2022-09-01')->where('competition_id', '!=', 11);
        });
    }

    public function assists(){
        $startDate = Carbon::parse('2022-09-01');
        return $this->hasMany('App\Models\Assist')->whereHas('goal', function($goal) use($startDate){
            $goal->whereHas('game', function($game) use($startDate){
                $game->whereDate('date','>=',$startDate)->where('competition_id', '!=', 11);
            });
        });
    }

    public function cards(){
        return $this->hasMany('App\Models\Card');
    }

    public function motm(){
        return $this->hasMany('App\Models\Motm')->whereHas('game', function($q){
            $q->where('date', '>=', '2022-09-01');
        });
    }

    public function squads(){
        return $this->belongsToMany('App\Models\Squad', 'profile_squad')->whereHas('game', function($q){
            $q->where('date', '>=', '2022-09-01');
        })->withPivot('squad_id');
    }

    public function availabilities(){
        return $this->belongsToMany('App\Models\Availability', 'availability_profile')->withPivot('availability_id', 'status', 'message');
    }


}