<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $fillable = [
        'opposition_id',
        'location',
        'city',
        'home_away',
        'date',
        'time',
        'competition_id',
        'team_id',
        'video_url'
    ];

    protected $casts = [
        'date' => 'date',
    ];

    public function competition(){
        return $this->belongsTo('App\Models\Competition');
    }

    public function team(){
        return $this->belongsTo('App\Models\Team');
    }

    public function opposition(){
        return $this->belongsTo('App\Models\Opposition');
    }

    public function goals(){
        return $this->hasMany('App\Models\Goal')->orderBy('time');
    }

    public function cards(){
        return $this->hasMany('App\Models\Card');
    }

    public function report(){
        return $this->hasOne('App\Models\Report');
    }

    public function motm(){
        return $this->hasOne('App\Models\Motm');
    }

    public function squad(){
        return $this->hasOne('App\Models\Squad');
    }


}