<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Team extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'nickname'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($category) {
            $category->slug = Str::slug($category->name, "-");
        });
    }

    public function games(){
        return $this->hasMany('App\Models\Game');
    }

    public function leagueTable(){
        return $this->hasOne('App\Models\leagueTable');
    }
}
