<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Squad extends Model
{
    use HasFactory;

    protected $fillable = [
        'game_id'
    ];

    public function game(){
        return $this->belongsTo('App\Models\Game');
    }

    public function profiles(){
        return $this->belongsToMany('App\Models\Profile', 'profile_squad')->withPivot('profile_id');
    }

}
