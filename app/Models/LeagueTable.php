<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueTable extends Model
{
    use HasFactory;

    protected $fillable = [
      'league_table',
      'team_id'
    ];

    public function team(){
        return $this->belongsTo('App\Models\Team');
    }
    
}
