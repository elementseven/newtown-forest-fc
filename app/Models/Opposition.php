<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;


class Opposition extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name',
      'slug',
      'logo'
  ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($oppostion) {
            $oppostion->slug = Str::slug($oppostion->name, "-");
        });
    }
    
    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(100);
        $this->addMediaConversion('normal-webp')->width(100)->format('webp');
        $this->addMediaConversion('double')->keepOriginalImageFormat()->width(200);
        $this->addMediaConversion('double-webp')->width(200)->format('webp');
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('oppositions')->singleFile();
    }

    public function games(){
        return $this->hasMany('App\Models\Game');
    }
}
