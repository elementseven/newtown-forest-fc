<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    use HasFactory;

    protected $fillable = [
        'time', 'profile_id', 'game_id'
    ];

    public function profile(){
        return $this->belongsTo('App\Models\Profile');
    }
    public function game(){
        return $this->belongsTo('App\Models\Game');
    }
    public function assist(){
        return $this->hasOne('App\Models\Assist');
    }


}
