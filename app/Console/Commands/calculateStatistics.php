<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Statistic;
use App\Models\Profile;
use Carbon\Carbon;

class calculateStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculateStatistics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate club statistics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        

        $startDate = Carbon::parse('2024-08-29');

        /**
         * Calculate top scorers accross all teams
         *
         * @return int
         */

        $profiles = Profile::whereHas('goals', function($q) use($startDate){
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where('competition_id', '!=', 11);
            });
        })->with(['goals' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where('competition_id', '!=', 11);
            });
        }])
        ->withCount(['squads' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11]]);
            });
        }])->get();

        foreach($profiles as $row){
            $row->thegoals = count($row->goals);
            $row->thesquads = count($row->squads);
        }
        $profiles = $profiles->sortBy('thesquads')->sortBy('thegoals', SORT_REGULAR, true);

        $result = new \DOMDocument();
        $result->formatOutput = true;
        $holder = $result->appendChild($result->createElement("div"));
        $holder->setAttribute('class', 'table-responsive');
        $table = $holder->appendChild($result->createElement("table"));
        $table->setAttribute('class', 'table table-striped');
        $thead = $table->appendChild($result->createElement("thead"));
        $tbody = $table->appendChild($result->createElement("tbody"));
        $newRow = $thead->appendChild($result->createElement("tr"));
        $newRow->appendChild($result->createElement("th", "Player"));
        $newRow->appendChild($result->createElement("th", "Goals Scored"));
        $newRow->appendChild($result->createElement("th", "Games"));
       
        foreach($profiles as $row)
        {
            $newRow = $tbody->appendChild($result->createElement("tr"));
            $newRow->appendChild($result->createElement("td", $row->full_name));
            $newRow->appendChild($result->createElement("td", count($row->goals)));
            $newRow->appendChild($result->createElement("td", $row->squads_count));
        }

        $topscorers = $result->saveXML($result->documentElement);
        $oldtable = Statistic::where([['title', "Top Scorers"],['season', '2024/25']])->delete();
        $topscorers = Statistic::create([
            'title' => 'Top Scorers',
            'season' => '2024/25',
            'notes' => 'Top scorers from all games, excluding friendlies, for the 2024/25 season.',
            'data' => $topscorers
        ]);
        $topscorers->save();


        /**
         * Calculate top scorers for the firsts
         *
         * @return int
         */

        $profiles = Profile::whereHas('goals', function($q) use($startDate){
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11],['team_id', 1]]);
            });
        })->with(['goals' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11],['team_id', 1]]);
            });
        }])->with(['squads' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11],['team_id', 1]]);
            });
        }])->get();
        
        foreach($profiles as $row){
            $row->thegoals = count($row->goals);
            $row->thesquads = count($row->squads);
        }
        $profiles = $profiles->sortBy('thesquads')->sortBy('thegoals', SORT_REGULAR, true);

        $result = new \DOMDocument();
        $result->formatOutput = true;
        $holder = $result->appendChild($result->createElement("div"));
        $holder->setAttribute('class', 'table-responsive');
        $table = $holder->appendChild($result->createElement("table"));
        $table->setAttribute('class', 'table table-striped');
        $thead = $table->appendChild($result->createElement("thead"));
        $tbody = $table->appendChild($result->createElement("tbody"));
        $newRow = $thead->appendChild($result->createElement("tr"));
        $newRow->appendChild($result->createElement("th", "Player"));
        $newRow->appendChild($result->createElement("th", "Goals Scored"));
        $newRow->appendChild($result->createElement("th", "Games"));

        foreach($profiles as $row)
        {
            $newRow = $tbody->appendChild($result->createElement("tr"));
            $newRow->appendChild($result->createElement("td", $row->full_name));
            $newRow->appendChild($result->createElement("td", count($row->goals)));
            $newRow->appendChild($result->createElement("td", $row->squads_count));
        }

        $topscorersfirsts = $result->saveXML($result->documentElement);
        $oldtable = Statistic::where([['title', "Top Scorers - Firsts"],['season', '2024/25']])->delete();
        $topscorersfirsts = Statistic::create([
            'title' => 'Top Scorers - Firsts',
            'season' => '2024/25',
            'notes' => 'Top scorers for the first team, excluding friendlies, for the 2024/25 season.',
            'data' => $topscorersfirsts
        ]);

        $topscorersfirsts->save();

        /**
         * Calculate top scorers for the seconds
         *
         * @return int
         */

        $profiles = Profile::whereHas('goals', function($q) use($startDate){
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11],['team_id', 2]]);
            });
        })->with(['goals' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11],['team_id', 2]]);
            });
        }])->with(['squads' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11],['team_id', 2]]);
            });
        }])->get();
        
        foreach($profiles as $row){
            $row->thegoals = count($row->goals);
            $row->thesquads = count($row->squads);
        }
        $profiles = $profiles->sortBy('thesquads')->sortBy('thegoals', SORT_REGULAR, true);

        $result = new \DOMDocument();
        $result->formatOutput = true;
        $holder = $result->appendChild($result->createElement("div"));
        $holder->setAttribute('class', 'table-responsive');
        $table = $holder->appendChild($result->createElement("table"));
        $table->setAttribute('class', 'table table-striped');
        $thead = $table->appendChild($result->createElement("thead"));
        $tbody = $table->appendChild($result->createElement("tbody"));
        $newRow = $thead->appendChild($result->createElement("tr"));
        $newRow->appendChild($result->createElement("th", "Player"));
        $newRow->appendChild($result->createElement("th", "Goals Scored"));
        $newRow->appendChild($result->createElement("th", "Games"));

        foreach($profiles as $row)
        {
            $newRow = $tbody->appendChild($result->createElement("tr"));
            $newRow->appendChild($result->createElement("td", $row->full_name));
            $newRow->appendChild($result->createElement("td", count($row->goals)));
            $newRow->appendChild($result->createElement("td", $row->squads_count));
        }

        $topscorersseconds = $result->saveXML($result->documentElement);
        $oldtable = Statistic::where([['title', "Top Scorers - Seconds"],['season', '2024/25']])->delete();
        $topscorersseconds = Statistic::create([
            'title' => 'Top Scorers - Seconds',
            'season' => '2024/25',
            'notes' => 'Top scorers for the second team, excluding friendlies, for the 2024/25 season.',
            'data' => $topscorersseconds
        ]);

        $topscorersseconds->save();


        /**
         * Calculate top scorers for the thirds
         *
         * @return int
         */

        $profiles = Profile::whereHas('goals', function($q) use($startDate){
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11],['team_id', 3]]);
            });
        })->with(['goals' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11],['team_id', 3]]);
            });
        }])->with(['squads' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11],['team_id', 3]]);
            });
        }])->get();

        foreach($profiles as $row){
            $row->thegoals = count($row->goals);
            $row->thesquads = count($row->squads);
        }
        $profiles = $profiles->sortBy('thesquads')->sortBy('thegoals', SORT_REGULAR, true);

        $result = new \DOMDocument();
        $result->formatOutput = true;
        $holder = $result->appendChild($result->createElement("div"));
        $holder->setAttribute('class', 'table-responsive');
        $table = $holder->appendChild($result->createElement("table"));
        $table->setAttribute('class', 'table table-striped');
        $thead = $table->appendChild($result->createElement("thead"));
        $tbody = $table->appendChild($result->createElement("tbody"));
        $newRow = $thead->appendChild($result->createElement("tr"));
        $newRow->appendChild($result->createElement("th", "Player"));
        $newRow->appendChild($result->createElement("th", "Goals Scored"));
        $newRow->appendChild($result->createElement("th", "Games"));

        foreach($profiles as $row)
        {
            $newRow = $tbody->appendChild($result->createElement("tr"));
            $newRow->appendChild($result->createElement("td", $row->full_name));
            $newRow->appendChild($result->createElement("td", count($row->goals)));
            $newRow->appendChild($result->createElement("td", $row->squads_count));
        }

        $topscorersthirds = $result->saveXML($result->documentElement);
        $oldtable = Statistic::where([['title', "Top Scorers - Thirds"],['season', '2024/25']])->delete();
        $topscorersthirds = Statistic::create([
            'title' => 'Top Scorers - Thirds',
            'season' => '2024/25',
            'notes' => 'Top scorers for the third team, excluding friendlies, for the 2024/25 season.',
            'data' => $topscorersthirds
        ]);

        $topscorersthirds->save();

        /**
         * Calculate player's appearances
         *
         * @return int
         */

        $profiles = Profile::whereHas('squads', function($q) use($startDate){
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11]]);
            });
        })->with(['squads' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11]]);
            });
        }])->get();

        foreach($profiles as $row){
            $row->thesquads = count($row->squads);
        }
        $profiles = $profiles->sortBy('thesquads', SORT_REGULAR, true);

        $result = new \DOMDocument();
        $result->formatOutput = true;
        $holder = $result->appendChild($result->createElement("div"));
        $holder->setAttribute('class', 'table-responsive');
        $table = $holder->appendChild($result->createElement("table"));
        $table->setAttribute('class', 'table table-striped');
        $thead = $table->appendChild($result->createElement("thead"));
        $tbody = $table->appendChild($result->createElement("tbody"));
        $newRow = $thead->appendChild($result->createElement("tr"));
        $newRow->appendChild($result->createElement("th", "Player"));
        $newRow->appendChild($result->createElement("th", "Appearances"));
       
        foreach($profiles as $row)
        {
            $newRow = $tbody->appendChild($result->createElement("tr"));
            $newRow->appendChild($result->createElement("td", $row->full_name));
            $newRow->appendChild($result->createElement("td", count($row->squads)));
        }

        $appearances = $result->saveXML($result->documentElement);
        $oldtable = Statistic::where([['title', "Appearances"],['season', '2024/25']])->delete();
        $appearances = Statistic::create([
            'title' => 'Appearances',
            'season' => '2024/25',
            'notes' => 'Appearances for any Newtown Forst team, excluding friendlies, for the 2024/25 season.',
            'data' => $appearances
        ]);

        $appearances->save();


        /**
         * Calculate top assists accross all teams
         *
         * @return int
         */

        $profiles = Profile::whereHas('assists', function($q) use($startDate){
            $q->whereHas('goal', function($goal) use($startDate){
                $goal->whereHas('game', function($game) use($startDate){
                    $game->whereDate('date','>=',$startDate)->where('competition_id', '!=', 11);
                });
            });
        })->with(['assists' => function ($q) use($startDate) {
            $q->whereHas('goal', function($goal) use($startDate){
                $goal->whereHas('game', function($game) use($startDate){
                    $game->whereDate('date','>=',$startDate)->where('competition_id', '!=', 11);
                });
            });
        }])
        ->withCount(['squads' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where([['competition_id', '!=', 11]]);
            });
        }])->get();

        foreach($profiles as $row){
            $row->theassists = count($row->assists);
        }
        $profiles = $profiles->sortBy('theassists', SORT_REGULAR, true);

        $result = new \DOMDocument();
        $result->formatOutput = true;
        $holder = $result->appendChild($result->createElement("div"));
        $holder->setAttribute('class', 'table-responsive');
        $table = $holder->appendChild($result->createElement("table"));
        $table->setAttribute('class', 'table table-striped');
        $thead = $table->appendChild($result->createElement("thead"));
        $tbody = $table->appendChild($result->createElement("tbody"));
        $newRow = $thead->appendChild($result->createElement("tr"));
        $newRow->appendChild($result->createElement("th", "Player"));
        $newRow->appendChild($result->createElement("th", "Assists"));
        $newRow->appendChild($result->createElement("th", "Games"));
       
        foreach($profiles as $row)
        {
            $newRow = $tbody->appendChild($result->createElement("tr"));
            $newRow->appendChild($result->createElement("td", $row->full_name));
            $newRow->appendChild($result->createElement("td", count($row->assists)));
            $newRow->appendChild($result->createElement("td", $row->squads_count));
        }

        $assists = $result->saveXML($result->documentElement);
        $oldtable = Statistic::where([['title', "Top Assists"],['season', '2024/25']])->delete();
        $assists = Statistic::create([
            'title' => 'Top Assists',
            'season' => '2024/25',
            'notes' => 'Top assists from all games, excluding friendlies, for the 2024/25 season.',
            'data' => $assists
        ]);

        $assists->save();


        /**
         * Calculate most motm accross all teams
         *
         * @return int
         */

        $profiles = Profile::whereHas('motm', function($q) use($startDate){
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where('competition_id', '!=', 11);
            });
        })->with(['motm' => function ($q) use($startDate) {
            $q->whereHas('game', function($g) use($startDate){
                $g->whereDate('date','>=',$startDate)->where('competition_id', '!=', 11);
            });
        }])->withCount('motm')->orderBy('motm_count', 'desc')->get();

        $result = new \DOMDocument();
        $result->formatOutput = true;
        $holder = $result->appendChild($result->createElement("div"));
        $holder->setAttribute('class', 'table-responsive');
        $table = $holder->appendChild($result->createElement("table"));
        $table->setAttribute('class', 'table table-striped');
        $thead = $table->appendChild($result->createElement("thead"));
        $tbody = $table->appendChild($result->createElement("tbody"));
        $newRow = $thead->appendChild($result->createElement("tr"));
        $newRow->appendChild($result->createElement("th", "Player"));
        $newRow->appendChild($result->createElement("th", "MOTM Performances"));
       
        foreach($profiles as $row)
        {
            $newRow = $tbody->appendChild($result->createElement("tr"));
            $newRow->appendChild($result->createElement("td", $row->full_name));
            $newRow->appendChild($result->createElement("td", count($row->motm)));
        }

        $topmotm = $result->saveXML($result->documentElement);
        $oldtable = Statistic::where([['title', "MOTM Performances"],['season', '2024/25']])->delete();
        $topmotm = Statistic::create([
            'title' => 'MOTM Performances',
            'season' => '2024/25',
            'notes' => 'Most MOTM Performances from all games, excluding friendlies, for the 2024/25 season.',
            'data' => $topmotm
        ]);

        $topmotm->save();
    }
}
