<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\LeagueTable;

class getLeagueTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getLeagueTables';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch the league tables from DAWFL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = "https://www.dawfl.com/fixtures/table/adult/dawfldivision1";
        $html = file_get_contents($url);
        libxml_use_internal_errors(true);
        $doc = new \DOMDocument();
        if($doc->loadHTML($html))
        {

            $result = new \DOMDocument();
            $result->formatOutput = true;
            $holder = $result->appendChild($result->createElement("div"));
            $holder->setAttribute('class', 'table-responsive');
            $table = $holder->appendChild($result->createElement("table"));
            $table->setAttribute('class', 'table table-striped');
            $thead = $table->appendChild($result->createElement("thead"));
            $tbody = $table->appendChild($result->createElement("tbody"));

            $xpath = new \DOMXPath($doc);

            $newRow = $thead->appendChild($result->createElement("tr"));
                    
            foreach($xpath->query("//table[contains(concat(' ', normalize-space(@class), ' '), ' UhXTve ')]/thead/tr/th[position()>0]") as $header)
            {
                $newRow->appendChild($result->createElement("th", trim($header->nodeValue)));
            }

            foreach($xpath->query("//table[contains(concat(' ', normalize-space(@class), ' '), ' UhXTve ')]/tbody/tr") as $row)
            {
                $newRow = $tbody->appendChild($result->createElement("tr"));

                foreach($xpath->query("./td[position()>0]", $row) as $cell)
                {
                    $newRow->appendChild($result->createElement("td", trim($cell->nodeValue)));
                }
            }

            $leaguetable = $result->saveXML($result->documentElement);
            $oldtable = LeagueTable::where('team_id', '1')->delete();
            $firststable = LeagueTable::create([
                'league_table' => $leaguetable,
                'team_id' => 1
            ]);
            $firststable->save();

        }

        $url2 = "https://www.dawfl.com/fixtures/table/adult/dawflreservedivision1";
        $html2 = file_get_contents($url2);
        libxml_use_internal_errors(true);
        $doc2 = new \DOMDocument();
        if($doc2->loadHTML($html2))
        {

            $result2 = new \DOMDocument();
            $result2->formatOutput = true;
            $holder2 = $result2->appendChild($result2->createElement("div"));
            $holder2->setAttribute('class', 'table-responsive');
            $table2 = $holder2->appendChild($result2->createElement("table"));
            $table2->setAttribute('class', 'table table-striped');
            $thead2 = $table2->appendChild($result2->createElement("thead"));
            $tbody2 = $table2->appendChild($result2->createElement("tbody"));

            $xpath2 = new \DOMXPath($doc2);

            $newRow2 = $thead2->appendChild($result2->createElement("tr"));
                    
            foreach($xpath2->query("//table[contains(concat(' ', normalize-space(@class), ' '), ' UhXTve ')]/thead/tr/th[position()>0]") as $header2)
            {
                $newRow2->appendChild($result2->createElement("th", trim($header2->nodeValue)));
            }

            foreach($xpath2->query("//table[contains(concat(' ', normalize-space(@class), ' '), ' UhXTve ')]/tbody/tr") as $row2)
            {
                $newRow2 = $tbody2->appendChild($result2->createElement("tr"));

                foreach($xpath2->query("./td[position()>0]", $row2) as $cell2)
                {
                    $newRow2->appendChild($result2->createElement("td", trim($cell2->nodeValue)));
                }
            }

            $leaguetable2 = $result2->saveXML($result2->documentElement);
            $oldtable2 = LeagueTable::where('team_id', '2')->delete();
            $secondsstable = LeagueTable::create([
                'league_table' => $leaguetable2,
                'team_id' => 2
            ]);
            $secondsstable->save();
        }
    }
}
