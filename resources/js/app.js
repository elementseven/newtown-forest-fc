/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require("babel-polyfill");
require("whatwg-fetch");
require('./bootstrap');
window.LazyLoad = require('vanilla-lazyload');
import Vue from 'vue';

require('./plugins/modernizr-custom.js'); 
require('waypoints/lib/jquery.waypoints.min.js');
require('lite-youtube-embed/src/lite-yt-embed.js');
require('./plugins/cookieConsent.js');

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);

import { Datetime } from 'vue-datetime';
Vue.component('datetime', Datetime);

import vueDebounce from 'vue-debounce';
Vue.use(vueDebounce, {
  listenTo: ['input', 'keyup']
});

window.AOS = require('AOS');
AOS.init();
window.addEventListener('load', AOS.refresh);
window.addEventListener('resize', AOS.refresh);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

Vue.component('main-menu', () => import(/* webpackChunkName: "MainMenu" */ './components/Menus/MainMenu.vue'));
Vue.component('mobile-menu', () => import(/* webpackChunkName: "MobileMenu" */ './components/Menus/MobileMenu.vue'));
Vue.component('site-footer', () => import(/* webpackChunkName: "Footer" */ './components/Footer.vue'));
Vue.component('seen-enough', () => import(/* webpackChunkName: "SeenEnough" */ './components/SeenEnough.vue'));

Vue.component('mailing-list', () => import(/* webpackChunkName: "MailingList" */ './components/MailingList.vue'));
Vue.component('loader', () => import(/* webpackChunkName: "Loader" */ './components/Loader.vue'));

Vue.component('contact-page-form', () => import(/* webpackChunkName: "ContactPageForm" */ './components/Contact/ContactPageForm.vue'));
Vue.component('join-form', require(/* webpackChunkName: "JoinForm" */ './components/Contact/JoinForm.vue').default);

Vue.component('news-index', () => import(/* webpackChunkName: "NewsIndex" */ './components/news/Index.vue'));
Vue.component('news-show', () => import(/* webpackChunkName: "NewsShow" */ './components/news/Show.vue'));
Vue.component('news-home', () => import(/* webpackChunkName: "NewsHome" */ './components/news/Home.vue'));

Vue.component('games-index', require(/* webpackChunkName: "GamesIndex" */ './components/Games/Index.vue').default);
Vue.component('games-show', () => import(/* webpackChunkName: "GamesShow" */ './components/Games/Show.vue'));

Vue.component('profiles-index', () => import(/* webpackChunkName: "ProfilesIndex" */ './components/Profiles/Index.vue'));
Vue.component('profiles-show', () => import(/* webpackChunkName: "ProfilesShow" */ './components/Profiles/Show.vue'));
Vue.component('profile-games', () => import(/* webpackChunkName: "ProfileGames" */ './components/Profiles/ProfileGames.vue'));

Vue.component('upcoming-fixtures', require(/* webpackChunkName: "UpcomingFixtures" */ './components/Games/UpcomingFixtures.vue').default);

Vue.component('first-team-league-table', () => import(/* webpackChunkName: "FirstTeamLeagueTable" */ './components/LeagueTables/FirstTeam.vue'));
Vue.component('second-team-league-table', () => import(/* webpackChunkName: "SecondTeamLeagueTable" */ './components/LeagueTables/SecondTeam.vue'));

Vue.component('statistics', require(/* webpackChunkName: "Statistics" */ './components/Statistics.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
