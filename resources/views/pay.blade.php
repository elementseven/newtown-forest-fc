@php
$page = 'Club Membership';
$pagetitle = 'Club Membership | Newtown Forest Football Club';
$metadescription = 'Club Membership - Newtown Forest';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative pt-5 mt-5 mob-mt-0 ipadp-pt-0 menu-padding">
	<img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
	<div class="row">
		<div class="container">	
		  <div class="row mt-4 position-relative z-2">
		    <div class="col-12 mt-5 pt-5 ipadp-pt-0 mob-mt-0 mob-pt-0 text-center">
		      <div class="pre-title-lines mob-mx-auto mb-4 mob-my-45"></div>
		      <h1 class="mob-mt-0">Club Membership</h1>
		      <p class="text-large">Register for ongoing monthly club membership</p>
		    </div>
		  </div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative pb-5 mob-pb-4 position-relative z-2">
  <div class="row">
    <div class="container pt-4">
      <div class="row justify-content-center">
      	<div class="col-lg-8 mob-px-4">
          <div id="mc2x1aujytt1s4" class="py-5" style="background-color: #f3f4f7;"><a href="https://app.moonclerk.com/pay/2x1aujytt1s4">2024/25 Membership Fees</a></div>
    		</div>
      </div>
    </div>
  </div>
</div>
<div class="mb-5"></div>
@endsection
@section('scripts')
<script type="text/javascript">var mc2x1aujytt1s4;(function(d,t) {var s=d.createElement(t),opts={"checkoutToken":"2x1aujytt1s4","width":"100%"};s.src='https://d2l7e0y6ygya2s.cloudfront.net/assets/embed.js';s.onload=s.onreadystatechange = function() {var rs=this.readyState;if(rs) if(rs!='complete') if(rs!='loaded') return;try {mc2x1aujytt1s4=new MoonclerkEmbed(opts);mc2x1aujytt1s4.display();} catch(e){}};var scr=d.getElementsByTagName(t)[0];scr.parentNode.insertBefore(s,scr);})(document,'script');</script>
@endsection