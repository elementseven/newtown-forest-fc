@php
$page = 'Summer League';
$pagetitle = 'Summer League | Newtown Forest Football Club';
$metadescription = 'Rules, fixtures & more info on the Newtown Forest summer league 2023';
$pagetype = 'white';
$pagename = 'summer-league';
$ogimage = 'https://newtownforest.com/img/logos/summer-league.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative ">
	{{-- <img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/> --}}
	<div class="row">
		<div class="container px-0">	
		  <div class="row mt-4 position-relative z-2">
		    <div class="col-lg-12 mt-5 mob-mt-0 text-center text-lg-left">
		      <img src="/img/logos/summer-league.jpg" class="w-100 d-none d-lg-block" alt="summer league logo" />
		      <img src="/img/logos/summer-league-mob.jpg" class="w-100 d-lg-none" alt="summer league logo mobile" />
		    </div>
		  </div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative pb-5 mob-pb-4 position-relative z-2">
  <div class="row">
    <div class="container pt-4">
      <div class="row justify-content-center">
      	<div class="col-lg-12 mob-px-4">
      		<h2 class="mb-3">League Table</h2>
      		<div class="table-responsive">
						<table class="table table-striped" role="grid">
						  <tr>
						    <th>Team</th>
						    <th>MP</th>
						    <th>W</th>
						    <th>D</th>
						    <th>L</th>
						    <th>GD</th>
						    <th>Pts</th>
						  </tr>
						  <tbody>
						  	<tr>
						      <td>Fat 2 Furious</td>
						      <td>3</td>
						      <td>2</td>
						      <td>0</td>
						      <td>1</td>
						      <td>+10</td>
						      <td>6</td>
						    </tr>
						  	<tr>
						      <td>Team C</td>
						      <td>3</td>
						      <td>2</td>
						      <td>0</td>
						      <td>1</td>
						      <td>+7</td>
						      <td>6</td>
						    </tr>
						    <tr>
						      <td>Team F</td>
						      <td>1</td>
						      <td>1</td>
						      <td>0</td>
						      <td>0</td>
						      <td>+1</td>
						      <td>3</td>
						    </tr>
						    <tr>
						      <td>Team B</td>
						      <td>2</td>
						      <td>1</td>
						      <td>0</td>
						      <td>1</td>
						      <td>-3</td>
						      <td>3</td>
						    </tr>
						    <tr>
						      <td>Team E</td>
						      <td>2</td>
						      <td>1</td>
						      <td>0</td>
						      <td>1</td>
						      <td>-5</td>
						      <td>3</td>
						    </tr>
						    <tr>
						      <td>Team D-lightful</td>
						      <td>3</td>
						      <td>0</td>
						      <td>0</td>
						      <td>3</td>
						      <td>-5</td>
						      <td>0</td>
						    </tr>
						  </tbody>
						</table>
					</div>
    		</div>
      </div>
      <div class="row mt-5">
      	<div class="col-12">
      		<h2 class="mb-3">Fixtures & Results</h2>
      	</div>
      	<div class="col-lg-4 mb-4">
      		<div class="card fixture-card">
      			<div class="card-header border-0 bg-primary text-center">
      				<p class="text-small letter-spacing text-uppercase text-dark mb-0"><b>Round 1</b></p> 
      				<p class="text-smallest letter-spacing text-uppercase text-dark mb-0">22nd May 2023 - 4th June 2023</p>
      			</div> 
      			<div class="card-body px-0 py-3">
      				<div class="container">
      					<div class="row">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Fat 2 Furious</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team B</span></p>
    							</div>
    						</div>
    						<div class="row pt-2">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Team C</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team D</span></p>
    							</div>
    						</div>
    						<div class="row pt-2">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Team E</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team F</span></p>
    							</div>
    						</div>
    					</div> 
    				</div>
    			</div>
      	</div>

      	<div class="col-lg-4 mb-4">
      		<div class="card fixture-card">
      			<div class="card-header border-0 bg-primary text-center">
      				<p class="text-small letter-spacing text-uppercase text-dark mb-0"><b>Round 2</b></p> 
      				<p class="text-smallest letter-spacing text-uppercase text-dark mb-0">29th May 2023 - 11th June 2023</p>
      			</div> 
      			<div class="card-body px-0 py-3">
      				<div class="container">
      					<div class="row">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Team D</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>6 - 7</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team E</span></p>
    							</div>
    						</div>
    						<div class="row pt-2">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Team B</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team C</span></p>
    							</div>
    						</div>
    						<div class="row pt-2">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Fat 2 Furious</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team F</span></p>
    							</div>
    						</div>
    					</div> 
    				</div>
    			</div>
      	</div>

      	<div class="col-lg-4 mb-4">
      		<div class="card fixture-card">
      			<div class="card-header border-0 bg-primary text-center">
      				<p class="text-small letter-spacing text-uppercase text-dark mb-0"><b>Round 3</b></p> 
      				<p class="text-smallest letter-spacing text-uppercase text-dark mb-0">5th June 2023 - 18th June 2023</p>
      			</div> 
      			<div class="card-body px-0 py-3">
      				<div class="container">
      					<div class="row">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Fat 2 Furious</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team E</span></p>
    							</div>
    						</div>
    						<div class="row pt-2">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Team B</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team D</span></p>
    							</div>
    						</div>
    						<div class="row pt-2">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Team C</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team F</span></p>
    							</div>
    						</div>
    					</div> 
    				</div>
    			</div>
      	</div>

				<div class="col-lg-4 mb-4">
      		<div class="card fixture-card">
      			<div class="card-header border-0 bg-primary text-center">
      				<p class="text-small letter-spacing text-uppercase text-dark mb-0"><b>Round 4</b></p> 
      				<p class="text-smallest letter-spacing text-uppercase text-dark mb-0">12th June 2023 - 25th June 2023</p>
      			</div> 
      			<div class="card-body px-0 py-3">
      				<div class="container">
      					<div class="row">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Fat 2 Furious</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team C</span></p>
    							</div>
    						</div>
    						<div class="row pt-2">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Team B</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team E</span></p>
    							</div>
    						</div>
    						<div class="row pt-2">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Team D</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team F</span></p>
    							</div>
    						</div>
    					</div> 
    				</div>
    			</div>
      	</div>

      	<div class="col-lg-4 mb-4">
      		<div class="card fixture-card">
      			<div class="card-header border-0 bg-primary text-center">
      				<p class="text-small letter-spacing text-uppercase text-dark mb-0"><b>Round 5</b></p> 
      				<p class="text-smallest letter-spacing text-uppercase text-dark mb-0">19th June 2023 - 2nd July 2023</p>
      			</div> 
      			<div class="card-body px-0 py-3">
      				<div class="container">
      					<div class="row">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Fat 2 Furious</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team D</span></p>
    							</div>
    						</div>
    						<div class="row pt-2">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Team B</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>VS</b></p></div> 
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team F</span></p>
    							</div>
    						</div>
    						<div class="row pt-2">
    							<div class="col-4 pl-0 text-right"><p class="mb-0 text-dark"><span>Team C</span></p></div> 
    							<div class="col-4 text-center px-0"><p class="text-dark text-uppercase letter-spacing mb-0"><b>21 - 15</b></p></div>
    							<div class="col-4 pr-0"><p class="mb-0 text-dark"><span>Team E</span></p>
    							</div>
    						</div>
    					</div> 
    				</div>
    			</div>
      	</div>

      </div>
      <div class="row mt-5">
      	<div class="col-12">
      		<h2 class="mb-3">Rules</h2>
      		<ol>
	      		<li>Teams are responsible for arranging their own fixtures. For each fixture, teams will have 2 weeks to arrange and play their match, otherwise they will forfeit the points.</li>

						<li>Home team captains will be responsible for booking pitches and arranging payment from their team (eg, for Team A Vs Team B, Team A is the home team).</li>
						
						<li>Matches can be either 5 or 6-a-side and will be no longer than one hour. Both of these points are flexible, but need to be agreed between the teams prior to the match.</li>

						<li>Temporary player swaps are allowed in order to facilitate fixtures. However, players being swapped in must be from a lower hat (ie, a first team player can be swapped out for a second team player, but the reverse is not allowed). Please confirm lineups in the chat before each game.</li>

						<li>If a team is late, and kick-off is delayed, the late team will be penalised by having one goal subtracted from their overall goal difference for every 5 minute delay. Goalscorer stats will be unaffected.</li>

						<li>Niall Kennedy must be subbed off if he scores more than 3 goals in a game.</li>

						<li>Matches will be self-refereed. We all know what constitutes a foul. Just get on with it.</li>

						<li>Drop Paddy or Phil a message after your fixtures to provide scores and goalscorers.</li>

						<li>If you're in possession of any, use common sense.</li>

					</ol>
      	</div>

      </div>
      <div class="row mt-5">
      	<div class="col-12 mb-3">
      		<h2>Teams</h2>
      	</div>
      	<div class="col-6 col-lg-4 mb-4">
      		<div class="card p-4">
      			<p class="text-large"><b>Fat 2 Furious</b></p>
      			<p>William Leeman</p>
      			<p>Patrick Turner</p>
      			<p>Will Quinn</p>
      			<p>Adam McDowell</p>
      			<p>Eoghan Adams</p>
      			<p class="mb-0">Shane Gibson</p>
      		</div>
      	</div>
      	<div class="col-6 col-lg-4 mb-4">
      		<div class="card p-4">
      			<p class="text-large"><b>Team B</b></p>
      			<p>Brendan McCrisken</p>
      			<p>Jack Devlin</p>
      			<p>Pearce McConkey</p>
      			<p>Rigsy</p>
      			<p>Andrew Harrison</p>
      			<p class="mb-0">Brendan Miskelly</p>
      		</div>
      	</div>
      	<div class="col-6 col-lg-4 mb-4">
      		<div class="card p-4">
      			<p class="text-large"><b>Team C</b></p>
      			<p>Aaron O'Reilly</p>
      			<p>Matt Bell</p>
      			<p>Matt McCrudden</p>
      			<p>Paul Gibson</p>
      			<p>Gary Hassan</p>
      			<p class="mb-0">Mark McLaughin</p>
      		</div>
      	</div>
      	<div class="col-6 col-lg-4 mb-4">
      		<div class="card p-4">
      			<p class="text-large"><b>Team D</b></p>
      			<p>Jack O'Hagan</p>
      			<p>Darren O'Hara</p>
      			<p>Tom Steer</p>
      			<p>Imaad Syed</p>
      			<p>Phil Taggart</p>
      			<p class="mb-0">Peter Skelton</p>
      		</div>
      	</div>
      	<div class="col-6 col-lg-4 mb-4">
      		<div class="card p-4">
      			<p class="text-large"><b>Team E</b></p>
      			<p>Mick Steele</p>
      			<p>Phil McLaughlin</p>
      			<p>Leigh Kimmins</p>
      			<p>Phil Lisk</p>
      			<p>Luke McKelvey</p>
      			<p class="mb-0">Steven Chapman</p>
      		</div>
      	</div>
      	<div class="col-6 col-lg-4 mb-4">
      		<div class="card p-4">
      			<p class="text-large"><b>Team F</b></p>
      			<p>Sam Halliday</p>
      			<p>Niall Kennedy</p>
      			<p>Tristan Crowe</p>
      			<p>Callum McNabb</p>
      			<p>Francis McDermott</p>
      			<p class="mb-0">Peter Brimstone</p>
      		</div>
      	</div>
      </div>
    </div>
  </div>
</div>
<div class="mb-5"></div>

@endsection

@section('scripts')
<style>
	body ol {
  margin:0 0 1.5em;
  padding:0;
  counter-reset:item;
}
 
body ol>li {
	font-size: 18px;
	margin-bottom: 15px;
  padding:0 0 0 1.5em;
  text-indent:-1.5em;
  list-style-type:none;
  counter-increment:item;
}
 
body ol>li:before {
  display:inline-block;
  width:1.5em !important;
  padding-right:0.5em;
  font-weight:bold;
  text-align:right;
  content:counter(item) "." !important;
  left:auto !important;
  top: auto !important;
  position: relative  !important;
  background-color: transparent !important;
}

</style>
@endsection