@php
$page = 'Contact';
$pagetitle = 'Contact | Newtown Forest Football Club';
$metadescription = 'Get in touch with Newtown Forest by email or using our online contact form!';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative pt-5 mt-5 mob-mt-0 ipadp-pt-0 menu-padding">
	<img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
	<div class="row">
		<div class="container">	
		  <div class="row mt-4 position-relative z-2">
		    <div class="col-lg-9 mt-5 pt-5 ipadp-pt-0 mob-mt-0 mob-pt-0 text-center text-lg-left">
		      <div class="pre-title-lines mob-mx-auto mb-4 mob-my-45"></div>
		      <h1 class="mob-mt-0">Get in touch</h1>
		      <p>It's easy to contact us, you can use the form below to send us a message or send an email to <a href="mailto:club@newtownforest.com">club@newtownforest.com</a> or if you prefer, send us a message on <a href="https://www.facebook.com/NewtownForestFC" aria-label="Facebook" rel="noreferrer" target="_blank">Facebook</a>.</p>
		    </div>
		  </div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative pb-5 mob-pb-4 position-relative z-2">
  <div class="row">
    <div class="container pt-4">
      <div class="row justify-content-center">
      	<div class="col-lg-12 mob-px-4">
          <contact-page-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></contact-page-form>
    		</div>
      </div>
    </div>
  </div>
</div>
<div class="mb-5"></div>

@endsection