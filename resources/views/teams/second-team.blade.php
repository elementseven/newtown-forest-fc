@php
$page = 'Second Team';
$pagetitle = 'Second Team | Our Club | Newtown Forest Football Club';
$metadescription = 'Our second team competes in the Down Area Winter Football League - Reserve Division 1, the Billy Allen Memorial Shield and the FonaCab Junior Cup. Home games take place at Hydebank Playing Fields in South Belfast, with away games taking place across Belfast and the surrounding area.';
$pagetype = 'white';
$pagename = 'our-club';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container-fluid position-relative z-2 bg-navy seconds-players-bg bg-bottom-contain top-padding">
  <div class="bottom-grad"></div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative pb-5 mob-pb-0 position-relative z-2">
	<img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
  <div class="row">
    <div class="container py-5 mob-pt-0">
      <div class="row py-5 justify-content-center half_row">
      	<div class="col-lg-6 mob-px-4 pr-5 ipadp-mb-5 mob-px-4 half_col text-center text-lg-left mob-mb-5">
      		<p class="text-smallest letter-spacing text-blue text-uppercase mb-0">DAWFL Reserve Division 1</p>
      		<h1>Second Team</h1>
      		<p>Our second team competes in the Down Area Winter Football League - Reserve Division 1, the Billy Allen Memorial Shield and the FonaCab Junior Cup. Home games take place at Hydebank Playing Fields in South Belfast, with away games taking place across Belfast and the surrounding area.</p>
      		<p>Phil McLaughlin continues coaching the second team for the 2023/24 season, with James Patterson resuming his role as his assistant.</p>
      		<p>Supporters, friends & family are welcome and encouraged to come along and watch our second team games, which usually take place on Saturday afternoons at 14:00.</p>
    		</div>
    		<div class="col-lg-3 col-6 text-center half_col">
          <picture>
            <source srcset="/img/coaches/phil-mclaughlin.webp" type="image/webp"/> 
            <source srcset="/img/coaches/phil-mclaughlin.jpg" type="image/jpeg"/> 
              <img src="/img/coaches/phil-mclaughlin.jpg" type="image/jpeg" class="w-100" alt="Phil McLaughlin - Second Team Manager - Newtown Forest FC"/>
          </picture>
    			<p class="mb-0 mt-2 text-large"><b>Phil McLaughlin</b></p>
    			<p class="text-smallest letter-spacing text-uppercase">Second Team Manager</p>
    		</div>
    		<div class="col-lg-3 col-6 text-center half_col">
          <picture>
            <source srcset="/img/coaches/rob-mccabe.webp" type="image/webp"/> 
            <source srcset="/img/coaches/rob-mccabe.jpg" type="image/jpeg"/> 
              <img src="/img/coaches/rob-mccabe.jpg" type="image/jpeg" class="w-100" alt="Robert McCabe - Second Team Assistant Manager - Newtown Forest FC"/>
          </picture>
    			<p class="mb-0 mt-2 text-large"><b>Robert McCabe</b></p>
    			<p class="text-smallest letter-spacing text-uppercase">Second Team Assistant Manager</p>
    		</div>
      </div>
    </div>
  </div>
  <div class="row mb-5">
    <upcoming-fixtures :team="'2'"></upcoming-fixtures>
	</div>
  <div class="row">
    <div class="container py-5 mob-pt-0">
      <div class="row">
      	<div class="col-12 pt-5 mob-pt-0">
          <p class="mimic-h3 mb-4 mob-mb-0 text-center text-lg-left">League Table</p>
          <p class="text-small text-blue d-md-none text-center text-lg-left">*Scroll sideways to see more</p>
          <second-team-league-table></second-team-league-table>
  		  </div>
      </div>
      <div class="row mt-5 py-5 mob-py-3">
        <div class="col-lg-8">
          <seen-enough></seen-enough>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection