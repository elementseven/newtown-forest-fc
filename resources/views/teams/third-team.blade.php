@php
$page = 'Third Team';
$pagetitle = 'Third Team | Our Club | Newtown Forest Football Club';
$metadescription = 'Our Third team competes in the Belfast & District Football League - Division 4. Home games take place at Hydebank Playing Fields in South Belfast, with away games taking place across Belfast and the surrounding area.';
$pagetype = 'white';
$pagename = 'our-club';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container-fluid position-relative z-2 bg-navy thirds-players-bg bg-bottom-contain top-padding">
	<div class="bottom-grad"></div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative pb-5 position-relative z-2">
	<img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
	<div class="row">
		<div class="container py-5 mob-pt-0">
			<div class="row py-5 justify-content-center half_row">
				<div class="col-lg-6 mob-px-4 pr-5 ipadp-mb-5 mob-px-4 half_col text-center text-lg-left mob-mb-5">
					<p class="text-smallest letter-spacing text-blue text-uppercase mb-0">BDFL Division 4</p>
					<h1>Third Team</h1>
					<p>Our Third team competes in the Belfast & District Football League - Division 4. Home games take place at Hydebank Playing Fields in South Belfast, with away games taking place across Belfast and the surrounding area.</p>
					<p>Looking forward to a competitive season ahead, Luke McKelvey continues managing the third team in BDFL division 4, supported by assistant manager & goalkeeping coach Adam Mcdowell. </p>
					<p class="mb-0">Supporters, friends & family are welcome and encouraged to come along and watch our third team games, which usually take place on Saturday afternoons at 14:00.</p>
				</div>
				<div class="col-lg-3 col-6 text-center half_col">
					<picture>
						<source srcset="/img/coaches/luke-mckelvey.webp" type="image/webp"/> 
						<source srcset="/img/coaches/luke-mckelvey.jpg" type="image/jpeg"/> 
						<img src="/img/coaches/luke-mckelvey.jpg" type="image/jpeg" class="w-100" alt="Luke McKelvey - Third Team Manager - Newtown Forest FC"/>
					</picture>
					<p class="mb-0 mt-2 text-large"><b>Luke McKelvey</b></p>
					<p class="text-smallest letter-spacing text-uppercase">Third Team Manager</p>
				</div>
				<div class="col-lg-3 col-6 text-center half_col">
					<picture>
						<source srcset="/img/coaches/adam-mcdowell.webp" type="image/webp"/> 
						<source srcset="/img/coaches/adam-mcdowell.jpg" type="image/jpeg"/> 
						<img src="/img/coaches/adam-mcdowell.jpg" type="image/jpeg" class="w-100" alt="Adam McDowell - Third Team Assistant Manager - Newtown Forest FC"/>
					</picture>
					<p class="mb-0 mt-2 text-large"><b>Adam McDowell</b></p>
					<p class="text-smallest letter-spacing text-uppercase">Third Team Assistant Manager</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row mb-5">
		<upcoming-fixtures :team="'3'"></upcoming-fixtures>
	</div>
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-12 pt-5 mob-pt-0">
					<p class="mimic-h3 mb-4 mob-mb-0 text-center text-lg-left">League Table</p>
					<p class="text-small text-blue d-md-none text-center text-lg-left">*Scroll sideways to see more</p>
					<div class="table-responsive">
						<table class="table table-striped" role="grid">
						  <tr>
						    <th>Pos.</th>
						    <th>Club</th>
						    <th>MP</th>
						    <th>W</th>
						    <th>D</th>
						    <th>L</th>
						    <th>GF</th>
						    <th>GA</th>
						    <th>GD</th>
						    <th>Pts</th>
						  </tr>
						  <tbody>
						    <tr>
						      <td>1</td>
						      <td>22nd OB III</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						    </tr>
						    <tr>
						      <td>2</td>
						      <td>Albert Foundry III</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						    </tr>
						    <tr>
						      <td>3</td>
						      <td>Agape Olympic</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						    </tr>
						    <tr>
						      <td>4</td>
						      <td>Andytown Harps</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						    </tr>
						    <tr>
						      <td>5</td>
						      <td>Borrusia N'Abbey</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						    </tr>
						    <tr>
						      <td>6</td>
						      <td>Ford FC III</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						    </tr>
						    <tr>
						      <td>7</td>
						      <td>Glenavy</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						    </tr>
						    <tr>
						      <td>8</td>
						      <td>Newtown Forest III</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						    </tr>
						    <tr>
						      <td>9</td>
						      <td>Willowfield 10th OB Arrows</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						    </tr>
{{-- 						    <tr>
						      <td>10</td>
						      <td>Cumann Spoirt an Phobail III</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						      <td>0</td>
						    </tr> --}}
						  </tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row mt-5 py-5 mob-py-3">
				<div class="col-lg-8">
					<seen-enough></seen-enough>
				</div>
			</div>
		</div>
	</div>
	@endsection