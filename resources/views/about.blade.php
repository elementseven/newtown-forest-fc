@php
$page = 'About';
$pagetitle = 'About | Newtown Forest Football Club';
$metadescription = "Find out about Newtown Forest Football Club, from the club’s formation and 25 year history to what we are doing today and plans for the future!";
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative pt-5 mt-5 menu-padding">
	<img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
	<div class="row">
		<div class="container">	
			<div class="row mt-4 position-relative z-2">
				<div class="col-lg-10 col-xl-8 mt-5 pt-5 ipadp-pt-0 mob-mt-0 mob-pt-0 text-center text-lg-left">
					<h1 class="mob-mt-0">About Our Club</h1>
					<p class="mb-1 text-large"><i>"Everything begins with an idea"</i></p>
					<p class="mb-4">Earl Nightingale</p>
					<a href="/join">
						<button type="button" class="btn btn-primary">Join us</button>
					</a>
				</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative pb-5 mob-pb-0 position-relative z-2">
	<div class="row">
		<div class="container mob-pt-5">
			<div class="row py-5">
				<div class="col-12 d-lg-none">
					<picture>
						<source srcset="/img/about/paper.webp" type="image/webp" />
						<source srcset="/img/about/paper.jpg" type="image/jpeg" />
						<img src="/img/about/paper.jpg" class="w-100" alt="Newtown Forest in the newspaper 1996" />
					</picture>
				</div>
				<div class="col-lg-7 pr-5 mob-px-4 ipadp-px-3 text-center text-lg-left pb-5 pt-4 mob-pb-3">
					<div class="d-table w-100 h-100">
						<div class="d-table-cell w-100 h-100 align-middle">
							<h2>Established 1995</h2>
							<p>Newtown Forest FC started with a bet between a couple of friends in Newtownbreda High School in 1995. It was simple, could we start a football team with players who weren't being selected to play for the school. Fast forward almost 30 years and we have three successful teams allowing friends to keep fit every week. More importantly however we have allowed people to make connections and we support people socially, emotionally and mentally within the club's in family atmosphere.</p>
							<p>During the first season in 1995/1996 we could only play friendlies until we were accepted into the Old Boys Football League.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-5 d-none d-lg-block">
					<picture>
						<source srcset="/img/about/paper.webp" type="image/webp" />
						<source srcset="/img/about/paper.jpg" type="image/jpeg" />
						<img src="/img/about/paper.jpg" class="w-100" alt="Steven Campbell Newtown Forest 2013" />
					</picture>
				</div>
			</div>
			<div class="row py-5 mob-pt-0">
				<div class="col-lg-5">
					<picture>
						<source srcset="/img/about/jamespatterson.webp" type="image/webp" />
						<source srcset="/img/about/jamespatterson.jpg" type="image/jpeg" />
						<img src="/img/about/jamespatterson.jpg" class="w-100" alt="James Patterson Newtown Forest 2012" />
					</picture>
					<figcaption class="text-center text-lg-left text-light">[ James Patterson playing against Willowfield in 2012 ]</figcaption>
				</div>
				<div class="col-lg-7 pl-5 mob-px-3 ipadp-px-3 text-center text-lg-left pb-5 mob-pt-3 mob-pb-3">
					<div class="d-table w-100 h-100">
						<div class="d-table-cell w-100 h-100 align-middle">
							<h2>Cross-community</h2>
							<p>It was in 1996/1997 that the club really took off, with the founding members Kyle Rendall, Keith Hunter, Darren Floyd, Mark Butterwood, Rory Lyndsey and Michael young to name a few. They joined with the 8th D members Andrew Rendall, David Galloway, William Gillan, Eddie Carlin, Thomas Ward, Richard Thompson and also the original members of the 97th old boys Keith Brunt, William Morrow and Davy Noonan to reform the 97th FC and create the club whose number is still part of our club badge today.</p>
							<p>From the first moment the club was created the underlying principle was that we would always be an all-inclusive mixed club, something to this day which makes us all proud considering this was done before the good Friday agreement.</p>
							
						</div>
					</div>
				</div>
			</div>
			<div class="row pt-5 mob-pt-0">
				<div class="col-12 d-lg-none">
					<picture>
						<source srcset="/img/about/noodles.webp" type="image/webp" />
						<source srcset="/img/about/noodles.jpg" type="image/jpeg" />
						<img src="/img/about/noodles.jpg" class="w-100" alt="Steven Campbell Newtown Forest 2013" />
					</picture>
					<figcaption class="text-center text-lg-left text-light">[ Steven Campbell celebrates a goal against 43rd OB in 2013 ]</figcaption>
				</div>
				<div class="col-lg-7 pr-5 mob-px-4 ipadp-px-3 text-center text-lg-left pb-5 pt-4 mob-pb-3">
					<div class="d-table w-100 h-100">
						<div class="d-table-cell w-100 h-100 align-middle">
							<h2>30 Years of Progress</h2>
							<p>The original founding committee comprised of Andrew Rendall, Kyle Rendall, Keith Brunt, Keith Hunter and David Galloway. During the early years in the Old Boys League we were promoted 6 divisions in 8 seasons as the club grew both on and off the pitch. </p>
							<p>In 2005 the club was rebranded thanks to Steven Campbell, Ross McCaughrain and Craig Murray along with others. We have been so blessed to have had so many play for us over the years.</p>
							<p>The club moved to the Down Area Winter Football League in 2005. Since then we have had youth teams and women’s teams over the years and have raised thousands of pounds for charity. This again shows how much the club has done within the local community.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-5 d-none d-lg-block">
					<picture>
						<source srcset="/img/about/noodles.webp" type="image/webp" />
						<source srcset="/img/about/noodles.jpg" type="image/jpeg" />
						<img src="/img/about/noodles.jpg" class="w-100" alt="Steven Campbell Newtown Forest 2013" />
					</picture>
					<figcaption class="text-center text-lg-left text-light">[ Steven Campbell celebrates a goal against 43rd OB in 2013 ]</figcaption>
				</div>
				<div class="col-lg-7 pr-5 mob-px-4 ipadp-px-3 text-center text-lg-left pb-5 pt-4 mob-pb-3">
				</div>
			</div>
			<div class="row py-5 mob-pt-0">
				<div class="col-lg-5">
					<picture>
						<source srcset="/img/about/niall.webp" type="image/webp" />
						<source srcset="/img/about/niall.jpg" type="image/jpeg" />
						<img src="/img/about/niall.jpg" class="w-100" alt="Niall Kennedy Newtown Forest 2016" />
					</picture>
					<figcaption class="text-center text-lg-left text-light">[ Niall Kennedy hits a cross in the Billy Allen Memorial Shield final - 2016 ]</figcaption>
				</div>
				<div class="col-lg-7 pl-5 mob-px-3 ipadp-px-3 text-center text-lg-left pb-5 pt-4 mob-pb-3">
					<div class="d-table w-100 h-100">
						<div class="d-table-cell w-100 h-100 align-middle">
							<h2>Three Senior Teams	</h2>
							<p>In 2006 the club added a second team under the management of David Shaw whose team talks where probably the funniest I’ve ever heard; thanks Dave!</p>
							<p>Following on from the success the club had in the Down Area Winter Football League and the growing number of players, we added a third team in 2013, playing in the South Antrim Football League under the guidance of Graeme bailie and Jonathan McCutcheon.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row justify-content-center py-5">
				<div class="col-lg-10 text-center">
					<p class="text-large"><i>"As I look forward to the next 30 years I can’t wait to see what’s in store for our little South Belfast club that now has members  from all over the country playing for us."</i></p>
					<p>Kyle Rendall - Club Co-founder</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid bg-light py-5 mob-pb-4">
	<div class="row">
		<div class="container container-wide py-4 mb-5">
			<div class="row">
				<div class="container container-wide text-center mb-3">
					<p class="mimic-h2 mb-2">Our Teams</p>
				</div>
				<div class="col-lg-4 mob-px-4 mob-pb-5 mob-mb-3 ipadp-mb-5">
					<div class="card">
						<picture>
							<source srcset="/img/teams/firsts.webp" type="image/webp"/>
							<source srcset="/img/teams/firsts.jpg" type="image/jpeg"/>
							<img src="/img/teams/firsts.jpg" class="w-100" alt="Newtown Forest First Team" />
						</picture>
						<div class="p-4 text-center">
							<p class="text-small text-blue letter-spacing mb-0">DAWFL Premier Division</p>
							<p class="mimic-h3 mb-0">First Team</p>
							<a href="/our-club/first-team">
								<button class="btn btn-primary mb-minus-65">Learn More</button>
							</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mob-px-4 mob-pb-5 mob-mb-3 ipadp-mb-5">
					<div class="card">
						<picture>
							<source srcset="/img/teams/seconds.webp" type="image/webp"/>
							<source srcset="/img/teams/seconds.jpg" type="image/jpeg"/>
							<img src="/img/teams/seconds.jpg" class="w-100" alt="Newtown Forest Second Team" />
						</picture>
						<div class="p-4 text-center">
							<p class="text-small text-blue letter-spacing mb-0">DAWFL Reserve Division 1</p>
							<p class="mimic-h3 mb-0">Second Team</p>
							<a href="/our-club/second-team">
								<button class="btn btn-primary mb-minus-65">Learn More</button>
							</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mob-px-4">
					<div class="card">
						<picture>
							<source srcset="/img/teams/thirds.webp" type="image/webp"/>
							<source srcset="/img/teams/thirds.jpg" type="image/jpeg"/>
							<img src="/img/teams/thirds.jpg" class="w-100" alt="Newtown Forest Third Team" />
						</picture>
						<div class="p-4 text-center">
							<p class="text-small text-blue letter-spacing mb-0">BDFL Division 4</p>
							<p class="mimic-h3 mb-0">Third Team</p>
							<a href="/our-club/third-team">
								<button class="btn btn-primary mb-minus-65">Learn More</button>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid position-relative pt-5 mob-pt-3">
	<div class="row ">
		<div class="container py-5">
			<div class="row pb-5 mob-pb-3">
				<div class="col-lg-8">
					<seen-enough></seen-enough>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection