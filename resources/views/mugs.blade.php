@php
$page = 'Mugs';
$pagetitle = 'Mugs | Newtown Forest Football Club';
$metadescription = 'Mugs Newtown Forest';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative pt-5 mt-5 mob-mt-0 ipadp-pt-0 menu-padding">
	<img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
	<div class="row">
		<div class="container">	
		  <div class="row mt-4 position-relative z-2">
		    <div class="col-12 mt-5 pt-5 ipadp-pt-0 mob-mt-0 mob-pt-0 text-center">
		      <div class="pre-title-lines mob-mx-auto mb-4 mob-my-45"></div>
		      <h1 class="mob-mt-0">Newtown Forest Mugs</h1>
		      <p class="text-large mb-0">Get yourself a mug and help raise funds for the club!</p>
		    </div>
		  </div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative pb-5 mob-pb-4 position-relative z-2">
  <div class="row">
    <div class="container pt-4 mt-5 mob-pt-0" style="background-color: #f3f4f7;"	>
      <div class="row justify-content-center">
      	<div class="col-lg-5 pr-0 mob-px-4 py-5 pl-5 mob-pb-0">
      		<picture>
            <source srcset="/img/shop/mugs.webp" type="image/webp"/>
            <source srcset="/img/shop/mugs.jpg" type="image/jpeg"/>
            <img src="/img/shop/mugs.jpg" alt="Newtown Forest Fundraising Mugs" class="w-100 shadow"/>
        	</picture>
        </div>
      	<div class="col-lg-7 pl-0  mob-px-4">
      		<div id="mcppug6pmdi69" class="py-5" style="background-color: #f3f4f7;"><a href="https://app.moonclerk.com/pay/ppug6pmdi69">Newtown Forest Mug</a></div>
    		</div>
      </div>
    </div>
  </div>
</div>
<div class="mb-5"></div>
@endsection
@section('scripts')
<script type="text/javascript">var mcppug6pmdi69;(function(d,t) {var s=d.createElement(t),opts={"checkoutToken":"ppug6pmdi69","width":"100%"};s.src='https://d2l7e0y6ygya2s.cloudfront.net/assets/embed.js';s.onload=s.onreadystatechange = function() {var rs=this.readyState;if(rs) if(rs!='complete') if(rs!='loaded') return;try {mcppug6pmdi69=new MoonclerkEmbed(opts);mcppug6pmdi69.display();} catch(e){}};var scr=d.getElementsByTagName(t)[0];scr.parentNode.insertBefore(s,scr);})(document,'script');</script>
@endsection