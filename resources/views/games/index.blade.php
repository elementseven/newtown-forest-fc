@php
$page = 'Results';
$pagetitle = "Newtown Forest Football Club | ";
$metadescription = "See all of Newtown Forest Football Club's latest results and upcoming fixtures.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
.vdatetime{
  position: relative;
}
.vdatetime:before{
  content: '\f073';
  font-family: 'FontAwesome';
  position: absolute;
  top: 20px;
  left: 20px;
  font-weight: bold;
  z-index: 1;
  color: #000;
}
.vdatetime:after{
  content: 'Select Date';
  position: absolute;
  top: 20px;
  left: 45px;
  font-weight: bold;
  z-index: 1;
}
.vdatetime.hide-placeholder:after{
    content: '';
}
.vdatetime input{
  background: transparent;
  position: relative;
  cursor: pointer;
  z-index: 2;
  font-weight: bold;
  color: #000;
  height: 60px;
  padding-left: 43px !important;
}
.vdatetime .post-box{
  cursor: pointer;
}
.vdatetime-popup__header,
.vdatetime-calendar__month__day--selected > span > span, 
.vdatetime-calendar__month__day--selected:hover > span > span{
  background: #3968bf !important;
}
.vdatetime-popup__actions__button{
  color: #3968bf !important;
}
</style>
@endsection
@section('header')
<header id="page-header" class="container-fluid position-relative z-2 fixtures-bg bg background-right top-padding">
{{-- 	<div class="trans"></div>
 --}}	<div class="d-table w-100 h-100 position-absolute">
		<div class="d-table-cell w-100 h-100 align-middle">
			<div class="container container-wide pt-5">
{{-- 				<h1 class="text-white mt-5">Fixtures & Results</h1>
 --}}			</div>
		</div>
	</div>

  <div class="bottom-grad"></div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative bg-light mob-mb-3">
	<img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
	<div class="row">
		<games-index :competitions="{{$competitions}}" :teams="{{$teams}}"></games-index>
	</div>
</div>
@endsection
@section('scripts')


@endsection