@php
$page = 'Result';
$gamename = \Carbon\Carbon::parse($game->date)->format('d/m/Y') . ' ' . $game->team->name . ' vs ' . $game->opposition->name;
$pagetitle = "Newtown Forest Football Club | " . \Carbon\Carbon::parse($game->date)->format('d/m/Y') . ' ' . $game->team->name . ' vs ' . $game->opposition->name;
$metadescription = \Carbon\Carbon::parse($game->date)->format('d/m/Y') . ' ' . $game->team->name . ' vs ' . $game->opposition->name;
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@section('styles')
<style>
	.text-yellow{
		color: #FFCB00;
	}
	.text-red{
		color: #E73C62;
	}
p[data-f-id="pbf"]{
	display: none !important;
}
</style>
@endsection
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding z-2">
  <img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
  <div class="row">
		<div class="container">
			<div class="row pt-5">
				<div class="col-12 mt-3">
					<div class="result text-center">
						<p class="mimic-h2">
							@if($game->home_away == 'Home')
							<picture>
			          <source srcset="/img/logos/logo.webp" type="image/webp"/> 
			          <source srcset="/img/logos/logo.png" type="image/png"/> 
			          <img src="/img/logos/logo.png" type="image/png" alt="Newtown Forest logo" class="mr-2 fixture-badge w-auto" width="50" height="50" style="margin-top: -10px;"/>
			        </picture>
					    @else
					    <picture>
			          <source srcset="{{$game->opposition->getFirstMediaUrl('oppositions', 'normal')}}" type="image/webp"/> 
			          <source srcset="{{$game->opposition->getFirstMediaUrl('oppositions', 'normal-webp')}}" type="{{$game->opposition->getFirstMedia('oppositions')->mime_type}}"/> 
			          <img src="{{$game->opposition->getFirstMediaUrl('oppositions', 'normal')}}" type="{{$game->opposition->getFirstMedia('oppositions')->mime_type}}" alt="{{$game->opposition->name}} logo" class="mr-2 fixture-badge w-auto" width="50" height="50" style="margin-top: -10px;" />
			        </picture>
					    @endif 
					    @if($game->report)
					    <b>{{$game->report->home_team_goals}} - {{$game->report->away_team_goals}}</b>
					    @endif
					    @if($game->home_away == 'Home')
					    <picture>
			          <source srcset="{{$game->opposition->getFirstMediaUrl('oppositions', 'normal')}}" type="image/webp"/> 
			          <source srcset="{{$game->opposition->getFirstMediaUrl('oppositions', 'normal-webp')}}" type="{{$game->opposition->getFirstMedia('oppositions')->mime_type}}"/> 
			          <img src="{{$game->opposition->getFirstMediaUrl('oppositions', 'normal')}}" type="{{$game->opposition->getFirstMedia('oppositions')->mime_type}}" alt="{{$game->opposition->name}} logo" class="ml-2 fixture-badge w-auto" width="50" height="50" style="margin-top: -10px;"/>
			        </picture>
					    @else
					    <picture>
			          <source srcset="/img/logos/logo.webp" type="image/webp"/> 
			          <source srcset="/img/logos/logo.png" type="image/png"/> 
			          <img src="/img/logos/logo.png" type="image/png" alt="Newtown Forest logo" class="ml-2 fixture-badge w-auto" width="50" height="50" style="margin-top: -10px;"/>
			        </picture>
					    @endif
					  </p>
					  <p class="mimic-h3 mob-text-smaller">
							@if($game->home_away == 'Home')
							{{$game->team->name}} 
						
					    @else
					    {{$game->opposition->name}}
					  
					    @endif 
					    vs
					    @if($game->home_away == 'Home')
					    {{$game->opposition->name}}
					    @else
					  
					    {{$game->team->name}}
					    @endif
					  </p>
					  <p class="text-smallest letter-spacing text-uppercase mb-0">{{$game->team->nickname}} - {{$game->competition->name}}</p>
					</div>
					
		      
		    </div>
		  </div>
		</div>
	</div>
	<div class="row bg-primary py-3 position-relative z-2 mt-5 mob-mt-4">
		<div class="col-12">
			<p class="mb-0 letter-spacing text-small text-center text-uppercase">{{$game->time}} - {{\Carbon\Carbon::parse($game->date)->format('jS M Y')}} | {{$game->location}}</p>
		</div>
	</div>
</header>

@endsection
@section('content')

<div class="container pb-5 pt-5 mob-pt-3 position-relative z-2">
	@if($game->report)
	<div class="row mt-3">
		<div class="col-lg-3 mb-4">
			@if($game->motm || count($game->goals))
			<div class="card shadow pt-4 px-4">
				<div class="row">
					@if(count($game->goals))
					<div class="col-6 col-lg-12 pb-4"> 
						<p class="mb-0"><b>Goals:</b></p>
						@foreach($game->goals as $goal)
						<p class="mb-0"><i class="fa fa-futbol-o mr-2" aria-hidden="true"></i>{{$goal->profile->full_name}} - {{$goal->time}}'</p>
						@endforeach
						<div class="mt-4 d-none d-lg-block"></div>
					</div>
					@endif
					@if(count($game->cards))
					<div class="col-6 col-lg-12 pb-4"> 
						<p class="mb-0"><b>Cards:</b></p>
						@foreach($game->cards as $card)
							<p class="mb-0"><i class="fa fa-square text-{{$card->colour}} mr-2" aria-hidden="true"></i>{{$card->profile->full_name}}</p>
						@endforeach
					</div>
					@endif
					@if($game->motm)
					<div class="col-6 col-lg-12 pb-4"> 
						<p class="mb-0"><b>Man of the match:</b></p>
						<p class="mb-0"><i class="fa fa-trophy mr-2" aria-hidden="true"></i>{{$game->motm->profile->full_name}}</p>
					</div>
					@endif
				</div>
			</div>
			@endif
			@if($game->squad && (Carbon\Carbon::now() >= Carbon\Carbon::createFromFormat('Y-m-d H:i', (Carbon\Carbon::parse($game->date)->format('Y-m-d') .' '. $game->time))))
			<div class="card shadow p-4 mt-4">
				<div class="row">
					
					<div class="col-12">
						<p class="mb-2"><b>Squad</b></p>
						@foreach ($game->squad->profiles->sortBy('full_name') as $p)
						<p class="mb-1"><i class="fa fa-user mr-2"></i> {{$p->full_name}}</p>
						@endforeach
					</div>
					
				</div>
			</div>
			@endif
		</div>
		<div class="col-lg-9 mob-px-4 text-center text-lg-left">
			@if($game->video_url != null && $game->video_url != 'null')
			<div class="game-video mb-4">
				<div class="embed-responsive embed-responsive-16by9">
				  <iframe class="embed-responsive-item" src="{{$game->video_url}}" allowfullscreen></iframe>
				</div>
			</div>
			@endif
			<div class="game-description">
				{!!$game->report->description!!}
			</div>
			<p class="mb-1 mt-5"><b>Share this result:</b>
        <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-blue">
          <i class="fa fa-facebook ml-3"></i>
        </a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($gamename)}}&amp;summary={{urlencode($gamename)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-blue">
          <i class="fa fa-linkedin ml-3"></i>
        </a>
        <a href="https://twitter.com/intent/tweet/?text={{urlencode($gamename)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-blue">
          <i class="fa fa-twitter ml-3"></i>
        </a>
        <a href="whatsapp://send?text={{urlencode($gamename)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-blue">
          <i class="fa fa-whatsapp ml-3"></i>
        </a>
      </p>
		</div>
	</div>

	<div class="row mt-5 py-5 mob-py-3">
  	<div class="col-lg-8 offset-lg-3">
  		<seen-enough></seen-enough>
  	</div>
  </div>
  @else
	<div class="row">
		<div class="col-12 text-center">
			<p class="text-large">Match report will be available here after the game.</p>
		</div>
	</div>
	<div class="row mt-5 py-5 mob-py-3">
  	<div class="col-lg-8">
  		<seen-enough></seen-enough>
  	</div>
  </div>
	@endif
</div>
@endsection
@section('scripts')


@endsection