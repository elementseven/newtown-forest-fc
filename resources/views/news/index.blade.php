@php
$page = 'News';
$pagetitle = 'Latest News - Newtown Forest Football Club';
$metadescription = 'Stay up to date with all of the latest Newtown Forest FC news and events, our news page has regular updates about everything going on the club.';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container-fluid position-relative z-2 news-bg bg background-right top-padding">
  <div class="bottom-grad"></div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative   bg-light">
	<img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
	<div class="row position-relative z-2 mob-mb-3">
		<news-index :categories="{{$categories}}"></news-index>
	</div>
</div>
@endsection