@php
$page = 'News';
$pagetitle = $post->title . ' | Newtown Forest Football Club';
$metadescription = $post->meta_description;
$keywords = $post->keywords;
$pagetype = 'light';
$pagename = 'news';
$ogimage = 'https://newtownforest.com' . $post->getFirstMediaUrl('double');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('styles')
<style>
.webp .news-bg-header{
  background-image: url('{{$post->getFirstMediaUrl('news','double-webp')}}');
}
.no-webp .news-bg-header{
  background-image: url('{{$post->getFirstMediaUrl('news','double')}}');
}
</style>
@endsection
@section('header')
<header id="page-header" class="container-fluid position-relative z-2 bg-navy news-bg-header news-header bg top-padding">
  <div class="bottom-grad"></div>
</header>
@endsection
@section('content')
<div class="container-fluid bg-light">
  <img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
  <div class="row">
    <div class="container container-wide mob-px-4 position-relative z-2">
      <div class="row mob-py-0 text-center text-lg-left">
        <div class="col-lg-9 mob-mt-0 mb-5 mob-mb-3 pr-5 mob-px-3 ipadp-px-3">
          <div class="card shadow p-5  mob-px-3 mt-minus">
            <div class="row">
              <div class="col-12 col-lg-6">
                <p class="text-small letter-spacing text-blue text-uppercase mb-3 mob-mb-0">News - {{\Carbon\Carbon::parse($post->created_at)->format('jS M Y')}}</p>
              </div>
              <div class="col-6 text-right d-none d-lg-block">
               <p class="text-small letter-spacing text-uppercase mb-3"><b>Share:</b>
                <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-blue">
                  <i class="fa fa-facebook ml-3 "></i>
                </a>
                <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-blue">
                  <i class="fa fa-linkedin ml-3"></i>
                </a>
                <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-blue">
                  <i class="fa fa-twitter ml-3"></i>
                </a>
                <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-blue">
                  <i class="fa fa-whatsapp ml-3"></i>
                </a>
              </p>
            </div>
          </div>
          <h1 class="mb-3 blog-title">{{$post->title}}</h1>
          <p class="text-large mb-4">{{$post->excerpt}}</p>
          <div class="blog-body">
            {!!$post->content!!}
               <p class="text-small letter-spacing text-uppercase mt-5 mb-0"><b>Share:</b>
                <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-blue">
                  <i class="fa fa-facebook ml-3 "></i>
                </a>
                <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-blue">
                  <i class="fa fa-linkedin ml-3"></i>
                </a>
                <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-blue">
                  <i class="fa fa-twitter ml-3"></i>
                </a>
                <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-blue">
                  <i class="fa fa-whatsapp ml-3"></i>
                </a>
              </p>
          </div>
        </div>
      </div> 
      <div class="col-lg-3 py-5 mob-pt-0">
        @if($others)
        <div class="row">
          @foreach($others as $article)
          <div class="col-12 col-md-6 col-lg-12 mt-4">
            <a href="/news/{{\Carbon\Carbon::parse($article->created_at)->format('Y-m-d')}}/{{$article->slug}}" >
                <picture>
                  <source srcset="{{$article->getFirstMediaUrl('news','featured-webp')}}" type="image/webp"/> 
                  <source srcset="{{$article->getFirstMediaUrl('news','featured')}}" type="{{$article->getFirstMedia('news')->mime_type}}"/> 
                    <img src="{{$article->getFirstMediaUrl('news','featured')}}" type="{{$article->getFirstMedia('news')->mime_type}}" alt="{{$article->title}}" class="w-100 h-auto lazy mb-2 cursor-pointer" />
                </picture>
                <div class="row">
                  <div class="col-8 pr-0 text-left">
                    <p class="text-smallest cursor-pointer letter-spacing text-grey text-uppercase mb-0">News - {{\Carbon\Carbon::parse($article->created_at)->format('jS M Y')}}</p>
                  </div>
                  <div class="col-4 pl-0 text-right">
                    <p class="text-smallest text-blue text-uppercase mb-0">Read article <i class="fa fa-angle-double-right cursor-pointer"></i></p>
                  </div>
                </div>
                <p class="text-large text-left text-title text-dark mb-0  cursor-pointer">{{$article->title}}</p>

              </a>
            </div>
            @endforeach
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container pb-5 mob-mt-5 mob-px-4">
  <div class="row mt-5 py-5 mob-py-3">
    <div class="col-lg-8">
      <seen-enough></seen-enough>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  $(window).load(function(){
    $('.ql-video').each(function(i, e){
      var width = $(e).width();
      $(e).css({
        "width": width,
        "height": width*(9/16)
      });
    });
  });
</script>
@endsection