@php
$page = 'Statistics';
$pagetitle = 'Statistics | Newtown Forest Football Club';
$metadescription = 'View club Statistics from the current season';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding z-2">
  <img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
  <div class="row">
		<div class="container">
			<div class="row pt-5">
				<div class="col-12 mt-3">
					<div class="result text-center">
						<p class="mimic-h1 text-capitalize mb-1">
							Statistics
					  </p>							
					  <p class="text-smallest letter-spacing text-uppercase mb-0">Check your stats below!</p>
					</div>
					
		    </div>
		  </div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative pb-5 mob-pb-4 position-relative z-2">
  <statistics></statistics>
</div>
<div class="mb-5"></div>
@endsection
@section('scripts')
@endsection