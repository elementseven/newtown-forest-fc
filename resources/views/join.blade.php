@php
$page = 'Join';
$pagetitle = 'Join Us | Newtown Forest Football Club';
$metadescription = 'Join Newtown Forest Football Club!';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
.vdatetime{
  position: relative;
}
.vdatetime:before{
  content: '\f073';
  font-family: 'FontAwesome';
  position: absolute;
  top: 20px;
  left: 20px;
  font-weight: bold;
  z-index: 1;
  color: #000;
}
.vdatetime:after{
  content: 'Date of birth';
  position: absolute;
  top: 20px;
  left: 45px;
  font-weight: bold;
  z-index: 1;
}
.vdatetime.hide-placeholder:after{
    content: '';
}
.vdatetime input{
  background: transparent;
  position: relative;
  cursor: pointer;
  z-index: 2;
  font-weight: bold;
  color: #000;
  height: 60px;
  padding-left: 43px !important;
}
.vdatetime .post-box{
  cursor: pointer;
}
.vdatetime-popup__header,
.vdatetime-calendar__month__day--selected > span > span, 
.vdatetime-calendar__month__day--selected:hover > span > span{
  background: #3968bf !important;
}
.vdatetime-popup__actions__button{
  color: #3968bf !important;
}
.training-bg{
  background-size: cover !important;
}
</style>
@endsection
@section('header')
<header class="container-fluid position-relative pt-5 mt-5 menu-padding">
</header>
@endsection
@section('content')
<div class="container-fluid position-relative position-relative z-2 mt-5 mob-mt-0">
  <div class="row mob-mb-5 pt-5 mob-pt-0 ipadp-pt-0">
    <div class="container pt-4 ipadp-pt-0 mob-pt-0">
      <div class="row justify-content-center">
      	<div class="col-lg-8 pr-5 mob-pr-3 pb-4 mob-px-4 ipadp-px-3">
      		<h1 class="mob-mt-0 text-center text-lg-left">Join Us</h1>
		      <p class=" text-center text-lg-left">Leave your details in the form below and we will get back to you. Don't forget you can always come to one of our training sessions and give it a go!</p>
          <join-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></join-form>
    		</div>
    		<div class="col-lg-4 d-none d-lg-block">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-bottom w-100 h-100">
        			<picture>
                <source srcset="/img/graphics/you.webp" type="image/webp"/> 
                <source srcset="/img/graphics/you.jpg" type="image/jpeg"/> 
                  <img src="/img/graphics/you.jpg" type="image/jpeg" class="w-100" alt="You? - Newtown Forest FC"/>
              </picture>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row training-bg bg-fixed bg position-relative">
  	<div class="trans"></div>
  	<div class="container py-5">
  		<div class="row py-5 mob-py-0">
  			<div class="col-lg-6 py-5">
  				<div class="card shadow bg-navy p-5 mob-px-4 mob-py-4 text-center text-lg-left">
	  				<p class="mimic-h3 text-white">Training Schedule:</p>
	  				<p class="text-white text-large mb-1">Tuesdays 8pm - 9pm @ <a href="https://www.google.com/maps/place/Belvoir+3G+Pitch/@54.5528414,-5.9339493,1125m/data=!3m1!1e3!4m8!1m2!2m1!1sboys+brigade+belvoir!3m4!1s0x0:0xdb6c68e4dd97321f!8m2!3d54.5536595!4d-5.9320134" target="_blank" class="text-primary">Belvoir BB</a></p>
	  				<p class="text-white text-large mb-0">Thursdays 8.45pm - 10pm @ <a href="https://www.google.com/maps/place/Aquinas+Diocesan+Grammar+School/@54.5759744,-5.9075122,15z/data=!4m5!3m4!1s0x0:0xa221983fe3340d31!8m2!3d54.5759744!4d-5.9075122" target="_blank" class="text-primary">Aquinas Grammar</a></p>
	  			</div>
  			</div>
  		</div>
  	</div>
  </div>
</div>
@endsection