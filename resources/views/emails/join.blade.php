<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
				<p style="text-align: center; padding: 40px 40px 0;">
					<img src="{{url('/')}}/img/logos/logo.png" width="70" style="margin: auto; display: block;" alt="Newtown Forest FC Logo" />
				</p>

				<p style="font-size:22px; background-color: #1c2d4b; padding: 13px 15px; height:auto;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;font-weight:700;">{{$subject}}</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi,</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">{{$name}} is interested in joining as a new player, details below:</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Name:</b> {{$name}}</p>
				
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Email Address:</b> {{$email}}</p>
				@if($phone)
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Phone:</b> {{$phone}}</p>
				@endif
				@if($position)
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Position:</b> {{$position}}</p>
				@endif
				@if($dob)
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Date of Birth:</b> {{$dob}}</p>
				@endif
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>Previous football experience:</b></p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">{{$content}}</p>

				</td>
				</tr>
				<tr><td>
					<hr style="margin: 30px auto 0;">
					<img src="{{url('/')}}/img/logos/logo.png" width="50" style="margin: 30px auto 0; display: block;" alt="Newtown Forest FC Logo" />
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the <a href="http://newtownforest.com">Newtown Forest FC website</a>.<br>Please ignore this email if it was sent to you by mistake.</p></td></tr>
			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>