@php
$page = 'Profile';
$pagetitle = $profile->full_name . " - Newtown Forest Football Club";
$metadescription = 'Player profile for ' . $profile->full_name . ' at Newtown Forest Football Club';
$pagetype = 'dark';
$pagename = 'profile';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@section('styles')
<style>
p[data-f-id="pbf"]{
	display: none !important;
}
</style>
@endsection
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container-fluid position-relative z-1 profile-bg bg top-padding d-none d-lg-block">
  <div class="bottom-grad"></div>
</header>

@endsection
@section('content')
<div class="container-fluid position-relative">
	<img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
	<div class="row">
		<div class="container position-relative z-2 mob-mt-5">
			<div class="row justify-content-center">
				<div class="col-lg-4 mob-px-4">
					<div class="card px-0 mt-minus mob-mt-5"> 
						@if($profile->hasMedia('profiles'))
						<picture>
			        <source src="{{$profile->normalwebp}}" type="image/webp"/> 
			        <source src="{{$profile->normal}}" type="{{$profile->mimetype}}"/> 
			        <img src="{{$profile->normal}}" type="{{$profile->mimetype}}" alt="{{$profile->full_name}} - NFFC" class="w-100"/>
			      </picture>
			      @else
			      <picture>
		          <source srcset="/img/temp/profile.webp" type="image/webp"/> 
		          <source srcset="/img/temp/profile.jpg" type="image/jpeg"/> 
		          <img src="/img/temp/profile.jpg" type="image/jpeg" alt="Profile placeholder image" class="w-100"/>
		        </picture>
			      @endif
			    </div>
		    </div>
		    <div class="col-lg-6 pl-5 mob-px-4">
		    	<div class="pt-5 mob-pt-3">
		    		<p class="mb-3 mimic-h2 text-title text-dark"><b>{{$profile->full_name}}</b></p>
		    		<p class="mb-1 text-large"><b>Appearances:</b> {{sprintf('%02d',count($profile->squads))}} <span class="smaller text-mid-grey">*since September 2022</span></p>
			    	<p class="mb-1 text-large"><b>Goals:</b> {{sprintf('%02d', count($profile->goals))}} <span class="smaller text-mid-grey">*since September 2022</span></p>
		    		@if($profile->position != null)<p class="mb-1 text-large"><b>Position:</b> {{$profile->position}}</p>@endif
			      @if($profile->height != null)<p class="mb-1 text-large"><b>Height:</b> {{$profile->height}}</p>@endif
			    	@if($profile->dob != null)<p class="mb-1 text-large"><b>Age:</b> {{\Carbon\Carbon::parse($profile->dob)->age}}</p>@endif
			    	@if($profile->foot != null)<p class="mb-1 text-large"><b>Foot:</b> {{$profile->foot}}</p>@endif
			    	
			    </div>
		    </div>
		    <div class="col-lg-2 d-none d-lg-block">
		    	<div class=" mt-minus pt-5">
		     		<img src="/img/positions/{{strtolower($profile->position)}}.png" class="w-100" width="200" />
		     	</div>
		    </div>
			</div>
		</div>
	</div>
</div>
<profile-games :profile="{{$profile->id}}"></profile-games>
<div class="container pb-5 pt-5 mob-pt-0 position-relative z-2">
	<div class="row mt-5 py-5 mob-py-3">
  	<div class="col-lg-8">
  		<seen-enough></seen-enough>
  	</div>
  </div>
</div>
@endsection
@section('scripts')


@endsection