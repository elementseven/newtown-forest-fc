@php
$page = 'Profiles';
$pagetitle = 'Player Profiles - Newtown Forest Football Club';
$metadescription = 'View profiles of our players and check out their stats.';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container-fluid position-relative z-2 bg-navy players-bg bg-bottom-contain top-padding">
  <div class="bottom-grad"></div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative   bg-light">
	<img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
	<div class="row position-relative z-2 mob-mb-3">
		<profiles-index></profiles-index>
	</div>
</div>
@endsection