@php
$page = 'Homepage';
$pagetitle = "Newtown Forest Football Club | Cross-community football team in Belfast";
$metadescription = "Newtown Forest Football Club is a friendly, open and inclusive football club based in Belfast, formed in 1995, welcoming people from all communities.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://newtownforest.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])

@section('styles')
<style>
.webp .home-bg-featured{
    background-image: url('{{$featured->getFirstMediaUrl('news','double-webp')}}');
}
.no-webp .home-bg-featured{
    background-image: url('{{$featured->getFirstMediaUrl('news','double')}}');
}
</style>
@endsection
@section('header')
<header id="page-header" class="container-fluid position-relative z-1 top-padding">
    <div class="row bg-dark-navy">
      <div class="col-lg-7 px-0 home-bg-featured bg">

      </div>
      <div class="col-lg-5 pt-5 mob-pt-0 ipadp-pt-0 mob-px-4 home-header-inner position-relative">
        <div class="bg bg-blur home-bg-featured home-header-inner-bg"></div>
        <div class="d-table w-100 h-100 px-4 mob-px-0 py-5 text-white position-relative z-2">
            <div class="d-table-cell align-bottom w-100 h-100 text-center text-lg-left">
                <p class="text-smallest text-primary text-uppercase letter-spacing mb-0">Announcements</p>
                <p class="mimic-h2 mb-1"><b>{{$featured->title}}</b></p>
                <p class="">{{$featured->excerpt}}</p>
                <a href="/news/{{\Carbon\Carbon::parse($featured->created_at)->format('Y-m-d')}}/{{$featured->slug}}">
                    <button class="btn btn-primary">Read More</button>
                </a>
            </div>
        </div>
      </div>
    </div>
  </header>
@endsection
@section('content')
<div class="container-fluid position-relative mob-mb-0 z-2">
    <img src="/img/graphics/ball.svg" alt="Newtown Forest Football Club ball graphic" class="big-ball" width="960" height="960"/>
    <div class="row">
        <div class="container py-5">
            <div class="row my-5 mob-my-3">
                <div class="col-lg-7 text-center text-lg-left">
                    <h2>Cross-community football club based<br class="d-md-none" /> in Belfast</h2>
                    <p>Newtown Forest Football Club is a friendly, open and inclusive football club formed in 1997, welcoming people from all communities.</p>
                    <p class="mb-4">We have three teams playing across three different leagues at different levels, with games taking place in Belfast and the surrounding area. Our club is based in South Belfast, home games are played at Hydebank Playing Fields and training takes place on Tuesday and Thursday evenings at Aquinas Grammar and Belvoir BB respectively.</p>
                    <a href="/join">
                        <button class="btn btn-primary" type="button">Join Us</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row pb-5 mob-pb-0">
        <div class="col-12 text-center d-lg-none">
            <p class="mimic-h3">Fixtures & Results</p>
        </div>
        <upcoming-fixtures :team="'*'"></upcoming-fixtures>
        <div class="col-12 text-center mt-4">
            <a href="/fixtures-and-results">
                <button class="btn btn-primary" type="button">Browse Fixtures</button>
            </a>
        </div>
    </div>
    <div class="row huddle-bg bg-fixed background-right py-5 mob-py-0 mt-5">
        <div class="container ipadp-px-0">
            <div class="row">
                <picture>
                    <source media="(min-width: 600px)" srcset="/img/bg/huddle.webp" type="image/webp">
                    <source media="(min-width: 600px)" srcset="/img/bg/huddle.jpeg" type="image/jpeg">
                    <source media="(max-width: 599px)"srcset="/img/bg/huddle-mob.webp" type="image/webp"/>
                    <source media="(max-width: 599px)"srcset="/img/bg/huddle-mob.jpg" type="image/jpeg"/>
                    <img src="/img/bg/huddle-mob.jpg" alt="Newtown Forest Cross Community football club Belfast Northern Ireland" class="img-fluid w-100 d-lg-none"/>
                </picture>
                <div class="col-lg-6 py-5 mob-py-0 text-center text-lg-left">
                    <div class="card shadow bg-navy p-5 mob-px-4 mob-py-4 mob-mt-card-minus">
                        <p class="mimic-h2 text-white">Our Club</p>
                        <p class="text-white">Our club is focussed on providing our players with a platform to perform to their highest possible level on the field, while also encouraging social engagement so players can benefit from everything the club has to offer. Team sports are known to improve your health, confidence and self-esteem. We want everyone at Newtown Forest to experience all the benefits of playing for a forward thinking football club.</p>
                        <a href="/our-club/about">
                            <button type="button" class="btn btn-primary">Find out more</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid position-relative bg-light py-5 mob-pt-3">
    <div class="row position-relative z-2 pt-5 mob-pb-3"> 
        <div class="container container-wide text-center">
            <p class="mimic-h2 mb-2">Latest News</p>
        </div>
        <news-home></news-home>
    </div>
</div>
<div class="container-fluid position-relative pt-5 mob-pt-3">
    <div class="row ">
        <div class="container py-5">
            <div class="row pb-5 mob-pb-3">
                <div class="col-lg-8">
                  <seen-enough></seen-enough>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')

@endsection