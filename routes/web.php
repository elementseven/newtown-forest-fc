<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/test-scorers', [PageController::class, 'testScorers'])->name('testScorers');

Route::get('/', [PageController::class, 'home'])->name('home');
Route::get('/our-club/about', [PageController::class, 'about'])->name('about');
Route::get('/join', [PageController::class, 'join'])->name('join');
Route::get('/contact', [PageController::class, 'contact'])->name('contact');
Route::get('/privacy-policy', [PageController::class, 'privacyPolicy'])->name('privacyPolicy');
Route::get('/terms-and-conditions', [PageController::class, 'tandcs'])->name('tandcs');

Route::post('/send-message', [SendMail::class, 'enquiry'])->name('sendMessage');
Route::post('/send-join-form', [SendMail::class, 'sendJoinForm'])->name('sendJoinForm');
Route::post('/mailinglist', [SendMail::class, 'mailingListSignup'])->name('mailingList');

Auth::routes();

Route::get('/news', [NewsController::class, 'index'])->name('news.index');
Route::get('/news/get', [NewsController::class, 'get'])->name('news.get');
Route::get('/news/get-without-featured', [NewsController::class, 'getWithoutFeatured'])->name('news.get');
Route::get('/news/{date}/{slug}', [NewsController::class, 'show'])->name('news.show');

Route::get('/fixtures-and-results', [GameController::class, 'index'])->name('games.index');
Route::get('/games/get', [GameController::class, 'get'])->name('games.get');
Route::get('/upcoming-fixtures/get', [GameController::class, 'upcomingFixtures'])->name('fixtures.get');
Route::get('/results/{game}/{date}/{teams}', [GameController::class, 'show'])->name('match-report');

Route::get('/our-club/first-team', [PageController::class, 'firstTeam'])->name('first-team');
Route::get('/our-club/second-team', [PageController::class, 'secondTeam'])->name('second-team');
Route::get('/our-club/third-team', [PageController::class, 'thirdTeam'])->name('third-team');

Route::get('/league-tables/first-team', [LeagueTableController::class, 'firstTeam'])->name('first-team-league-table');
Route::get('/league-tables/second-team', [LeagueTableController::class, 'secondTeam'])->name('second-team-league-table');

Route::get('/sign-up', [PageController::class, 'pay'])->name('pay');
Route::get('/mugs', [PageController::class, 'mugs'])->name('mugs');
Route::get('/donate', [PageController::class, 'donate'])->name('donate');

Route::get('/player-profiles', [ProfileController::class, 'index'])->name('profiles.index');
Route::get('/profiles/get', [ProfileController::class, 'get'])->name('profiles.get');
Route::get('/profiles/{profile}/{slug}', [ProfileController::class, 'show'])->name('profiles.show');
Route::get('/profiles/fixtures/get/{profile}', [GameController::class, 'profileGames'])->name('profiles.games');

Route::get('/stats', [PageController::class, 'stats'])->name('stats');
Route::get('/stats/get-top-scorers', [ProfileController::class, 'getTopScorers'])->name('stats.get-top-scorers');
Route::get('/stats/get-top-scorers-firsts', [ProfileController::class, 'getTopScorersFirsts'])->name('stats.get-top-scorers-firsts');
Route::get('/stats/get-top-scorers-seconds', [ProfileController::class, 'getTopScorersSeconds'])->name('stats.get-top-scorers-seconds');
Route::get('/stats/get-top-scorers-thirds', [ProfileController::class, 'getTopScorersThirds'])->name('stats.get-top-scorers-thirds');
Route::get('/stats/get-appearances', [ProfileController::class, 'getAppearances'])->name('stats.get-appearances');
Route::get('/stats/get-assists', [ProfileController::class, 'getAssists'])->name('stats.get-assists');
Route::get('/stats/get-motms', [ProfileController::class, 'getMotms'])->name('stats.get-motms');

Route::get('/summer-league', [PageController::class, 'summerLeague'])->name('summer-league');

Route::get('/logout', function(){
    Auth::logout();
    return redirect()->to('/login');
});