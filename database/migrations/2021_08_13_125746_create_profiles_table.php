<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();

            $table->string('full_name');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('position')->nullable();
            $table->boolean('status');
            $table->string('height')->nullable();
            $table->string('foot')->nullable();
            $table->date('dob')->nullable();
            $table->dateTime('joined')->nullable();
            $table->text('description')->nullable();
            $table->string('image')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
